import unittest
import sys
from unittest.case import SkipTest
from compiler.ast import flatten
import base64

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import StaleElementReferenceException
from selenium import webdriver


# Executor / Base Caps -------------------------------------------------------------------------------------------------


EXECUTOR = 'http://<YOUR_USERNAME>:<YOUR_ACCESS_KEY>@hub.browserstack.com:80/wd/hub'
BASE_CAPS = {"acceptSslCerts": "true", "ACCEPT_SSL_CERTS": "true", "safariIgnoreFraudWarning": "true",
             "browserstack.debug": "true", "network.http.phishy-userpass-length": "255",
             "INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS": "true", "ignoreProtectedModeSettings": "true"}

# Taking screen-shots --------------------------------------------------------------------------------------------------

def screenshot( webdriver, filename="screenshot.png" ):
    if isinstance( webdriver, webdriver.Remote ):
        data = base64.decodestring(webdriver.get_screenshot_as_base64())
        file = open(filename, "w")
        file.write(data)
        file.close()
    else:
        webdriver.save_screenshot(filename)


# Combining test classes with driver classes and setup classes ---------------------------------------------------------

class TestCase(object):

    def check_language(self):
        try:
            WebDriverWait(self.driver, 5).until( EC.presence_of_element_located((By.XPATH, "//html[@lang='%s']" % self.language()[:2] )) )
        except:
            try:
                lang = WebDriverWait(self.driver, 10).until( EC.presence_of_element_located((By.XPATH, "//html")), "HTML tag not found" ).get_attribute('lang')
            except StaleElementReferenceException:
                lang = WebDriverWait(self.driver, 10).until( EC.presence_of_element_located((By.XPATH, "//html")), "HTML tag not found" ).get_attribute('lang')
            self.verify( lang!=None and len(lang)>0 and self.language().startswith(lang), "Wrong language. Expected: '%s', Got: '%s'" % ( self.language(), lang ) )
        return self

    def get(self,url):
        self.driver.get(url)
        self.check_language()
        return self

    def verify( self, condition, description="" ):
        self.assertTrue(condition, description)

    def verifyWait( self, conditionFunc, description="", timeOut=20 ):
        try:
            WebDriverWait(self.driver, timeOut).until( lambda driver : conditionFunc() )
        except TimeoutException:
            self.assertTrue(False, description)


def define_testcases( *classes ):
    classes = list(set(flatten(classes)))
    tests = [test for test in classes if issubclass(test,TestCase)]
    drivers = [driver for driver in classes if issubclass(driver,WebDriver)]
    setups = [setup for setup in classes if issubclass(setup,TestSetup)]
    if not setups:
        setups = [DEFAULT,]
    result = []
    for test in tests:
        for setup in setups:
            for driver in drivers:
                name = "_".join( cl.__name__ for cl in (test,setup,driver) )
                tc = type(name, (test,setup,driver), {'__module__': test.__module__})
                setattr(sys.modules[test.__module__], name, tc)
                result.append(tc)
    return result


# Test setups ----------------------------------------------------------------------------------------------------------

class TestSetup(object):
    pass

# Default setup
class DEFAULT(TestSetup):
    def setUp(self):
        self.driver = self.get_driver()
        self.driver.delete_all_cookies()

# Small screen size
class SMALL(TestSetup):
    def setUp(self):
        self.driver = self.get_driver()
        self.driver.delete_all_cookies()
        self.driver.set_window_size(320,480)

# Desktop screen size
class DESKTOP(TestSetup):
    def setUp(self):
        self.driver = self.get_driver()
        self.driver.delete_all_cookies()
        self.driver.set_window_size(1024,768)


# Web drivers ----------------------------------------------------------------------------------------------------------

class WebDriver(unittest.TestCase):
    def language(self):
        return 'en'
    def base_caps(self):
        caps = BASE_CAPS.copy()
        caps['name'] = self.__class__.__name__
        return caps
    def get_driver(self):
        return None
    def setUp(self):
        self.driver = self.get_driver()
        if self.driver is None:
            raise SkipTest
    def tearDown(self):
        self.driver.quit()

# Firefox

class LocalFirefox(WebDriver):
    def language(self):
        return self.__language__
    def get_driver(self,lang='en'):
        self.__language__ = lang
        profile = webdriver.FirefoxProfile()
        profile.set_preference("intl.accept_languages",self.__language__)
        return webdriver.Firefox(profile)

class LocalFirefox_EN(LocalFirefox):
    def get_driver(self):
        return super(LocalFirefox_EN,self).get_driver('en_UK')

class LocalFirefox_DE(LocalFirefox):
    def get_driver(self):
        return super(LocalFirefox_DE,self).get_driver('de_DE')

class RemoteFirefox(WebDriver):
    def language(self):
        return self.__language__
    def get_driver(self,lang='en'):
        self.__language__ = lang
        profile = webdriver.FirefoxProfile()
        profile.set_preference("intl.accept_languages",self.__language__)
        profile.update_preferences()
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Firefox', 'browser_version': '31.0', 'os': 'Windows', 'os_version': '8.1'}.items()), browser_profile=profile )

class RemoteFirefox_EN(RemoteFirefox):
    def get_driver(self):
        return super(RemoteFirefox_EN,self).get_driver('en_UK')

class RemoteFirefox_DE(RemoteFirefox):
    def get_driver(self):
        return super(RemoteFirefox_DE,self).get_driver('de_DE')

class RemoteFirefoxWin7(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Firefox', 'browser_version': '31.0', 'os': 'Windows', 'os_version': '7'}.items()) )

class RemoteFirefoxWin81(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Firefox', 'browser_version': '31.0', 'os': 'Windows', 'os_version': '8.1'}.items()) )

class RemoteFirefoxOSX109(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Firefox', 'browser_version': '31.0', 'os': 'OS X', 'os_version': 'Mavericks'}.items()) )

# Chrome

class LocalChrome(WebDriver):
    def language(self):
        return self.__language__
    def get_driver(self,lang='en'):
        self.__language__ = lang
        options = webdriver.ChromeOptions()
        options.add_argument("--lang=%s" % self.__language__)
        return webdriver.Chrome(chrome_options=options)

class LocalChrome_EN(LocalChrome):
    def get_driver(self):
        return super(LocalChrome_EN,self).get_driver('en_UK')

class LocalChrome_DE(LocalChrome):
    def get_driver(self):
        return super(LocalChrome_DE,self).get_driver('de_DE')

class RemoteChrome(WebDriver):
    def language(self):
        return self.__language__
    def get_driver(self,lang='en'):
        self.__language__ = lang
        options = webdriver.ChromeOptions()
        options.add_argument("--lang=%s" % self.__language__ )
        CAPS = {'browser': 'Chrome', 'browser_version': '36.0', 'os': 'Windows', 'os_version': '8.1' }
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + CAPS.items() + options.to_capabilities().items() ) )

class RemoteChrome_EN(RemoteChrome):
    def get_driver(self):
        return super(RemoteChrome_EN,self).get_driver('en_UK')

class RemoteChrome_DE(RemoteChrome):
    def get_driver(self):
        return super(RemoteChrome_DE,self).get_driver('de_DE')

class RemoteChromeWin7(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Chrome', 'browser_version': '36.0', 'os': 'Windows', 'os_version': '7'}.items()) )

class RemoteChromeWin81(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Chrome', 'browser_version': '36.0', 'os': 'Windows', 'os_version': '8.1'}.items()) )

class RemoteChromeOSX109(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Chrome', 'browser_version': '36.0', 'os': 'OS X', 'os_version': 'Mavericks'}.items()) )

class RemoteAndroid41(WebDriver):
    def get_driver(self):
        # Android 4.1 on BrowserStack: 'Samsung Galaxy Note II' or 'Google Nexus 7'
        driver = webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browserName': 'android', 'platform': 'ANDROID', 'device': 'Samsung Galaxy Note II' }.items()) )
        driver.implicitly_wait(20)
        return driver

class RemoteAndroid42(WebDriver):
    def get_driver(self):
        driver = webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browserName': 'android', 'platform': 'ANDROID', 'device': 'LG Nexus 4' }.items()) )
        driver.implicitly_wait(20)
        return driver

# Internet Explorer

class LocalIE(WebDriver):
    def get_driver(self):
        return webdriver.Ie()

class RemoteIE10Win7(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'IE', 'browser_version': '10.0', 'os': 'Windows', 'os_version': '7'}.items()) )

class RemoteIE10Win8(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'IE', 'browser_version': '10.0', 'os': 'Windows', 'os_version': '8'}.items()) )

class RemoteIE11Win7(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'IE', 'browser_version': '11.0', 'os': 'Windows', 'os_version': '7'}.items()) )

class RemoteIE11Win81(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'IE', 'browser_version': '11.0', 'os': 'Windows', 'os_version': '8.1'}.items()) )

# Safari

class RemoteSafariOSX109(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browser': 'Safari', 'browser_version': '7.0', 'os': 'OS X', 'os_version': 'Mavericks'}.items()) )

class RemoteIPadAir(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browserName': 'iPad', 'platform': 'MAC', 'device': 'iPad Air'}.items()) )

class RemoteIPadMini(WebDriver):
    def get_driver(self):
        return webdriver.Remote( command_executor=EXECUTOR, desired_capabilities=dict( self.base_caps().items() + {'browserName': 'iPad', 'platform': 'MAC', 'device': 'iPad mini Retina'}.items()) )


# Web driver groups ----------------------------------------------------------------------------------------------------

drivers_local =   (LocalFirefox_EN,LocalChrome_EN)
drivers_desktop = (RemoteFirefoxWin7,RemoteFirefoxOSX109,RemoteChromeWin7,RemoteChromeWin81,RemoteChromeOSX109,RemoteIE10Win7,RemoteIE10Win8,RemoteIE11Win7,RemoteIE11Win81,RemoteSafariOSX109)
drivers_mobile =  (RemoteAndroid42,RemoteIPadAir,RemoteIPadMini)
drivers_remote =  drivers_desktop + drivers_mobile
