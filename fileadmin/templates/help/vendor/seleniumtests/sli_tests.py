# requirements:  pip install selenium
# run directly:  python sli_tests.py
# nose/xml:      nosetests -v --with-xunit

from selenium.webdriver.common.keys import Keys

from drivers import *
from globals import SERVER
import HTMLTestRunner


class dummy_test(unittest.TestCase):
    pass


class SliBase(TestCase):
    def check_submit(self, enabled):
        submitBtn = self.driver.find_element_by_class_name("submit")
        wait = WebDriverWait(self.driver, 10)
        if enabled:
            wait.until( lambda driver : submitBtn.is_enabled() and "btn-primary" in submitBtn.get_attribute("class").split(), "Submit not enabled" )
        else:
            wait.until( lambda driver : (not submitBtn.is_enabled()) and "btn-disabled" in submitBtn.get_attribute("class").split(), "Submit enabled" )


class WhatBlurredTest(SliBase):

    def test_what_should_be_blurred(self):
        self.get("%s/index.php?id=1" % SERVER)

        self.check_submit(False)

        self.driver.find_element_by_css_selector("label[for='numberplate']").click()
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='someonesface']").click()
        self.check_submit(False)
        self.driver.find_element_by_id("textface").find_element_by_tag_name("textarea").send_keys("some text")
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='yourhome']").click()
        self.check_submit(False)
        self.driver.find_element_by_id("textyourhome").find_element_by_tag_name("textarea").send_keys("some text")
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='something']").click()
        self.check_submit(False)
        self.driver.find_element_by_id("textsomething").find_element_by_tag_name("textarea").send_keys("some text")
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='notifyme']").click()
        self.check_submit(False)
        mailtext = self.driver.find_element_by_id("textemail").find_element_by_css_selector("input[class='email'][type='text']")
        mailtext.send_keys("test@test.test")
        self.check_submit(True)
        mailtext.clear()
        mailtext.send_keys(Keys.BACK_SPACE)
        self.check_submit(False)
        mailtext.send_keys("test")
        self.check_submit(False)
        mailtext.send_keys(".test")
        self.check_submit(False)
        mailtext.send_keys("@")
        self.check_submit(False)
        mailtext.send_keys("test")
        self.check_submit(False)
        mailtext.send_keys(".")
        self.check_submit(False)
        mailtext.send_keys("test")
        self.check_submit(True)
        self.driver.find_element_by_class_name("submit").click()
        #WebDriverWait(driver, 10).until( lambda driver : driver.find_elements_by_xpath("//h1[contains(text(), 'Success')]") )


class ImageQualityTest(SliBase):

    def test_report_image_quality(self):
        self.get("%s/index.php?id=2" % SERVER)
        self.check_submit(False)

        self.driver.find_element_by_css_selector("label[for='nomatch']").click()
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='viewobstructed']").click()
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='poorquality']").click()
        self.check_submit(True)

        self.driver.find_element_by_css_selector("label[for='notifyme']").click()
        self.check_submit(False)
        mailtext = self.driver.find_element_by_id("textemail").find_element_by_css_selector("input[class='email'][type='text']")
        mailtext.send_keys("test@test.test")
        self.check_submit(True)
        mailtext.clear()
        mailtext.send_keys(Keys.BACK_SPACE)
        self.check_submit(False)
        mailtext.send_keys("test")
        self.check_submit(False)
        mailtext.send_keys(".test")
        self.check_submit(False)
        mailtext.send_keys("@")
        self.check_submit(False)
        mailtext.send_keys("test")
        self.check_submit(False)
        mailtext.send_keys(".")
        self.check_submit(False)
        mailtext.send_keys("test")
        self.check_submit(True)
        self.driver.find_element_by_class_name("submit").click()
        #WebDriverWait(driver, 10).until( lambda driver : driver.find_elements_by_xpath("//h1[contains(text(), 'Success')]") )


TESTS = []
TESTS += define_testcases(

        WhatBlurredTest,
        ImageQualityTest,

        LocalFirefox_EN, LocalFirefox_DE,
        #drivers_desktop,
        #drivers_mobile,

        SMALL,
        DESKTOP,

    )


if __name__ == "__main__":
    suite = unittest.TestSuite()
    for t in TESTS:
        suite.addTests( unittest.TestLoader().loadTestsFromTestCase(t) )
    HTMLTestRunner.HTMLTestRunner(stream=file('results_sli_tests.html','wb'),verbosity=2).run(suite)

