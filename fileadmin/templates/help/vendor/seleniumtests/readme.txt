
Files:
======

readme.txt         : this file
ConfigMatrix.jpg   : test configuration matrix

chromedriver.exe   : needed for local tests on Chrome Browser
IEDriverServer.exe : needed for local tests on Internet Explorer
ie_user_pass.reg   : registry entry to enable user:pass@ syntax on IE

drivers.py         : web driver configurations
globals.py         : global config, i.e. test server address
sli_tests.py       : SLI tests
help_tests.py      : HELP tests
suite.py           : run all tests with HTML output

Setup:
======

Install python2.7  (https://www.python.org/download/releases/2.7/)
Install pip        (http://pip.readthedocs.org/en/latest/installing.html)
Install selenium   (pip install selenium)

Run tests:
==========

1. Verify executor (browserstack) and drivers configuration in drivers.py

	Verify EXECUTOR string in drivers.py
	If another browserstack account or seleniumgrid hub is used, adapt the EXECUTOR setting accordingly

	To configure other remote drivers for running on browserstack, find more information here:
	https://www.browserstack.com/automate/python#configure-capabilities

2. In help_tests.py verify the configuration of used WebDrivers:

	TESTS += define_testcases(

	        NavigationTest,
	        SearchTest,
	        FaqTest,
	        ContactFormTest,

	        LocalFirefox,          # <- Firefox on your machine
	        #LocalChrome,          # <- Chrome on your machine
	        #LocalIE,              # <- Internet Explorer on your machine
	        #drivers_desktop,      # <- Desktop browsers according to ConfigMatrix on browserstack
	        #drivers_mobile,       # <- Mobile devices according to ConfigMatrix on browserstack

	        SMALL,
	        DESKTOP,

	    )

	With mobile devices, the user:pass@ syntax in server URL does not work.
	For these tests ask Oliver or/to deactivate HTTP AUTH on his server and adapt SERVER string in globals.py

3.  Execute the help tests:

	> python helps_tests.py

	This will create test results in report_help_tests.html

	To run tests with XML output, use nose:

	> pip install nose
	> nosetests -v --with-xunit

4. Verify test execution and visual logs at browserstack if used:

	https://www.browserstack.com/automate


