from drivers import *
import help_tests
import sli_tests
import HTMLTestRunner


TESTS = define_testcases(

        sli_tests.WhatBlurredTest,
        sli_tests.ImageQualityTest,

        help_tests.NavigationTest,
        help_tests.SearchTest,
        help_tests.FaqTest,
        help_tests.ContactFormTest,

        LocalFirefox_EN, LocalFirefox_DE,
        #drivers_desktop,
        #drivers_mobile,

        SMALL,
        DESKTOP,
    )


if __name__ == "__main__":
    suite = unittest.TestSuite()
    for t in TESTS:
        suite.addTests( unittest.TestLoader().loadTestsFromTestCase(t) )
    HTMLTestRunner.HTMLTestRunner(stream=file('results_suite.html','wb'),verbosity=2).run(suite)
