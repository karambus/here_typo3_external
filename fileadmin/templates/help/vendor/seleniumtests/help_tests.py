# requirements:  pip install selenium
# run directly:  python help_tests.py
# nose/xml:      nosetests -v --with-xunit

from selenium.webdriver.common.keys import Keys

from drivers import *
from globals import SERVER
import HTMLTestRunner


class dummy_test(unittest.TestCase):
    pass

languages = ( 'bg_BG','da_DK','de_DE','el_GR','en_UK','en_US','es_ES','fi_FI','fr_FR','hu_HU','it_IT','nl_NL','no_NO','pl_PL','pt_BR','pt_PT','ro_RO','ru_RU','sv_SE','tr_TR','zh_CN','zh_TW' )



class HelpBase(TestCase):
    def home(self):
        try:
            WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='header']//a[@id='here']")),"Home link not found").click()
        except StaleElementReferenceException:
            WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='header']//a[@id='here']")),"Home link not found").click()
        self.verifyWait( lambda : self.driver.current_url.endswith("/help"), "Wrong URL: %s" % self.driver.current_url )
        self.check_language()
        return self
    def nav(self,i):
        it = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='header']//ul[@role='menu']/li[%d]/a" % i)), "Nav item %d not found" % i)
        if not it.is_displayed():
            self.driver.find_element_by_xpath("//section[@id='header']//a[@id='header-menu-mobile']").click()
            WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH,"//section[@id='header']//ul[@role='menu']/li[%d]/a" % i)), "Nav item %d not clickable (Menu not opened)" % i)
        self.driver.find_element_by_xpath("//section[@id='header']//ul[@role='menu']/li[%d]/a" % i).click()
        self.check_language()
        return self
    def sl_left(self):
        try:
            try:
                it = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='slider']//a[@id='slider-left']")), "Left arrow not found" )
                it.click()
            except StaleElementReferenceException:
                it = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='slider']//a[@id='slider-left']")), "Left arrow not found" )
                it.click()
        except:
            pass
        return self
    def sl_right(self):
        try:
            try:
                it = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='slider']//a[@id='slider-right']")), "Right arrow not found" )
                it.click()
            except StaleElementReferenceException:
                it = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='slider']//a[@id='slider-right']")), "Right arrow not found" )
                it.click()
        except:
            pass
        return self
    def sl_item(self,i):
        return WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//section[@id='slider']//div[@id='slider-items']/ul/li[%d]/a" % i)),"Slider item %d not found" % i)
    def sl_scroll(self,i):
        for x in xrange(4):
            self.sl_left()
        for x in xrange(i-1):
            self.sl_right()
            WebDriverWait(self.driver, 20).until( lambda driver : self.sl_item(x+2).is_displayed() , "Item not shown" )
        return self
    def sl_click(self,i):
        self.sl_scroll(i)
        WebDriverWait(self.driver, 20).until( lambda driver : self.sl_item(i).is_displayed() , "Slider item %d not displayed" % i )
        try:
            self.sl_item(i).click()
        except StaleElementReferenceException:
            self.sl_item(i).click()
        self.check_language()
        return self
    def search(self,text):
        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//input[@type='search'][@id='search-query']"))).clear()
        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH,"//input[@type='search'][@id='search-query']"))).send_keys( text + Keys.ENTER )
        self.check_language()
        return self
    def check_results(self,minResults):
        if minResults:
            try:
                WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH, "//div[@class='tx-indexedsearch-res']//div[@id='result']//div[@class='item']")))
                result = self.driver.find_elements_by_xpath("//div[@class='tx-indexedsearch-res']//div[@id='result']//div[@class='item']")
                return len(result)>=minResults
            except:
                return False
        else:
            try:
                WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH, "//p[@class='tx-indexedsearch-noresults']")))
                return True
            except:
                return False


class NavigationTest(HelpBase):
    def test_navigation(self):
        self.get("%s/help" % SERVER)
        self.verifyWait( lambda : self.driver.current_url.endswith("/help"), "Wrong URL: %s" % self.driver.current_url )
        self.nav(1)
        self.verifyWait( lambda : self.driver.current_url.endswith("/help/contact/"), "Wrong URL: %s" % self.driver.current_url )
        self.nav(2)
        self.verifyWait( lambda : self.driver.current_url.endswith("/help/nav-2/"), "Wrong URL: %s" % self.driver.current_url )
        self.nav(3)
        self.verifyWait( lambda : self.driver.current_url.endswith("/help/nav-3/"), "Wrong URL: %s" % self.driver.current_url )
        self.home()
        self.verifyWait( lambda : self.driver.current_url.endswith("/help"), "Wrong URL: %s" % self.driver.current_url )
        self.sl_click(1)
        self.home()
        self.sl_click(2)
        self.home()
        self.sl_click(3)
        #self.home()
        #self.sl_click(4)


class SearchTest(HelpBase):
    def check_search(self):
        self.search('HERE')
        self.assertTrue(self.check_results(2),"Not enough results")
        self.search('theory of everything')
        self.assertTrue(self.check_results(0),"Congratulations")
        self.search('HERE')
        self.assertTrue(self.check_results(2),"Not enough results")
    def test_search(self):
        self.get("%s/help" % SERVER)
        self.check_search()
        #no search field on contacts page
        #page.nav(1)
        #self.check_search(page)
        self.nav(2)
        self.check_search()
        self.nav(3)
        self.check_search()


class FaqTest(HelpBase):

    def verify_faq(self):
        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH, "//section[@id='content'][@class='questions']")), "FAQ not found")
        WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH, "//section[@id='content'][@class='questions']/ul[@class='root']/li")), "No FAQ items found")
        group_lis = self.driver.find_elements_by_xpath("//section[@id='content'][@class='questions']/ul[@class='root']/li")
        self.verify( len(group_lis)>0, "No FAQ items found" )
        for group_li in group_lis[:2]:
            group = group_li.find_element_by_xpath("h3")
            self.verify( group.is_displayed(), "FAQ group not visible" )
            self.verify( len(group.text.strip())>0, "Empty FAQ section header" )
            question_lis = group_li.find_elements_by_xpath("ul[@class='group']/li")
            self.verify( len(question_lis)>0, "No FAQ questions found in section" )
            self.verify( not question_lis[0].is_displayed(), "FAQ question not initially hidden" )
            group.click()
            self.verifyWait( lambda : question_lis[0].is_displayed(), "FAQ question list not opened" )
            for question_li in question_lis[:2]:
                question = question_li.find_element_by_xpath("h4")
                self.verify( question.is_displayed(), "FAQ question not shown" )
                self.verify( len(question.text.strip())>0, "Empty FAQ question" )
                answer = question_li.find_element_by_xpath("div[@class='answer']")
                self.verify( len(answer.find_elements_by_xpath("*"))>0, "Empty FAQ answer" )
                self.verify( not answer.is_displayed(), "FAQ answer not initially hidden" )
                question.click()
                self.verifyWait( lambda : answer.is_displayed(), "FAQ answer not shown" )
                question.click()
                self.verifyWait( lambda : not answer.is_displayed(), "FAQ answer not hidden" )
            group.click()
            self.verifyWait( lambda : not question_lis[0].is_displayed(), "FAQ question list not closed" )

    def test_faq_list(self):
        self.get("%s/help" % SERVER)
        self.sl_click(2)
        self.verify_faq()


class ContactFormTest(HelpBase):
    def check_send(self, enabled):
        sendBtn = WebDriverWait(self.driver, 20).until(EC.presence_of_element_located((By.XPATH, "//input[@type='submit'][@value='Send']")))
        if enabled:
            WebDriverWait(self.driver, 20).until( lambda driver : sendBtn.is_enabled(), "Send not enabled" )
        else:
            WebDriverWait(self.driver, 20).until( lambda driver : not sendBtn.is_enabled(), "Send enabled" )

    def input(self,id,keys):
        self.driver.find_element_by_xpath( "//input[@id='%s']" % id ).send_keys(keys)
    def text(self,id,keys):
        self.driver.find_element_by_xpath( "//textarea[@id='%s']" % id ).send_keys(keys)
    def select(self,id,i):
        self.driver.find_element_by_xpath( "//select[@id='%s']/option[%d]" % (id,i+1) ).click()

    def test_contacts_page(self):
        self.get("%s/help" % SERVER)
        self.nav(1)

        # Fill from top and check send button finally enabled
        self.check_send(False)
        self.input('powermail_field_name','test')
        self.check_send(False)
        self.input('powermail_field_emailaddress','test@test.test')
        self.check_send(False)
        self.select('powermail_field_application',1)
        self.check_send(False)
        self.select('powermail_field_version',1)
        self.check_send(False)
        self.select('powermail_field_devicetype',1)
        self.check_send(False)
        self.select('powermail_field_empty_marker_01',1)
        self.check_send(False)
        self.text('powermail_field_description','test')
        self.check_send(True)

        # Clear each field and fill again; check send button gets disabled and enabled again
        self.input('powermail_field_name', Keys.BACK_SPACE * 20)
        self.check_send(False)
        self.input('powermail_field_name', "test")
        self.check_send(True)

        self.input('powermail_field_emailaddress', Keys.BACK_SPACE * 20)
        self.check_send(False)
        self.input('powermail_field_emailaddress','test@test.test')
        self.check_send(True)

        self.select('powermail_field_application',0)
        self.check_send(False)
        self.select('powermail_field_application',1)
        self.check_send(True)

        self.select('powermail_field_version',0)
        self.check_send(False)
        self.select('powermail_field_version',1)
        self.check_send(True)

        self.select('powermail_field_devicetype',0)
        self.check_send(False)
        self.select('powermail_field_devicetype',1)
        self.check_send(True)

        self.select('powermail_field_empty_marker_01',0)
        self.check_send(False)
        self.select('powermail_field_empty_marker_01',1)
        self.check_send(True)

        self.text('powermail_field_description', Keys.BACK_SPACE * 20)
        self.check_send(False)
        self.text('powermail_field_description','test')
        self.check_send(True)

        # Check invalid email
        self.input('powermail_field_emailaddress', Keys.BACK_SPACE * 20)
        self.check_send(False)
        self.input('powermail_field_emailaddress', "test")
        self.check_send(False)
        self.input('powermail_field_emailaddress', ".test")
        self.check_send(False)
        self.input('powermail_field_emailaddress', "@")
        self.check_send(False)
        self.input('powermail_field_emailaddress', "test")
        self.check_send(False)
        self.input('powermail_field_emailaddress', ".")
        self.check_send(False)
        self.input('powermail_field_emailaddress', "test")
        self.check_send(True)

        # Finally click the button
        self.driver.find_element_by_xpath("//input[@type='submit'][@value='Send']").click()


TESTS = []
TESTS += define_testcases(

        NavigationTest,
        SearchTest,
        FaqTest,
        ContactFormTest,

        LocalFirefox_EN, LocalFirefox_DE,
        #drivers_desktop,
        #drivers_mobile,

        SMALL,
        DESKTOP,

    )


if __name__ == "__main__":
    suite = unittest.TestSuite()
    for t in TESTS:
        suite.addTests( unittest.TestLoader().loadTestsFromTestCase(t) )
    HTMLTestRunner.HTMLTestRunner(stream=file('results_help_tests.html','wb'),verbosity=2).run(suite)

