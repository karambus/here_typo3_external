<?php
define("DS",DIRECTORY_SEPARATOR);

if(strtolower(PHP_SAPI) !== "cli") {
    die("Forbidden");
}

// include composer autoload
if (file_exists("vendor/autoload.php")) {
	include("vendor/autoload.php");
}

// include database config
include("config/db.config.php");

function autoloadCustomClasses($class) {
    $file = __DIR__.DS."classes".DS.str_replace("_",DS,$class).".php";

    if(file_exists($file)) {
        include_once($file);
    }
}
spl_autoload_register("autoloadCustomClasses");



function handleError($errno, $errstr, $errfile, $errline) {

    switch ($errno) {
        case E_NOTICE:
            $errtype = 'E_NOTICE';
            break;

        case E_WARNING:
            $errtype = 'E_WARNING';
            break;

        case E_ERROR:
            $errtype = 'E_ERROR';
            break;

        case E_USER_ERROR:
            $errtype = 'E_USER_ERROR';
            break;

        case E_USER_WARNING:
            $errtype = 'E_USER_WARNING';
            break;

        case E_USER_NOTICE:
            $errtype = 'E_USER_NOTICE';
            break;

        default:
            $errtype = "E_UNKNOWN";
            break;
    }

    $E = new Exception($errtype.": ".$errstr);
    helpers::abort($E);

}

set_error_handler("handleError");
