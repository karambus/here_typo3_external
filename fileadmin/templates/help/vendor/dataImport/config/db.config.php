<?php
// FETCH CONFIG FROM TYPO3
$typo3conf = require_once(realpath("../../../../..") . DS . "typo3conf/LocalConfiguration.php");
$dbConf    = $typo3conf["DB"];

// DATABASE CONFIG
define("DB_DSN", sprintf(
    "mysql:host=%s;dbname=%s;charset=utf8",
    $dbConf["host"],
    $dbConf["database"]
));
define("DB_USERNAME", $dbConf["username"]);
define("DB_PASSWORD", $dbConf["password"]);
define("DB_INIT_COMMAND", "SET NAMES utf8");