# data import thing

## running

* run `php execute.php`
  this will return a list of processes seperated into:
  **available processes:**
  these are free to be used
  **not configured processes:**
  a processes where *processes/processname/config.php* is missing.
  in most cases a *processes/processname/sample.config.php* should exist within the folder.
  the *config.php* is needed in any case, even if it is an empty script


* run `php execute.php --process={processname}`
  this will basically include
  *processes/processname/config.php* and
  *processes/processname/execute.php*
   the *execute.php* can contain more parameter-requirements

## creating new processes

* create your processname within */processes*
  **example:** */processes/myprocess/*
* create */processes/myprocess/sample.config.php*
* create */processes/myprocess/execute.php*
* an do your import/whatever magix







