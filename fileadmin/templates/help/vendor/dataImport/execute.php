<?php
// include some usefull function
include "bootstrap.php";

// get available processes
$availableProcesses = array(
	"available"          => array(),
	"not configured yet" => array()
);
foreach (new DirectoryIterator("processes") as $process) {
	if (substr($process, 0, 1) == '.') {
		continue;
	}
	if (file_exists($process->getRealPath() . DS . "config.php") === false) {
		$availableProcesses["not configured"][] = $process->getBasename();
		continue;
	}
	$availableProcesses["available"][] = $process->getBasename();
}

// check for process process
$opts = getopt("", array('process:'));
if (isset($opts["process"]) === false OR in_array($opts["process"], $availableProcesses["available"]) === false) {
	if (isset($opts["process"]) === false) {
		helpers::outnl("process is a required parameter!", "red");
		helpers::outnl("example: ");
		helpers::out("php execute.php --process=", "cyan");
		helpers::out("processname", "light_green");
	} else {
		helpers::outnl("'" . $opts["process"] . "' is not a valid process!", "red");
		helpers::outnl("");
	}
	foreach ($availableProcesses as $key => $processes) {
		if (count($processes) > 0) {
			helpers::outnl($key . " processes: ");
			helpers::outnl(join(", ", $processes), "light_green");
			if ($key === "not configured") {
				helpers::getout("\n(you need to create a ") . helpers::getout("config.php", "yellow") . " from "
				. helpers::getout("sample.config.php", "yellow") . ")\nsee README.md for detailed instructions";
			}
			helpers::outnl("");
		}
	}
	helpers::abort();
}

// set the process
$processpath = "processes/" . $opts["process"] . "/";

// include the files
$configfile  = $processpath . "config.php";
$processfile = $processpath . "execute.php";

require $configfile;
require $processfile;


