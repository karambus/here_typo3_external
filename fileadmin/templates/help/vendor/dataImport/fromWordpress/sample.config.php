<?php
$cfgVersion = 201408291803;

// FETCH CONFIG FROM TYPO3
$typo3conf = require(realpath("../../..").DS."typo3conf/LocalConfiguration.php");
$dbConf = $typo3conf["DB"];

// DATABASE CONFIG
define("DB_DSN",sprintf(
    "mysql:host=%s;dbname=%s",
    $dbConf["host"],
    $dbConf["datebase"]
));
define("DB_USERNAME",$dbConf["username"]);
define("DB_PASSWORD",$dbConf["password"]);
define("DB_INIT_COMMAND","SET NAMES utf8");

// WORDPRESS XML EXPORT FILE
$importFilename = __DIR__."/import/here.wordpress.2014-08-05.xml";