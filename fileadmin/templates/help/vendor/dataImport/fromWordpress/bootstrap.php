<?php
define("DS",DIRECTORY_SEPARATOR);

// some useful functions
require_once(__DIR__.DS."functions".DS."helpers.php");

// include the config file
$configfile = __DIR__.DS."config.php";
// abort if config does not exist
if(file_exists($configfile) === false) {
    abort("Please create the mandatory config file: 'config.php' from 'config.php.sample'!");
}

// include the config file
$configfile = __DIR__.DIRECTORY_SEPARATOR."config.php";
// abort if config does not exist
if(file_exists($configfile) === false) {
    abort("Please create the mandatory config file: 'config.php' from 'config.php.sample'!");
}

// get starting point pid
$opts = getopt("",array('id:'));
if(isset($opts["id"]) === false) {
    die("\nid is a required parameter!\nexample: php execute.php --id=123123\nstopped execution in ".__FILE__.":".__LINE__."\n");
}
define("STARTING_PID",$opts["id"]);

// include relevant stuff
require_once __DIR__."/vendor/autoload.php";

require_once __DIR__ . "/classes/importItem.php";
require_once __DIR__ . "/classes/languageHelper.php";
require_once __DIR__ . "/classes/itemRegistry.php";
require_once __DIR__ . "/classes/importer.php";
require_once __DIR__ . "/classes/table/content.php";
require_once __DIR__ . "/classes/table/page.php";
require_once __DIR__ . "/classes/table/pagelang.php";
