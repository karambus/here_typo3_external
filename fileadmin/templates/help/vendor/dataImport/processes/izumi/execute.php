<?php
// get starting point pid
$opts = getopt("", array('id:', 'help', 'clear'));
if (isset($opts["id"]) === false) {
	helpers::outnl("\nid is a required parameter!", "red");
	$opts["help"] = true;
}

if (isset($opts["help"]) === true) {
	helpers::outnl("");
	helpers::outnl("example:");
	helpers::outnl("php execute.php --process=izumi --id=123123 [--clear]", "green");
	helpers::outnl("-----------------------------------------------------");
	helpers::outnl("     --clear   truncate herehelp tables");
	helpers::outnl("      --help   display this");
	helpers::outnl("  --id=[0-9]   set starting point id, parent pid");
	helpers::outnl("-----------------------------------------------------");
	helpers::outnl("");
	helpers::abort("help displayed");
}

// this is the very parent PID
define("STARTING_PID", $opts["id"]);

// modify these projects to automaticaly eigher create or remove products
$modifyProjectIdentifier = array(
	'mh5'        => 'mh5.maps',
	'w81.help'   => 'w81.maps',
	'web.legacy' => 'web',
	'aol.maps.help'   => 'NokiaX.maps',
	'suite.andr.help' => 'HEREapp.maps',
);

// save content to cache
$cacheRootPath = "data";
chdir(__DIR__);

if ($skipExport !== true) {

	// fetch my projects
	$fetcher       = new izumiFetcher($izumiUsername, $izumiPassword);
	$projectsJson  = $fetcher->fetch('https://izumiapp.com/user/projects.json');
	$usersProjects = json_decode($projectsJson, true);

	if (isset($usersProjects["projects"]) === true AND count($usersProjects["projects"]) > 0) {
		if (isset($usersProjects["projects"]["mine"]) === true AND count($usersProjects["projects"]["mine"]) > 0) {
			$usersProjects = $usersProjects["projects"]["mine"];
		}
		$usersProjects = $usersProjects["projects"];
	} else {
		helpers::abort("no projects were fetched, check your credentials provided in the config.php");
		helpers::outnl("given data:", "yellow");
		helpers::outnl($projectsJson);
	}

	// validate user projects
	if (isset($usersProjects[0]["identifier"]) === false) {
		helpers::outnl("projects have unknows sctucture, please apply modifications to this import", "red");
		helpers::outnl("given data", "yellow");
		helpers::outnl($projectsJson);
	}

	if ($exportFlushCache === true) {
		rmdir($cacheRootPath);
	}
	include_once "inc/export-from-izumi.php";
	exportFromIzumi($fetcher, $usersProjects, $cacheRootPath);
}

// open the db instance
$db = DB::getInstance();

if (isset($opts["clear"]) === true) {
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_answers");
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_apps");
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_faqs");
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_questions");
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_subsections");
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_sections");
	$db->query("TRUNCATE TABLE tx_herehelp_domain_model_customurls");

	helpers::outnl("\ntables truncated", "yellow");
}

helpers::outnl("\nstarting import into TYPO3", "purple");

// create sections
$SMARTPHONE   = null;
$WEB          = null;
$TABLET       = null;
$PRIVACY_MORE = null;
foreach (array("Smartphone", "Web", "Tablet", "Privacy & more") as $key) {
	$var  = strtoupper(helpers::transformToAlnum($key, "_"));
	$$var = new dbRow_sections();
	//    $$var->findByField('title',$key,true);
	if ($$var->exists() === false) {
		$$var->setTitle($key);
		$$var->setBackendtitle($key);
		$$var->setPid(STARTING_PID);
		$$var->setIcon(strtolower(helpers::dashify($key)) . ".png");
		$$var->update();
	}

	var_dump($$var->getUID());
	itemRegistry::set("section_" . $$var->getUID(), $$var);
	itemRegistry::set("section_" . $var, $$var);
}

/** @var SplFileInfo $languageCC */
foreach (new RecursiveDirectoryIterator($cacheRootPath) as $languageCC) {
	$projectIdentifierDirectory = $languageCC->getBasename();
	$projectIdentifier          = trim($projectIdentifierDirectory);
	$languageLocale             = $projectIdentifierDirectory;

	if (substr($projectIdentifier, 0, 1) === ".") {
		continue;
	}
	helpers::outnl("\nprocessing subsection " . $projectIdentifier, "blue");

	// modify the identifier if renaming is configured
	if (isset($modifyProjectIdentifier[$projectIdentifier]) === true) {
		$projectIdentifier = $modifyProjectIdentifier[$projectIdentifier];
		helpers::out(" and renamed to " . $projectIdentifier, "red");
	}

	$chunks = explode(".", $projectIdentifier);
	if (!is_array($chunks)) {
		$chunks = array($projectIdentifier);
	}

	// just remove the help thing, since it is not needed
	$values = array_diff($chunks, array("help"));

	if (count($values) === 2) {
		list($subsectionName, $productName) = $values;
	} else {
		// in general skipp app / product if obviously no app exists
		$productName = null;
		list($subsectionName) = $values;
	}

	// get the language
	$languageUID = languageHelper::getLanguageUID($languageLocale);

	// create subsections
	$SUBSECTION = new dbRow_subsections();
	$importItem = new importItem_izumi(null);
	$importItem
		->setTitle(copyHelper::resolveAbbr($subsectionName))
		->setPid(STARTING_PID)
		->setApps("")
		->setPaths("");

	// resolve the subsection to a section, unfortunately this needs to be hardcoded
	// get the parent section based on subsectionName
	if ($subsectionName == "w81") {
		$SECTION = $TABLET;
	} elseif ($subsectionName == "web") {
		$SECTION = $WEB;
	} else {
		$SECTION = $SMARTPHONE;
	}

	$SUBSECTION->setDataFromImportItem($importItem)
		->setSections($SECTION->getUID())
		->setBackendtitle($SECTION->getBackendtitle() . " | " . copyHelper::resolveAbbr($subsectionName))
		->setIcon(helpers::dashify($subsectionName) . '.png')
		->setPaths(helpers::dashify($subsectionName));

	// check if this subsection has already been created
	$SUBSECTIONEXISTS = itemRegistry::get("subsection_" . $SUBSECTION->getTitle());
	if ($SUBSECTIONEXISTS instanceof dbRow) {
		$SUBSECTION = $SUBSECTIONEXISTS;
		helpers::outnl(
			"subsection #" . $SUBSECTION->getUID() . " " . $SUBSECTION->getTitle() . " updated",
			"light_gray"
		);
	} else {
		$SUBSECTION->update();
		itemRegistry::set("subsection_" . $SUBSECTION->getUID(), $SUBSECTION);
		itemRegistry::set("subsection_" . $SUBSECTION->getTitle(), $SUBSECTION);
		helpers::outnl("subsection #" . $SUBSECTION->getUID() . " " . $SUBSECTION->getTitle() . " created", "green");

		// update the subsection count in sections
		$SECTION->setSubsections(($SECTION->getSubsections() + 1))
			->update();
	}

	// create app
	if (is_null($productName) === false) {
		$APP        = new dbRow_apps();
		$importItem = new importItem_izumi(null);
		$appTitle = copyHelper::resolveAbbr($subsectionName.".".$productName);
		if($appTitle === $subsectionName.".".$productName) {
			$appTitle = copyHelper::resolveAbbr($productName);
		}
		$importItem
			->setPid(STARTING_PID)
			->setTitle($appTitle)
			->setFaqs("")
			->setPaths(helpers::pathify($productName));
		$APP->setDataFromImportItem($importItem)
			->setIcon(helpers::dashify($subsectionName . "." . $productName) . '.png')
			->setBackendtitle($SUBSECTION->getBackendtitle() . " | " . $APP->getTitle())
			->setSubsections($SUBSECTION->getUID())
			->update();
		itemRegistry::set("apps_" . $APP->getUID(), $APP);

		helpers::outnl("app #" . $APP->getUID() . " " . $APP->getTitle() . " created", "green");

		// update the amount of apps in subsection
		$SUBSECTION->setApps(($SUBSECTION->getApps() + 1))
			->update();
	} else {
		unset($APP);
	}

	$projectPath = $cacheRootPath . "/" . $projectIdentifierDirectory;

	// prepare the bundles
	$availableLanguages = array();
	foreach (new RecursiveDirectoryIterator($projectPath) as $fileInfo) {
		$languageIdentifier = $fileInfo->getBasename(".xml");
		if ($fileInfo->getExtension() !== "xml") {
			continue;
		}
		$availableLanguages[$languageIdentifier] = $fileInfo->getRealPath();
	}

	// check if default language is available and push it at the first place to create default pages
	if (isset($availableLanguages[$defaultLanguage]) === true) {
		$tmpAvailableLanguages = $availableLanguages;
		$defaultLanguageArray  = array($defaultLanguage => $availableLanguages[$defaultLanguage]);
		unset($tmpAvailableLanguages[$defaultLanguage]);
		if (count($tmpAvailableLanguages) > 0) {
			$availableLanguages = array_merge($defaultLanguageArray, $tmpAvailableLanguages);
		}
	} else {
		helpers::outnl("\n default language " . $defaultLanguage . " not available, skipping this section", 'red');
		continue;
	}

	// process the items for this project
	foreach ($availableLanguages as $languageCC => $languageFileRealPath) {
		$languageIdentifier = $languageCC;

		$languageUID = languageHelper::getLanguageUID($languageIdentifier);

		// skip unresolved languages
		if ($languageUID === false) {
			helpers::outnl("language for " . $languageIdentifier . " not found, skipping", "red");
			continue;
		} else {
			helpers::outnl("\nprocessing language " . $languageIdentifier . " (uid: ".$languageUID.")", "green");
		}

		if ($languageUID === 0) {
			unset($FAQParentUID);
		}

		$res = helpers::isValidXmlFile($languageFileRealPath);
		if ($res === true) {
			$xml = simplexml_load_file($languageFileRealPath);
			helpers::outnl("processing languagefile " . $projectIdentifierDirectory . "/" . $languageIdentifier . ':');
			$importItem = new importItem_izumi($xml);
		}

		$data = $importItem->getData();

		$catcount = 0;
		if (isset($data["categories"]) === true) {
			$catcount = count($data["categories"]);
		}

		if ($catcount > 0) {

			foreach ($data["categories"] as $category) {
				$FAQ = new dbRow_faqs();
				$FAQ->setPid(STARTING_PID);
				$FAQ->setKey($category["key"]);
				$FAQ->setDisplay($category["display"]);

				if ($languageIdentifier === 'en-GB') {
					$languageUID = '0';
				}

				$FAQ->setLanguageUID($languageUID);
				$FAQ->setPaths($category["paths"]);

				// if an app exists, set the apps.uid and raise the faq count
				if (isset($APP)) {
					$FAQ->setApps($APP->getUID());
					$APP->setFaqs(($APP->getFaqs() + 1))
						->update();
					// if there is no app, set the subsections.uid and raise the faq count
				} else {
					$FAQ->setSubsections($SUBSECTION->getUID());
					$SUBSECTION->setFaqs(($SUBSECTION->getFaqs() + 1))
						->update();
				}

				$FAQ->setHeadline($category["title"]);
				$FAQ->setTitle($category["title"]);
				$FAQ->update();

				if (count($category["questions"]) > 0) {
					foreach ($category["questions"] as $question) {

						$ANSWER = new dbRow_answers();
						$ANSWER
							->setPid(STARTING_PID)
							->setLanguageUID($languageUID)
							->setAnswer($question["content"])
							->update();

						$QUESTION = new dbRow_questions();
						$QUESTION->setPid(STARTING_PID)
							->setFaqs($FAQ->getUID())
							->setKey($question["key"])
							->setLanguageUID($languageUID)
							->setQuestion($question["title"])
							->setPaths($question["paths"])
							->setAnswers($ANSWER->getUID())
							->update();
						itemRegistry::set("QUESTION" . $QUESTION->getUID(), $QUESTION);
						itemRegistry::set(
							"QUESTION_" . $languageUID . "_" . $projectIdentifier . "_" . $FAQ->getKey() . "_"
							. $QUESTION->getKey(),
							$QUESTION
						);

						$parentsCheck = itemRegistry::get(
							"QUESTION_0_" . $projectIdentifier . "_" . $FAQ->getKey() . "_" . $QUESTION->getKey()
						);
						if ($parentsCheck AND $languageUID > 0) {
							$QUESTION->setL10nParent($parentsCheck->getUID())
								->update();
							$ANSWER->setL10nParent($parentsCheck->getAnswers())
								->update();
						}

						// update the amount of faq in apps or subsections
						$FAQ->setQuestions(($FAQ->getQuestions() + 1));
					}
				}

				$checkIfParent = itemRegistry::get("FAQ_0_" . $projectIdentifier . "_" . $FAQ->getKey());
				if ($checkIfParent !== null AND $checkIfParent !== false AND $languageUID > 0) {
					$FAQ->setL10Parent($checkIfParent->getUID());
				}

				$FAQ->update();
				itemRegistry::set("FAQ_" . $FAQ->getUID(), $FAQ);
				itemRegistry::set("FAQ_" . $languageUID . "_" . $projectIdentifier . "_" . $FAQ->getKey(), $FAQ);

				$parentsCheck = itemRegistry::get("FAQ_0_" . $projectIdentifier . "_" . $FAQ->getKey());
				if ($parentsCheck AND $languageUID > 0) {
					$FAQ->setL10nParent($parentsCheck->getUID())
						->update();
				}
			}
		}
	}
}
