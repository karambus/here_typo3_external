<?php
// IZUMI ACCESS CONFIG
$izumiUsername = "someusername";
$izumiPassword = "somepassword";

// RUNTIME SETTINGS
// skip import into databse
$skipImport = false;
// skip export from izumi
$skipExport = false;
// clean _cache before the import
// this will only happen, if import is not skipped
$exportFlushCache = false;

// IMPORT CONFIGURATION
$defaultLanguage = "en-GB";