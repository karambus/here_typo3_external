<?php
/**
 * @param $fetcher
 *
 * @param $usersProjects
 * @param $cacheRootPath
 *
 * @return void
 */
function exportFromIzumi($fetcher, $usersProjects, $cacheRootPath) {
	// iterate through all projects and fetch all data needed

	foreach ($usersProjects as $projectData) {

		$cachePath = $cacheRootPath;

		// skip this project if no identifier or name is available
		if (isset($projectData["identifier"]) === false OR isset($projectData["name"]) === false) {
			$fetcher->log("skipped invalid project: " . var_export($projectData, 1) . "\n", "yellow");
			continue;
		}

		$projectName       = $projectData["name"];
		$projectIdentifier = $projectData["identifier"];

		// create cache folder
		helpers::appendDir($cachePath, $projectIdentifier);

		// fetch the bundles first
		$projectBundlesUrl = sprintf(
			"https://izumiapp.com/%s/text-sets/bundles.json",
			$projectIdentifier
		);

		$projectBundlesJson = $fetcher->fetch($projectBundlesUrl);
		$projectBundles     = json_decode($projectBundlesJson, true);

		if (isset($projectBundles) === false) {
			$fetcher->log("invalid bundles received: " . $projectBundlesJson . "\n", "red");
		}

		// continue if we have some locales
		if (isset($projectBundles["locales"]) AND count($projectBundles["locales"]) > 0) {

			// get passed revision
			$currentBundleRevision = null;
			if (isset($projectBundles["revision"])) {
				$currentBundleRevision = $projectBundles["revision"];
			}

			// check if cache is outdated
			$cachedBundleRevision     = null;
			$cachedBundleRevisionFile = $cachePath . DS . "revision.txt";
			if (file_exists($cachedBundleRevisionFile) === true) {
				$cachedBundleRevision = file_get_contents($cachedBundleRevisionFile);
			}

			// skip if current revision matches the cached revision
			if (!is_null($currentBundleRevision) AND
				!is_null($cachedBundleRevision) AND (int)$currentBundleRevision === (int)$cachedBundleRevision
			) {
				$fetcher->log("revision " . $currentBundleRevision . " already cached, skipping\n", "cyan");
				continue;
			}

			// iterate thourgh all available locales and fetchcache the content
			foreach ($projectBundles["locales"] as $bundleLocale) {

				$bundleLocaleUrl = sprintf(
					"https://izumiapp.com/%s/help/content/%s.xml",
					$projectIdentifier,
					$bundleLocale
				);

				$localeXmlContent = $fetcher->fetch($bundleLocaleUrl);

				// test is xml
				simplexml_load_string($localeXmlContent);

				// save the content
				$cachedLocaleXmlFile = $cachePath . DS . $bundleLocale . ".xml";
				if (file_put_contents($cachedLocaleXmlFile, $localeXmlContent)) {
					$fetcher->log(strlen($localeXmlContent) . " written to " . $cachedLocaleXmlFile . "\n");
				}
			}

			// write revision to file after all content has been fetched
			file_put_contents($cachedBundleRevisionFile, $currentBundleRevision);
		}
	}
}

