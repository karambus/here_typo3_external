<?php
// get starting point pid
$opts = getopt("", array('id:', 'help', 'clear'));
if (isset($opts["id"]) === false) {
	helpers::outnl("\nid is a required parameter!", "red");
	$opts["help"] = true;
}

// this is the very parent PID
define("STARTING_PID", $opts["id"]);


// create dom document from wordpres xml
$importXML = new DOMDocument("1.0","UTF-8");
$importXML->load($importFilename);

// create array from domdocument
$res = \LSS\XML2Array::createArray($importXML);

$items = array();
// iterate through all items
foreach($res['rss']['channel']["item"] as $item) {
	// create a import instance, where the data will be parsed
	$inst = new importItem_wordpress($item);
	$id = $inst->getPostId();
	// ignore all unpublished pages and other pages except legal
	if($inst->isTemplate("page-templates/legal.php") === false OR $inst->isPublished() === false) continue;
	$items[$id] = $inst;
	// register the items for later usage
	itemRegistry::set($id, $inst);
}

// update all references to the other pages
// since we have all instances created at this point
foreach($items as $id => $inst) {
	$inst->updateReferences();
}

echo "\n".count($items)." parsed\n\n";







// create the starting point page (aka pages folder?)
$page = new table_page();
$page->setUid(STARTING_PID);
$page->executeImport();
$parentUID = $page->getUid();

// create all pages
/** @var $inst importItem_wordpress */
foreach($items as $id => $inst) {



	// create the page
	$page = new table_page($inst);
	$page->setPid(STARTING_PID);
	$page->executeImport();

	// create the content for default "en-US"
	$basecontent = new table_content($inst);
	$basecontent->setPid($page->getUid());
	$basecontent->setL18nParent(0);
	$syslangid = languageHelper::getDatabaseLanguageUIDForLocale('en-US');
	$basecontent->setSysLanguageUid($syslangid);
	$basecontent->executeImport();

	// create the translations @returns pages_languages_overlay.uid
	//    pages_languages_overlay.pid = pages.uid
	//    pages_languages_overlay.sys_language_uid =

	// create the content @returns
	//    tt_content.uid,
	//    tt_content.pid = pages.uid
	//    tt_content.l18n_parent = tt_content.uid

	// create the content
	$cnt = new table_content($inst);
	$cnt->executeImport();

	// assign the content
	$lng = new table_pagelang($inst);
	$lng->executeImport();

}
