<?php

# Here API's:
$URL_CITIES = 'http://transit.api.here.com/coverage/v1/city.json?accessId=f324d6794236cb6c425e74cf521dea04&client=mayflowerfaq&details=1';
$URL_REVGEO = 'http://reverse.geocoder.api.here.com/6.2/reversegeocode.json?app_id=03CEhMvn9yEUiM5dz42s&token=wTlRASICYCa4A9HyOHs5_g&mode=retrieveAreas';

# Use only for testing:
#$USE_FILE_CACHE = true;
#$URL_CITIES = 'http://integration.rnd.transit.api.here.com/coverage/v1/city.json?accessId=f324d6794236cb6c425e74cf521dea04&client=mayflowerfaq&details=1&max=10';
