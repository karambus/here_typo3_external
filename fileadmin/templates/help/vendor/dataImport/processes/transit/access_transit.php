<?php

# Demo showing access to transit cities data

$T3C = require_once(__DIR__."/../../../../../../../typo3conf/LocalConfiguration.php");
$DB = new PDO(sprintf("mysql:host=%s;dbname=%s;charset=utf8",$T3C["DB"]["host"],$T3C["DB"]["database"]),$T3C["DB"]["username"],$T3C["DB"]["password"]);
#$DB = new PDO('mysql:host=127.0.0.1;dbname=here61;charset=utf8','root','');

/**
 * Returns same language if supported, 'en' otherwise.
 */
function mapLanguage( $isoLanguage ) {
    global $DB;
    $count = $DB->query(
        sprintf(
            "SELECT DISTINCT isoLanguage
             FROM tx_herehelp_transit_cities
             WHERE isoLanguage=%s
             "
             ,$DB->quote($isoLanguage)
           ) )->rowCount();
    return $count>0 ? $isoLanguage : 'en';
}

/**
 * Returns list of covered regions in given language.
 */
function getTransitRegions( $isoLanguage ) {
    global $DB;
    return $DB->query(
        sprintf(
            "SELECT DISTINCT isoRegion,regionName
             FROM tx_herehelp_transit_cities
             WHERE isoLanguage=%s
             ORDER BY regionName ASC
             "
             ,$DB->quote($isoLanguage)
           ) )->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Returns list of covered countries for given region in given language.
 */
function getTransitCountries( $isoLanguage, $isoRegion ) {
    global $DB;
    return $DB->query(
        sprintf(
            "SELECT DISTINCT isoCountry,countryName
             FROM tx_herehelp_transit_cities
             WHERE isoLanguage=%s AND isoRegion=%s
             ORDER BY countryName ASC
             "
             ,$DB->quote($isoLanguage)
             ,$DB->quote($isoRegion)
           ) )->fetchAll(PDO::FETCH_ASSOC);
}

/**
 * Returns list of covered cities with quality for given country in given language.
 */
function getTransitCities( $isoLanguage, $isoCountry ) {
    global $DB;
    return $DB->query(
        sprintf(
            "SELECT DISTINCT id,cityName,quality
             FROM tx_herehelp_transit_cities
             WHERE isoLanguage=%s AND isoCountry=%s
             ORDER BY cityName ASC
             "
             ,$DB->quote($isoLanguage)
             ,$DB->quote($isoCountry)
           ) )->fetchAll(PDO::FETCH_ASSOC);
}

print "\nmapLanguage('de'):\n". mapLanguage("de")."\n";
print "\nmapLanguage('zh'):\n". mapLanguage("zh")."\n";
print "\nmapLanguage('xx'):\n". mapLanguage("xx")."\n";

print "\ngetTransitRegions(en):\n";
print_r( getTransitRegions("en") );

print "\ngetTransitCountries(en,EU):\n";
print_r( getTransitCountries("en","EU") );

print "\ngetTransitCities(en,DEU):\n";
print_r( getTransitCities("en","DEU") );

?>
