<?php

# 1.) Verify database connection configuration in:
#     ../../../../typo3conf/LocalConfiguration.php
# 2.) goto ../.. and run:
#     php execute.php --process=transit

$opts = getopt("", array('help', 'clean', 'full'));
if (isset($opts["help"])) {
    print "\n";
    print "Usage:\n";
    print "  php execute.php --process=transit [--clean] [--full]\n";
    print "\n";
    print "Options:\n";
    print "  --clean   drop tables and exit\n";
    print "  --full    do full update\n";
    print "  --help    show this text\n";
    print "\n";
    exit(0);
}

# Data files:
$COUNTRY_2_REGION = json_decode(file_get_contents(__DIR__.'/data/country2region.json'),true);
$REGION_NAMES = json_decode(file_get_contents(__DIR__.'/data/regionNames.json'),true);
$QUALITY_NAMES = json_decode(file_get_contents(__DIR__.'/data/qualityNames.json'),true);

# Database defines:
$TABLE_CITIES ="tx_herehelp_transit_cities";
$TABLE_METADATA ="tx_herehelp_transit_metadata";
$TABLE_REVGEO ="tx_herehelp_transit_revgeo";
$MAX_AGE_REVGEO = 30*24*60*60;  # 30 days
$KEY_LAST_UPDATE = "LAST_UPDATE";

print "DB-Connection: DB_DSN='".DB_DSN."', DB_USERNAME='".DB_USERNAME."'\n";
$db = DB::getInstance();
# Or emergency shortcut:
# $db = new PDO('mysql:host=127.0.0.1;dbname=here61;charset=utf8','root','root');

# Drop tables if requested
if (isset($opts["clean"])) {
    print "Dropping tables...\n";
    $db->query("DROP TABLE IF EXISTS $TABLE_CITIES, $TABLE_METADATA, $TABLE_REVGEO");
    print "Done.\n";
    exit(0);
}

# Create tables if needed
# ts DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
$db->query(
    "CREATE TABLE IF NOT EXISTS $TABLE_CITIES (
        id CHAR(5),
        isoLanguage CHAR(2),
        isoRegion CHAR(2),
        isoCountry CHAR(3),
        regionName VARCHAR(255),
        countryName VARCHAR(255),
        cityName VARCHAR(255),
        latitude VARCHAR(255),
        longitude VARCHAR(255),
        quality VARCHAR(255),
        ts TIMESTAMP,
        PRIMARY KEY(id,isoLanguage)
     )");
$db->query(
    "CREATE TABLE IF NOT EXISTS $TABLE_REVGEO (
        id CHAR(5),
        isoLanguage CHAR(2),
        data TEXT,
        ts TIMESTAMP,
        PRIMARY KEY(id,isoLanguage)
     )");
$db->query(
    "CREATE TABLE IF NOT EXISTS $TABLE_METADATA (
        id VARCHAR(255),
        data VARCHAR(255),
        ts TIMESTAMP,
        PRIMARY KEY(id)
     )");


/**
 * Get revgeo address, cached.
 */
function get_revgeo_address($id,$isoLanguage,$latitude,$longitude,$update=false) {
    global $TABLE_REVGEO, $MAX_AGE_REVGEO, $URL_REVGEO, $db;
    if ( !$update && ($res=$db->query(sprintf("SELECT data, ts FROM $TABLE_REVGEO WHERE id=%s AND isoLanguage=%s",$db->quote($id),$db->quote($isoLanguage)) )->fetch(PDO::FETCH_ASSOC)) && $res['data'] ) {
        if ( time() - $MAX_AGE_REVGEO/2 - mt_rand()/mt_getrandmax()*$MAX_AGE_REVGEO/2 < strtotime($res['ts']) ) {
            try {
                return json_decode($res['data']);
            } catch (Exception $e1) {}
        }
    }
    try {
        $data = json_encode( json_decode( get_data($URL_REVGEO . "&prox=$latitude,$longitude&language=$isoLanguage") )->Response->View["0"]->Result["0"]->Location->Address );
        $db->query( sprintf( "REPLACE INTO $TABLE_REVGEO (id,isoLanguage,data) VALUES (%s,%s,%s)",$db->quote($id),$db->quote($isoLanguage),$db->quote($data)) );
        return json_decode($data);
    } catch (Exception $e2) {
    }
}


/**
 * Fetch data from URL, maybe cached on file system
 */
function get_data($url,$update=false) {
    global $USE_FILE_CACHE;
    if (!$USE_FILE_CACHE) return file_get_contents($url);
    $CACHE_DIR = __DIR__.'/cache'; if (!is_dir($CACHE_DIR)) mkdir($CACHE_DIR, 0755, true);
    $fname = "$CACHE_DIR/" . md5($url);
    if ( $update || !file_exists("$fname") || filesize($fname)==0 ) file_put_contents($fname,file_get_contents($url));
    return file_get_contents($fname);
}


/**
 * Build quality description based on operator routing types
 */
function build_quality($isoLanguage,$types) {
    global $QUALITY_NAMES;
    $result = ""; $sep = "";
    foreach ( array('RT','TT','SR') as $itype => $type ) {
        if ( isset($types[$type]) ) {
            $result .= $sep . $QUALITY_NAMES[$isoLanguage][$type];
            $sep = ", ";
        }
    }
    return $result;
}

# Read last update timestamp, request data newer than last update - 1 day overlap
if ( !isset($opts["full"]) && ($res=$db->query(sprintf("SELECT data FROM $TABLE_METADATA WHERE id=%s",$db->quote($KEY_LAST_UPDATE)))->fetch(PDO::FETCH_ASSOC)) && $res['data'] ) {
    print "Requesting updates since: " . $res['data'] . "\n";
    $PAR_TIME = '&time=' . date( "Y-m-d\TH:i:s", strtotime($res['data']) - 60*60*24 );
}else{
    print "Requesting full data set\n";
    $PAR_TIME = '';
}

$result = json_decode( get_data( $URL_CITIES . $PAR_TIME ) )->Res;
if (isset($result->Message)) {
    print "Response message: " . $result->Message->{'$'} . "\n";
    if (!isset($result->Coverage)) {
        print "Aborted.\n";
        exit(0);
    }
}

foreach ($result->Coverage->Cities->City as $icity => $city) {

    # Check needed fields
    $id = $city->{'@id'};
    $countryName = $city->{'@country'};
    $cityName = $city->{'@name'};
    $latitude = $city->{'@y'};
    $longitude = $city->{'@x'};
    if ( !($id && $countryName && $cityName && $latitude && $longitude) ) {
        print "WARNING: Invalid city:\n";
        print_r($city);
        continue;
    }

    # Collect routing types from operators
    $types = array();
    if ( isset($city->Operators) ) {
        foreach ($city->Operators->Op as $ioperator => $operator) {
            $types[$operator->{"@type"}] = 1;
        }
    }else{
        $types['SR'] = 1;
    }

    # Get isoCountry, isoRegion, and regionName
    $address = get_revgeo_address($id,'en',$latitude,$longitude);
    if ( !($address) ) {
        print "WARNING: REVGEO lookup (en) failed for city:\n";
        print_r($city);
        continue;
    }
    $isoCountry = $address->Country;
    if ( !($isoCountry) ) {
        print "WARNING: isoCountry not found for city:\n";
        print_r($city);
        print_r($address);
        continue;
    }
    $isoRegion = $COUNTRY_2_REGION[$isoCountry];
    $regionName = $REGION_NAMES['en'][$isoRegion];
    if ( !($isoRegion && $regionName) ) {
        print "WARNING: region not found for city: (isoCountry=$isoCountry)\n";
        print_r($city);
        continue;
    }

    print "Import: ". $id .", ".$isoRegion .", ". $isoCountry .", ". $regionName .", ". $countryName .", ". $cityName  .", ". $latitude .", ". $longitude .", ". build_quality('en',$types) ."\n";

    foreach ($REGION_NAMES as $isoLanguage => $regionNames) {

        # Get names in isoLanguage
        $address = get_revgeo_address($id,$isoLanguage,$latitude,$longitude);
        if ( !($address) ) {
            print "WARNING: REVGEO lookup (". $isoLanguage .") failed for city:\n";
            print_r($city);
            continue;
        }
        $regionNameAlt = $regionNames[$isoRegion];
        $cityNameAlt = $address->City;
        $countryNameAlt = $countryName;
        foreach ($address->AdditionalData as $ientry => $entry) {
            if ($entry->key == "CountryName") {
                $countryNameAlt = $entry->value;
                break;
            }
        }

        # Create routing quality string in isoLanguage based on routing types from operators
        $quality = build_quality($isoLanguage,$types);

        # Write to database
        $query = sprintf(
            "REPLACE INTO $TABLE_CITIES (id,isoLanguage,isoRegion,isoCountry,regionName,countryName,cityName,latitude,longitude,quality) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
            $db->quote($id),
            $db->quote($isoLanguage),
            $db->quote($isoRegion),
            $db->quote($isoCountry),
            $db->quote($regionNameAlt),
            $db->quote($countryNameAlt),
            $db->quote($cityNameAlt),
            $db->quote($latitude),
            $db->quote($longitude),
            $db->quote($quality)
        );

        #print $query ."\n";
        $db->query( $query );
    }
}

# Update last update timestamp
$db->query( sprintf( "REPLACE INTO $TABLE_METADATA (id,data) VALUES (%s,CURRENT_TIMESTAMP)", $db->quote($KEY_LAST_UPDATE)) );
print "Done.\n";

?>
