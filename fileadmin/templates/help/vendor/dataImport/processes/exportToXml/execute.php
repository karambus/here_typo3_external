<?php

include("inc/exportHelper.php");
include("inc/queries.php");

print "DB-Connection: DB_DSN='".DB_DSN."', DB_USERNAME='".DB_USERNAME."'\n";

$writer = new XMLWriter();
$hlp = new exportHelper();

$writer->openMemory();
$writer->startDocument('1.0','UTF-8');
$writer->setIndent(true);
$writer->setIndentString("  ");

$writer->startElement("xliff");
	$writer->writeAttribute('xmlns', 'urn:oasis:names:tc:xliff:document:1.2');
	$writer->writeAttribute('xmlns:izumi', 'http://izumiapp.com/xliff');
	$writer->writeAttribute('version', '1.2');

	$writer->startElement("file");
		$writer->writeAttribute('tool-id', 'izumi');
		$writer->writeAttribute('datatype', 'plaintext');
		$writer->writeAttribute('date', gmdate("Y-m-d\TH:i:s\Z"));
		$writer->writeAttribute('izumi:processing-time', '');
		$writer->writeAttribute('product-name', 'help');
		$writer->writeAttribute('original', 'en-GB.xlf');
		$writer->writeAttribute('source-language', 'en-GB');
		$writer->writeAttribute('target-language', 'en-GB');

		$hlp->writeHeader($writer);

		$writer->startElement("body");

			// adding faqs with full section/subsection/app path
			$list = array();
			while ( $row = $faqs->fetch(PDO::FETCH_ASSOC) ) $list[] = $row;

			foreach ( $list as $f )
				$hlp->writeTransUnit( $writer, 	$f['title'], 
												$f['section_izumi'], 
												$f['subsection_izumi'], 
												$f['app_izumi'], 
												$f['faq_izumi'] );

			// adding faqs without app
			$list = NULL;
			while ( $row = $faqs_without_apps->fetch(PDO::FETCH_ASSOC) ) $list[] = $row;
			
			foreach ( $list as $f )
				$hlp->writeTransUnit( $writer, 	$f['title'], 
												$empty_keyword, 
												$empty_keyword, 
												$empty_keyword, 
												$f['faq_izumi'] );

			// adding questions with full section/subsection/app/faq path
			$list = NULL;
			while ( $row = $questions->fetch(PDO::FETCH_ASSOC) ) $list[] = $row;
			
			foreach ( $list as $f )
				$hlp->writeTransUnit( $writer, 	$f['question'], 
												$f['section_izumi'], 
												$f['subsection_izumi'], 
												$f['app_izumi'], 
												$f['faq_izumi'], 
												$f['question_izumi'] );

			// adding questions without app
			$list = NULL;
			while ( $row = $questions_without_apps->fetch(PDO::FETCH_ASSOC) ) $list[] = $row;
			
			foreach ( $list as $f )
				$hlp->writeTransUnit( $writer, 	$f['question'], 
												$empty_keyword, 
												$empty_keyword, 
												$empty_keyword, 
												$f['faq_izumi'], 
												$f['question_izumi'] );

			// adding answers with full section/subsection/app/faq/question path
			$list = NULL;
			while ( $row = $answers->fetch(PDO::FETCH_ASSOC) ) $list[] = $row;
			
			foreach ( $list as $f )
				$hlp->writeTransUnit( $writer, 	$f['answer'], 
												$f['section_izumi'], 
												$f['subsection_izumi'], 
												$f['app_izumi'], 
												$f['faq_izumi'], 
												$f['question_izumi'], 
												$f['answer_izumi'] );

			// adding answers without app
			$list = NULL;
			while ( $row = $answers_without_apps->fetch(PDO::FETCH_ASSOC) ) $list[] = $row;
			
			foreach ( $list as $f )
				$hlp->writeTransUnit( $writer, 	$f['answer'], 
												$empty_keyword, 
												$empty_keyword, 
												$empty_keyword, 
												$f['faq_izumi'], 
												$f['question_izumi'], 
												$f['answer_izumi'] );

		$writer->endElement(); // body
	$writer->endElement(); // file
$writer->endElement(); // xliff
$writer->endDocument();
$xml = $writer->outputMemory();

$saved = file_put_contents($output_file, $xml);
if ( $saved ) {
	echo 'Data (' . $saved . ' characters) exported to "' . $output_file . '" file.';
} else echo 'ERROR. Export failed.'

?>