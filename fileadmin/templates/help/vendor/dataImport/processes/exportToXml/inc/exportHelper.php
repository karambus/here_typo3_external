<?php

class exportHelper {

    static public function writeHeader($writer) {
        $writer->startElement("header");

            $writer->startElement('phase-group');
            $writer->startElement('phase');
                $writer->writeAttribute('phase-name', 'draft');
                $writer->writeAttribute('process-name', 'draft');
            $writer->endElement(); // phase

            $writer->startElement('phase');
                $writer->writeAttribute('phase-name', 'review');
                $writer->writeAttribute('process-name', 'review');
            $writer->endElement(); // phase

            $writer->startElement('phase');
                $writer->writeAttribute('phase-name', 'localisation');
                $writer->writeAttribute('process-name', 'localisation');
            $writer->endElement(); // phase

            $writer->startElement('phase');
                $writer->writeAttribute('phase-name', 'complete');
                $writer->writeAttribute('process-name', 'complete');
            $writer->endElement(); // phase
            $writer->endElement(); // phase-group

            $writer->startElement('tool');
            $writer->writeAttribute('tool-id', 'izumi');
            $writer->writeAttribute('tool-name', 'Izumi');
            $writer->writeAttribute('tool-version', '3.8.30');
            $writer->writeAttribute('tool-company', 'Nokia');
            $writer->endElement(); // tool

        $writer->endElement(); // header
    }

    static public function writeTransUnit($writer, $text, $section=NULL, $subsection=NULL, $app=NULL, $faq=NULL, $question=NULL, $answer=NULL) {

        $path = array();
        array_push($path, "help");
        if ( $section != NULL ) {
            array_push($path, $section);
            if ( $subsection != NULL ) {
                array_push($path, $subsection);
                if ( $app != NULL ) {
                    array_push($path, $app);
                    if ( $faq != NULL ) {
                        array_push($path, $faq);
                        if ( $question != NULL ) {
                            array_push($path, $question);
                            if ( $answer != NULL ) {
                                array_push($path, $answer);
                            }
                        }
                    }
                }
            }
        }

        $writer->startElement('trans-unit');

        $writer->writeAttribute('id', implode('_', $path));
        $writer->writeAttribute('phase-name', '');

            $writer->startElement('source');
            $writer->writeAttribute('izumi:date', '');
                $writer->text($text);
            $writer->endElement(); // source

            $writer->startElement('target');
                $writer->text($text);
            $writer->endElement(); // target

            $writer->startElement('context-group');
            $writer->writeAttribute('purpose', 'information');

                $writer->startElement('context');
                $writer->writeAttribute('context-type', 'x-description');
                    $writer->text('');
                $writer->endElement(); // context

            $writer->endElement(); // context-group

        $writer->endElement(); // trans-unit
    }
}

?>