<?php

$db = DB::getInstance();

$faqs = $db->query('
            SELECT  s.uid       AS section_izumi,
                    ss.uid      AS subsection_izumi,
                    app.uid     AS app_izumi,
                    f.uid       AS faq_izumi,
                    f.title

            FROM    tx_herehelp_domain_model_sections       AS s,
                    tx_herehelp_domain_model_subsections    AS ss,
                    tx_herehelp_domain_model_apps           AS app,
                    tx_herehelp_domain_model_faqs           AS f

            WHERE   s.sys_language_uid = 0 AND
                    ss.sys_language_uid = 0 AND
                    app.sys_language_uid = 0 AND
                    f.sys_language_uid = 0 AND

                    f.apps = app.uid AND
                    app.subsections = ss.uid AND
                    ss.sections = s.uid');

$faqs_without_apps = $db->query('
            SELECT  f.uid       AS faq_izumi,
                    f.title

            FROM    tx_herehelp_domain_model_faqs           AS f

            WHERE   f.sys_language_uid = 0 AND

                    f.apps = 0');

$questions = $db->query('
            SELECT  s.uid       AS section_izumi,
                    ss.uid      AS subsection_izumi,
                    app.uid     AS app_izumi,
                    f.uid       AS faq_izumi,
                    q.uid       AS question_izumi,
                    q.question

            FROM    tx_herehelp_domain_model_sections       AS s,
                    tx_herehelp_domain_model_subsections    AS ss,
                    tx_herehelp_domain_model_apps           AS app,
                    tx_herehelp_domain_model_faqs           AS f,
                    tx_herehelp_domain_model_questions      AS q

            WHERE   s.sys_language_uid = 0 AND
                    ss.sys_language_uid = 0 AND
                    app.sys_language_uid = 0 AND
                    f.sys_language_uid = 0 AND
                    q.sys_language_uid = 0 AND

                    q.faqs = f.uid AND
                    f.apps = app.uid AND
                    app.subsections = ss.uid AND
                    ss.sections = s.uid');

$questions_without_apps = $db->query('
            SELECT  f.uid       AS faq_izumi,
                    q.uid       AS question_izumi,
                    q.question

            FROM    tx_herehelp_domain_model_faqs           AS f,
                    tx_herehelp_domain_model_questions      AS q

            WHERE   f.sys_language_uid = 0 AND
                    q.sys_language_uid = 0 AND

                    q.faqs = f.uid AND
                    f.apps = 0');

$answers = $db->query('
            SELECT  s.uid       AS section_izumi,
                    ss.uid      AS subsection_izumi,
                    app.uid     AS app_izumi,
                    f.uid       AS faq_izumi,
                    q.uid       AS question_izumi,
                    a.uid       AS answer_izumi,
                    a.answer

            FROM    tx_herehelp_domain_model_sections       AS s,
                    tx_herehelp_domain_model_subsections    AS ss,
                    tx_herehelp_domain_model_apps           AS app,
                    tx_herehelp_domain_model_faqs           AS f,
                    tx_herehelp_domain_model_questions      AS q,
                    tx_herehelp_domain_model_answers        AS a

            WHERE   s.sys_language_uid = 0 AND
                    ss.sys_language_uid = 0 AND
                    app.sys_language_uid = 0 AND
                    f.sys_language_uid = 0 AND
                    q.sys_language_uid = 0 AND
                    a.sys_language_uid = 0 AND

                    a.uid = q.answers AND
                    q.faqs = f.uid AND
                    f.apps = app.uid AND
                    app.subsections = ss.uid AND
                    ss.sections = s.uid');

$answers_without_apps = $db->query('
            SELECT  f.uid       AS faq_izumi,
                    q.uid       AS question_izumi,
                    a.uid       AS answer_izumi,
                    a.answer

            FROM    tx_herehelp_domain_model_faqs           AS f,
                    tx_herehelp_domain_model_questions      AS q,
                    tx_herehelp_domain_model_answers        AS a

            WHERE   f.sys_language_uid = 0 AND
                    q.sys_language_uid = 0 AND
                    a.sys_language_uid = 0 AND

                    a.uid = q.answers AND
                    q.faqs = f.uid AND
                    f.apps = 0');
