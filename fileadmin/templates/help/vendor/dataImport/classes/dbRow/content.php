<?php

/**
 * [short description]
 *
 * [long description]
 *
 * @category
 * @package
 *
 * @author   Tschitsch
 * @since    05.08.14 <dev@tschitsch.de>
 */
class dbRow_content extends dbRow {

	// interne ID des Datensatzes (KEY)
	protected $uid;

	// Page ID, d.h. die Seite auf welcher dieser Inhalt liegt.
	// Muß zwingend gesetzt sein.
	// (Vorsicht Falle: Entspricht pages.UID - siehe Unten !)
	protected $pid;

	// Timestamp bei Erstellung des Datensatzes
	protected $tstamp;

	// Timestamp bei Erstellung des Datensatzes
	protected $crdate;

	// „Content Type“, anhand dessen TYPO3 weiss, um welche Art von Inhalt es sich handelt.
	// Bei Legal sollte das Durchgängig „text“ sein (Kleingeschrieben!)
	protected $CType = "text";

	// Feld für die Überschrift
	protected $header;

	// Der eigentliche Inhalt.
	protected $bodytext;

	// Die UID der jeweiligen Sprache. Diese wird an anderer Stelle
	// in TYPO3 definiert. Daher ist _vorher_ wichtig zu Wissen,
	// welche Sprachen existieren, die müssen dann in TYPO3 noch
	// Angelegt werden und damit wir dann für den Importer auch wissen,
	// welcher Sprache welche UID zugewiesen ist!
	protected $sys_language_uid;

	// Die eindeutige UID des Eltern-Datensatzes einer Übersetzung.
	// Beispiel: UID 3 ist der „Original“-Datensatz. UID 4 ist die Übersetzung.
	// Dann würde bei UID 4 z.B. hier in dem Tabellenfeld '3' stehen,
	// da der Datensatz mit der UID „3“ der l18n_parent zu diesem Datensatz ist.
	// Wichtig: sys_language_uid _muss_ dann zwingend auf die richtige Sprache
	// gesetzt werden!
	protected $l18n_parent;

	// Ist ein Mediumblob-Feld mit einem serialized-Array,
	// in welchem TYPO3 die DIFFerenz zwischen dem Original-Inhalt
	// und dem jeweils Übersetzten speichert. In der Originalsprache!
	protected $l18n_diffsource;

	/**
	 * @param mixed $bodytext
	 *
	 * @return $this
	 */
	public function setBodytext($bodytext) {
		$this->bodytext = $bodytext;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBodytext() {
		return $this->bodytext;
	}

	/**
	 * @param mixed $header
	 *
	 * @return $this
	 */
	public function setHeader($header) {
		$this->header = $header;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getHeader() {
		return $this->header;
	}

	/**
	 * @param mixed $l18n_parent
	 *
	 * @return $this
	 */
	public function setL18nParent($l18n_parent) {
		$this->l18n_parent = $l18n_parent;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getL18nParent() {
		return $this->l18n_parent;
	}

	/**
	 * @param mixed $pid
	 *
	 * @return $this
	 */
	public function setPid($pid) {
		$this->pid = $pid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPid() {
		return $this->pid;
	}

	/**
	 * @param mixed $sys_language_uid
	 *
	 * @return $this
	 */
	public function setSysLanguageUid($sys_language_uid) {
		$this->sys_language_uid = $sys_language_uid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSysLanguageUid() {
		return $this->sys_language_uid;
	}

	/**
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {

		$now = date("Y-m-d H:i:s", time());

		$this->uid              = "content_" . $inst->getPostId();
		$this->pid              = "";
		$this->tstamp           = $now;
		$this->crdate           = $now;
		$this->CType            = "text";
		$this->header           = $inst->getHeader();
		$this->bodytext         = $inst->getContent();
		$this->sys_language_uid = null;
		$this->l18n_parent      = null;
		$this->l18n_diffsource  = null;

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {

		$q = sprintf(
			"INSERT INTO tt_content
				('uid','pid','tstamp','crdate','CType','header','bodytext','sys_language_uid','l18n_parent','l18n_diffsource')
			VALUES
				(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
			$this->db->quote($this->uid),
			$this->db->quote($this->pid),
			$this->db->quote($this->tstamp),
			$this->db->quote($this->crdate),
			$this->db->quote($this->CType),
			$this->db->quote($this->header),
			$this->db->quote($this->bodytext),
			$this->db->quote($this->sys_language_uid),
			$this->db->quote($this->l18n_parent),
			$this->db->quote($this->l18n_diffsource)
		);

		$this->db->query($q);
		$this->uid = $this->db->lastInsertId();

		return $this;
	}

}
