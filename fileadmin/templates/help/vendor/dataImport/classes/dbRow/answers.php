<?php

/**
 * Tabelle: answers
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_answers extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_answers";

	protected $uid;
	protected $pid;
	protected $answer;
	//    protected $paths;

	/*
	 * prepare the dataset
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$this->pid    = $inst->getPid();
		$this->answer = $inst->getAnswer();

		//        $this->paths            = $inst->getPaths();

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata           = array();
		$rowdata["pid"]    = ($this->pid);
		$rowdata["answer"] = ($this->answer);
		//        $rowdata["paths"]            = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;
	}

}
