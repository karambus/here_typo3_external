<?php

/**
 * Tabelle: subsections
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_subsections extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_subsections";
	protected $parents
		= array(
			array('model' => 'dbRow_sections', 'field' => 'sections')
		);

	protected $uid;
	protected $pid;
	protected $sections;
	protected $title;
	protected $backendtitle;
	protected $icon;
	protected $apps;
	protected $faqs;
	protected $paths;

	/*
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		//        $this->key              = $inst->getKey();
		$this->pid   = $inst->getPid();
		$this->title = $inst->getTitle();
		$this->icon  = "";
		$this->apps  = $inst->getApps();
		$this->paths = $inst->getPaths();

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata                 = array();
		$rowdata["pid"]          = ($this->pid);
		$rowdata["sections"]     = ($this->sections);
		$rowdata["title"]        = ($this->title);
		$rowdata["backendtitle"] = ($this->backendtitle);
		$rowdata["icon"]         = ($this->icon);
		$rowdata["apps"]         = ($this->apps);
		$rowdata["faqs"]         = ($this->faqs);
		$rowdata["paths"]        = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;
	}

}
