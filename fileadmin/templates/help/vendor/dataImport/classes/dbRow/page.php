<?php

/**
 * Damit sind Inhalte dann entsprechend angelegt.
 * Nun gehören Inhalte in TYPO3 ja aber auf Seiten ;)
 *
 * Tabelle: pages
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_page extends dbRow {

	// interne ID des Datensatzes (KEY)
	protected $uid;

	// Page ID
	protected $pid;

	// Timestamp bei Erstellung des Datensatzes
	protected $tstamp;

	// Timestamp bei Erstellung des Datensatzes
	protected $crdate;

	// Titel der Seite
	protected $title;

	// TYP der Seite. Im Fall von Legal sollte das immer „1“ sein
	protected $doktype = 1;

	// Sollte im Falle von Legal immer „1“ sein
	protected $urltype = 1;

	// Timestamp der letzten Änderung der Seite.
	// Bei Neuanlage also identisch zu crdate
	protected $SYS_LASTCHANGED;

	/**
	 * @param mixed $uid
	 *
	 * @return $this
	 */
	public function setUid($uid) {
		$this->uid = $uid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUid() {
		return $this->uid;
	}

	/**
	 * @param mixed $pid
	 *
	 * @return $this
	 */
	public function setPid($pid) {
		$this->pid = $pid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPid() {
		return $this->pid;
	}

	/**
	 * @param mixed $title
	 *
	 * @return $this
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$now = time();

		$this->uid             = "page_" . $inst->getPostId();
		$this->pid             = "";
		$this->tstamp          = $now;
		$this->crdate          = $now = date("Y-m-d H:i:s", $now);
		$this->title           = $inst->getHeader();
		$this->doktype         = 1;
		$this->urltype         = 1;
		$this->SYS_LASTCHANGED = $now;

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {

		$q = sprintf(
			"INSERT INTO pages
				('uid','pid','tstamp','crdate','title','doktype','urltype','SYS_LASTCHANGED')
			VALUES
				(%s,%s,%s,%s,%s,%s,%s,%s)",
			$this->db->quote($this->uid),
			$this->db->quote($this->pid),
			$this->db->quote($this->tstamp),
			$this->db->quote($this->crdate),
			$this->db->quote($this->title),
			$this->db->quote($this->doktype),
			$this->db->quote($this->urltype),
			$this->db->quote($this->SYS_LASTCHANGED)
		);

		$this->db->query($q);
		$this->uid = $this->db->lastInsertId();

		return $this;
	}

}
