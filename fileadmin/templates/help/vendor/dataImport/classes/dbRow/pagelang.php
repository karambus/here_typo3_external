<?php

/**
 * Und damit eine Seite weiß, wenn für sie eine Sprachübersetzung da ist
 * (die auch ERST von einem Inhalt verwendet werden kann, wenn diese Übersetzung
 * für eine Seite angelegt wurde):
 *
 * Tabelle: pages_languages_overlay
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_pagelang extends dbRow {

	// interne ID des Datensatzes (KEY)
	protected $uid;

	// Page ID der „Originalseite“ (Vorsicht Falle: Entspricht pages.UID !)
	protected $pid;

	// Analog zu pages.doktype
	protected $doktype = 1;

	// Timestamp bei Erstellung des Datensatzes
	protected $tstamp;

	// Timestamp bei Erstellung des Datensatzes
	protected $crdate;

	// Die UID der jeweiligen Sprache, siehe Erklärung oben bei tt_content
	protected $sys_language_uid;

	// Der (übersetzte) Titel der Seite
	protected $title;

	// Analog zu pages.urltype
	protected $urltype = 1;

	// Ebenfalls wieder serialized Array mit dem DIFF
	// zwischen Original-Datensatz und diesem Lokalisierten
	protected $l18n_diffsource;

	/**
	 * @param mixed $pid
	 *
	 * @return $this
	 */
	public function setPid($pid) {
		$this->pid = $pid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPid() {
		return $this->pid;
	}

	/**
	 * @param mixed $sys_language_uid
	 *
	 * @return $this
	 */
	public function setSysLanguageUid($sys_language_uid) {
		$this->sys_language_uid = $sys_language_uid;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getSysLanguageUid() {
		return $this->sys_language_uid;
	}

	/**
	 * @param mixed $title
	 *
	 * @return $this
	 */
	public function setTitle($title) {
		$this->title = $title;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$now = time();

		$this->uid              = "lang_" . $inst->getPostId();
		$this->pid              = "";
		$this->doktype          = 1;
		$this->tstamp           = $now;
		$this->crdate           = $now = date("Y-m-d H:i:s", $now);
		$this->sys_language_uid = "";
		$this->title            = $inst->getHeader();
		$this->urltype          = 1;
		$this->l18n_diffsource  = "";

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {

		$q = sprintf(
			"INSERT INTO pages_languages_overlay
				('uid','pid','doktype','tstamp','crdate','sys_language_uid','title','urltype','l18n_diffsource')
			VALUES
				(%s,%s,%s,%s,%s,%s,%s,%s)",
			$this->db->quote($this->uid),
			$this->db->quote($this->pid),
			$this->db->quote($this->doktype),
			$this->db->quote($this->tstamp),
			$this->db->quote($this->crdate),
			$this->db->quote($this->title),
			$this->db->quote($this->urltype),
			$this->db->quote($this->l18n_diffsource)
		);

		$this->db->query($q);
		$this->uid = $this->db->lastInsertId();

		return $this;
	}

}
