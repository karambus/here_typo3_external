<?php

/**
 * Tabelle: questions
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_questions extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_questions";
	protected $parentitem = null;
	protected $parents
		= array(
			array('model' => 'dbRow_faqs', 'field' => 'faqs'),
		);

	protected $uid;
	protected $pid;
	protected $faqs;
	protected $question;
	protected $answers;
	protected $paths;

	/*
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$this->key      = $inst->getKey();
		$this->display  = $inst->getDisplay();
		$this->pid      = $inst->getPid();
		$this->question = $inst->getTitle();
		$this->answers  = $inst->getAnswers();
		$this->paths    = $inst->getPaths();

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata             = array();
		$rowdata["pid"]      = ($this->pid);
		$rowdata["faqs"]     = ($this->faqs);
		$rowdata["question"] = ($this->question);
		$rowdata["answers"]  = ($this->answers);
		$rowdata["paths"]    = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;
	}

}
