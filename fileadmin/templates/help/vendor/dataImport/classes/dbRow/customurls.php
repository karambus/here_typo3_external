<?php

/**
 * Tabelle: customurls
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_customurls extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_customurls";

	protected $uid;
	protected $pid;
	protected $paths;

	/*
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$this->pid              = $inst->getPid();
		$this->paths            = $inst->getPaths();
		$this->sys_language_uid = "";
		$this->l10n_parent      = "";
		$this->l10n_diffsource  = "";

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata          = array();
		$rowdata["pid"]   = ($this->pid);
		$rowdata["paths"] = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;
	}


	public function getPathString() {
		return $this->getPaths();
	}

}
