<?php

/**
 * Tabelle: sections
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_sections extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_sections";
	protected $parents = array();

	protected $uid;
	protected $pid;
	protected $title;
	protected $backendtitle;
	protected $icon;
	protected $subsections;
	protected $faqs;
	//    protected $paths;
	protected $sys_language_uid;
	protected $l10n_parent;
	protected $l10n_diffsource;

	/*
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$this->key         = $inst->getKey();
		$this->pid         = $inst->getPid();
		$this->title       = $inst->getTitle();
		$this->icon        = "";
		$this->subsections = $inst->getSubsections();

		//        $this->paths            = $inst->getPaths();

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata                 = array();
		$rowdata["pid"]          = ($this->pid);
		$rowdata["title"]        = ($this->title);
		$rowdata["backendtitle"] = ($this->backendtitle);
		$rowdata["icon"]         = ($this->icon);
		$rowdata["subsections"]  = ($this->subsections);
		$rowdata["faqs"]         = ($this->faqs);
		//        $rowdata["paths"]            = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;
	}

}
