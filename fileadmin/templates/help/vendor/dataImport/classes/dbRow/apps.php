<?php

/**
 * Tabelle: apps
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_apps extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_apps";
	protected $parents
		= array(
			array('model' => 'dbRow_subsections', 'field' => 'subsections'),
			array('model' => 'dbRow_sections', 'field' => 'sections')
		);


	protected $uid;
	protected $pid;
	protected $subsections;
	protected $title;
	protected $backendtitle;
	protected $icon;
	protected $faqs;
	protected $paths;


	/*
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		//        $this->key              = $inst->getKey();
		$this->pid   = $inst->getPid();
		$this->title = $inst->getTitle();
		$this->icon  = "";
		$this->paths = $inst->getPaths();

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata                 = array();
		$rowdata["pid"]          = ($this->pid);
		$rowdata["subsections"]  = ($this->subsections);
		$rowdata["title"]        = ($this->title);
		$rowdata["backendtitle"] = ($this->backendtitle);
		$rowdata["icon"]         = ($this->icon);
		$rowdata["faqs"]         = ($this->faqs);
		$rowdata["paths"]        = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;
	}

}
