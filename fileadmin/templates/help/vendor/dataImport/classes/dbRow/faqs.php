<?php

/**
 * Tabelle: faqs
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class dbRow_faqs extends dbRow {
	protected $tablename = "tx_herehelp_domain_model_faqs";
	protected $parents
		= array(
			array('model' => 'dbRow_apps', 'field' => 'apps'),
			array('model' => 'dbRow_subsections', 'field' => 'subsections'),
			array('model' => 'dbRow_sections', 'field' => 'sections')
		);

	protected $uid;
	protected $pid;
	protected $sections;
	protected $apps;
	protected $subsections;
	protected $headline;
	protected $title;
	protected $backendtitle;
	protected $transitdb;
	protected $questions;
	protected $paths;


	/*
	 * prepare the dataset
	 *
	 * @param importItem $inst
	 *
	 * @return $this
	 */
	public function setDataFromImportItem(importItem $inst) {
		$this->key       = $inst->getKey();
		$this->display   = $inst->getDisplay();
		$this->pid       = "";
		$this->headline  = $inst->getHeadline();
		$this->title     = $inst->getTitle();
		$this->transitdb = $inst->getTransitdb();
		$this->questions = $inst->getQuestions();
		$this->paths     = $inst->getPaths();

		return $this;
	}

	/**
	 * execute the import
	 *
	 * @return $this
	 */
	public function update() {
		$rowdata                 = array();
		$rowdata["pid"]          = ($this->pid);
		$rowdata["sections"]     = ($this->sections);
		$rowdata["subsections"]  = ($this->subsections);
		$rowdata["apps"]         = ($this->apps);
		$rowdata["headline"]     = ($this->headline);
		$rowdata["title"]        = ($this->title);
		$rowdata["backendtitle"] = ($this->backendtitle);
		$rowdata["questions"]    = ($this->questions);
		$rowdata["paths"]        = ($this->paths);

		parent::updateDbRow($rowdata);

		return $this;


	}

}
