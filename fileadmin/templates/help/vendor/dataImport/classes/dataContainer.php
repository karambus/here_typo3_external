<?php

/**
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    04.08.14
 */
class dataContainer {

	protected $data;

	public function set($key, $value, $override = false) {
		if (isset($this->data[$key]) === true AND $override !== true) {
			throw new Exception("item exists");
		}
		$this->data[$key] = $value;

		return $this;
	}

	public function get($key) {
		if (isset($this->data[$key]) === false OR is_null($this->data[$key]) === true) {
			return "NOT AVAILABLE INSTANCE WITH ID " . $key;
		}

		return $this->data[$key];
	}

	public function __get($key) {
		return $this->get($key);
	}

	public function __set($key, $value) {
		$this->set($key, $value, false);

		return $this;
	}

}
