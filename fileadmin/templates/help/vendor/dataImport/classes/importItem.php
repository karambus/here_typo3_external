<?php

/**
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    04.08.14
 */
abstract class importItem {

	public static $logEnabled = true;

	protected $raw;
	protected $data;

	public final function __construct($raw) {
		$this->raw = $raw;
		$this->prepareData($raw);
	}

	public function getData() {
		return ($this->data);
	}

	protected final function prepareData($raw) {
		$this->data = $this->transformData($raw);
	}

	abstract public function updateReferences();

	abstract protected function transformData();

	public function __get($prop) {
		return $this->data[$prop];
	}

	public function __set($prop, $value) {
		$this->data[$prop] = $value;

		return $this;
	}

	public function __call($method, $args) {
		if (in_array(substr($method, 0, 3), array("get", "set"))) {
			$prop = substr($method, 3);
			$prop = preg_replace("/([A-Z])/", " $1", $prop);
			$prop = preg_replace("/ /", "_", $prop);
			$prop = trim($prop, " _");
			$prop = strtolower($prop);

			if (substr($method, 0, 3) === "set") {
				$this->$prop = $args[0];

				return $this;
			}
			if (substr($method, 0, 3) === "get") {
				return $this->$prop;
			}
		}
	}

	protected final function lognl($msg, $fg = null, $bg = null) {
		if (self::$logEnabled === true) {
			helpers::outnl($msg, $fg, $bg);
		}
	}

	protected final function log($msg, $fg = null, $bg = null) {
		if (self::$logEnabled === true) {
			helpers::out($msg, $fg, $bg);
		}
	}
}
