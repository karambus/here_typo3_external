<?php

class izumifetcher {
	/**
	 * credentials
	 *
	 * @var null
	 */
	protected $username = null;
	protected $password = null;

	//    protected $proxy = '172.16.42.42:8080';
	protected $proxy = null;

	/**
	 * constructor
	 *
	 * @param $username
	 * @param $password
	 */
	public function __construct($username, $password) {
		$this->username = $username;
		$this->password = $password;
	}

	/**
	 * fetch data from the given url and return its contents
	 *
	 * @param $target_url
	 *
	 * @return mixed
	 */
	public function fetch($target_url) {
		$this->log("downloading " . $target_url . " ... ");

		$ch = curl_init($target_url);
		curl_setopt($ch, CURLOPT_USERPWD, $this->username . ':' . $this->password);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array("Authorization: Basic " . base64_encode($this->username . ":" . $this->password))
		);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		if (isset($this->proxy)) {
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$response_data = curl_exec($ch);

		if (curl_errno($ch) > 0) {
			helpers::abort('there was a curl error: ' . curl_error($ch));
		} else {
			curl_close($ch);
		}

		$this->log(strlen($response_data) . " received\n", "green");

		return $response_data;
	}

	public function log($msg, $fg = null, $bg = null) {
		helpers::out($msg, $fg, $bg);
	}

}