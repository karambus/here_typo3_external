<?php

/**
 * Created by PhpStorm.
 * User: meekluc
 * Date: 18.08.14
 * Time: 18:30
 */
class languageHelper {

	static protected $languagesTable = 'sys_language';
	static protected $languagesResolved = array();
	static protected $languageUIDsHardcoded = array(
		"en_en" => 0,
		"en_us" => 1,
		"de_de" => 2,
		"fi_fi" => 3,
		"it_it" => 4,
		"es_es" => 5,
		"fr_fr" => 6,
		"bg" => 7,
		"dk" => 8,
		"gr" => 9,
		"hu" => 10,
		"nl" => 11,
		"no" => 12,
		"pl" => 13,
		"br" => 14,
		"pt" => 15,
		"ro" => 16,
		"ru" => 17,
		"sv" => 18,
		"tr" => 19,
		"zh" => 20
	);



	/**
	 * get the language uid for the given locale idenfitier from multiple sources
	 * first try the database, then hardcoded values
	 *
	 * @param $str
	 *
	 * @return bool|string
	 */
	static public function getLanguageUID($str) {
		// get the language uid set for this typo3 instance
		try {
			$languageUID = languageHelper::getDatabaseLanguageUIDForLocale($str);
		} catch (Exception $e) {
			$languageUID = false;
		}

		// resolve hardcoded for en_US if it has not been resolved before
		if ($languageUID === false AND strtolower(str_replace("-","_",$str)) === "en_us") {
			$languageUID = languageHelper::getHardcodedLanguageUIDByLocale($str);
		}

		return $languageUID;
	}

	/**
	 * get the sys language uid from database
	 *
	 * @param $str
	 *
	 * @return bool|string
	 * @throws Exception
	 */
	public static function getDatabaseLanguageUIDForLocale($str) {

		if (isset(self::$languagesResolved[$str]) AND is_numeric(self::$languagesResolved[$str]) === true) {
			return self::$languagesResolved[$str];
		}

		$db = DB::getInstance();

		$strUnderscored = str_replace("-","_",$str);

		$q = sprintf(
			'SELECT sysl.uid
			FROM %1$s as sysl
			INNER JOIN static_languages as staticl ON (staticl.uid = sysl.static_lang_isocode)
			WHERE staticl.lg_collate_locale=%2$s OR staticl.lg_typo3 = %2$s OR staticl.lg_collate_locale=%3$s OR staticl.lg_typo3 = %3$s',
			self::$languagesTable,
			$db->quote($str),
			$db->quote($strUnderscored)
		);

		$stmt = $db->query($q);
		$uid  = false;
		if ($stmt !== false) {
			$uid = $stmt->fetchColumn(0);
		}

		if ($uid === false) {
			throw new \Exception($str . " is not available in " . self::$languagesTable);
		}

		return $uid;
	}

	/**
	 * get hardcoded language uid from defined properties within this helper class
	 *
	 * @uses self::$languageUIDsHardcoded
	 *
	 * @param $str
	 *
	 * @return bool
	 */
	public static function getHardcodedLanguageUIDByLocale($str) {

		$str = str_replace("-", "_", $str);
		$str = strtolower($str);

		if ($str === "en_gb") {
			$str = "en_en";
		}

		if (isset(self::$languageUIDsHardcoded[$str])) {
			return self::$languageUIDsHardcoded[$str];
		}

		return false;

	}

}
