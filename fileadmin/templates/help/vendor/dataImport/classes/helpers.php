<?php

class helpers {
	/**
	 * abort the script execution and report
	 *
	 * @param $msg
	 * @param $exitpoint
	 */
	static public function abort($msg = null, $exitpoint = 0) {
		$backtrace = debug_backtrace();

		if ($msg instanceof Exception) {
			$E         = $msg;
			$msg       = $E->getMessage();
			$backtrace = $E->getTrace();
			array_shift($backtrace);
		}

		$file = $backtrace[$exitpoint]["file"];
		$line = $backtrace[$exitpoint]["line"];

		self::outnl("");
		if (isset($msg)) {
			self::outnl($msg, "white", "red");
		}
		self::outnl("script aborted in " . $file . ":" . $line, "black", "red");
		self::outnl("");
		die();
	}

	/**
	 * remove all non-alnum characters from the given string and replace it with given replace-param-value
	 *
	 * @param        $string
	 * @param string $replace
	 *
	 * @return mixed
	 */
	static public function transformToAlnum($string, $replace = "-") {
		$return = preg_replace("/[^A-Za-z0-9]/", $replace, $string);
		$return = preg_replace("/" . $replace . $replace . "+/", "-", $return);

		return $return;
	}

	/**
	 * alias for transformToAlnum using dash as replace param
	 *
	 * @param $string
	 *
	 * @return mixed
	 */
	static public function dashify($string) {
		return self::transformToAlnum($string, '-');
	}

	/**
	 * @param $string
	 *
	 * @return mixed
	 */
	static public function snakify($string) {
		return self::transformToAlnum($string, '_');
	}

	/**
	 * alias for dashify
	 *
	 * @param $string
	 *
	 * @return mixed
	 */
	static public function pathify($string) {
		return self::dashify($string);
	}

	/**
	 * return a colorizes string for cli ouput
	 *
	 * @param      $string
	 * @param null $fg
	 * @param null $bg
	 *
	 * @return string
	 */
	static public function colorize($string, $fg = null, $bg = null) {

		if (stristr(PHP_OS, "win") !== false) {
			return $string;
		}

		if (!isset($fg) AND isset($bg)) {
			return $string;
		}

		$foreground_colors                 = array();
		$foreground_colors['black']        = '0;30';
		$foreground_colors['dark_gray']    = '1;30';
		$foreground_colors['blue']         = '0;34';
		$foreground_colors['light_blue']   = '1;34';
		$foreground_colors['green']        = '0;32';
		$foreground_colors['light_green']  = '1;32';
		$foreground_colors['lime']         = '1;32';
		$foreground_colors['cyan']         = '0;36';
		$foreground_colors['light_cyan']   = '1;36';
		$foreground_colors['red']          = '0;31';
		$foreground_colors['light_red']    = '1;31';
		$foreground_colors['purple']       = '0;35';
		$foreground_colors['light_purple'] = '1;35';
		$foreground_colors['brown']        = '0;33';
		$foreground_colors['yellow']       = '1;33';
		$foreground_colors['light_gray']   = '0;37';
		$foreground_colors['white']        = '1;37';

		$background_colors               = array();
		$background_colors['black']      = '40';
		$background_colors['red']        = '41';
		$background_colors['green']      = '42';
		$background_colors['yellow']     = '43';
		$background_colors['blue']       = '44';
		$background_colors['magenta']    = '45';
		$background_colors['cyan']       = '46';
		$background_colors['light_gray'] = '47';

		$colored_string = "";

		// Check if given foreground color found
		if (isset($foreground_colors[$fg])) {
			$colored_string .= "\033[" . $foreground_colors[$fg] . "m";
		}
		// Check if given background color found
		if (isset($background_colors[$bg])) {
			$colored_string .= "\033[" . $background_colors[$bg] . "m";
		}

		// Add string and end coloring
		$colored_string .= $string . "\033[0m";

		return $colored_string;
	}

	/**
	 * display debug-style content
	 *
	 * @param $mixed
	 */
	static public function debug($mixed) {
		$backtrace = debug_backtrace();
		$file      = $backtrace[0]["file"];
		$line      = $backtrace[0]["line"];

		if (is_object($mixed)) {
			$mixed = var_export($mixed, 1);
		}
		if (is_array($mixed)) {
			$mixed = print_r($mixed, 1);
		}
		self::outnl("");
		self::outnl($mixed, "white", "red");
		self::outnl("file: " . $file . ":" . $line, "black", "red");
		self::outnl("");
	}


	static public function out($str, $fg = null, $bg = null) {
		echo self::getout($str, $fg, $bg);
	}

	static public function outnl($str, $fg = null, $bg = null) {
		echo "\n" . self::getout($str, $fg, $bg);
	}

	static public function getout($str, $fg = null, $bg = null) {
		$out = self::colorize($str, $fg, $bg);

		return $out;
	}

	static public function lpad($str, $len) {
		return str_pad($str, $len, " ", STR_PAD_LEFT);
	}

	static public function rpad($str, $len) {
		return str_pad($str, $len, " ", STR_PAD_RIGHT);
	}

	static public function cpad($str, $len) {
		return str_pad($str, $len, " ", STR_PAD_BOTH);
	}

	static public function runProcess($command) {
		$process = proc_open(
			$command,
			array(
				0 => array("pipe", "r"), //STDIN
				1 => array("pipe", "w"), //STDOUT
				2 => array("pipe", "w")
			), //STDERR
			$pipes
		);
		$result  = stream_get_contents($pipes[1]);
		$error   = stream_get_contents($pipes[2]);
		fclose($pipes[1]);
		fclose($pipes[2]);
		$status = proc_close($process);

		return $result;
	}

	/**
	 * appends $dir to $path and creates the folder recursively
	 * and returns boolean whether the path has been created or not
	 *
	 * @param $path
	 * @param $dir
	 * @param $chmod
	 *
	 * @return boolean
	 */
	static public function appendDir(&$path, $dir, $chmod = 0777) {
		$path = trim($path, " " . DS);
		$path .= DS . $dir;
		if (file_exists($path) === false) {
			mkdir($path, $chmod, true);

			return true;
		}

		return false;
	}


	/**
	 * @param $string
	 *
	 * @return bool
	 */
	static public function isValidXmlString($string) {
		return (substr($string, 0, 1) === "<");
	}

	static public function isValidXmlFile($filepath) {
		$content = file_get_contents($filepath);

		return self::isValidXmlString($content);
	}
}