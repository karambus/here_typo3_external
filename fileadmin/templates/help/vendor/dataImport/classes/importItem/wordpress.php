<?php

/**
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    04.08.14
 */
class importItem_wordpress extends importItem {

	protected $database_id;

	public function updateReferences() {

		if (is_null($this->data["wp:post_parent"]) === false) {
			$id                           = $this->data["wp:post_parent"];
			$inst                         = itemRegistry::get($id);
			$this->data["wp:post_parent"] = $inst;
		}

		if (is_null($this->data["wp:postmeta"]) === false) {
			foreach ($this->data["wp:postmeta"] as $key => $value) {
				if (substr($key, 0, 4) === "lang" AND is_numeric($value) === true) {
					$id                              = $this->data["wp:postmeta"][$key];
					$inst                            = itemRegistry::get($id);
					$this->data["wp:postmeta"][$key] = $inst;

					// resolve language code
					// get 5 character

					$this->data["translations"][] = $inst;
				}
			}
		}
	}	

	protected function transformData() {
		$raw = $this->raw;
		$res = $this->convertData($raw);
		return $res;
	}

	protected function convertData($raw) {

		$return = $raw;

		if ($raw instanceof \Traversable OR is_array($raw)) {

			$return = array();

			// meta key/values pairs
			if (count($raw) === 2) {
				if (isset($raw["wp:meta_key"]) AND isset($raw["wp:meta_value"])) {
					$raw = array($raw["wp:meta_key"] => $this->convertData($raw["wp:meta_value"]));
					//                    $return[$raw["wp:meta_key"]] = $raw["wp:meta_value"];
					//                    unset($raw);
				}
			}

			if (count($raw) === 1) {
				$key = key($raw);
				if ($key === "@cdata") {
					$return = $this->convertData(current($raw));
				} else {
					foreach ($raw as $key => $val) {
						$key          = preg_replace("/^\@/", "~~~", $key);
						$return[$key] = $this->convertData($val);
					}
				}
			} else {

				if (count($raw) === 0) {
					$return = var_export($raw, 1);
				} else {
					foreach ($raw as $key => $val) {
						$val = $this->convertData($val);
						if (count($val) === 1 AND is_numeric($key)) {
							$key = key($val);
							$val = current($val);
						}
						// remove this for production
						if ($key === "content:encoded") {
							//$val = "REMOVED!";
						}
						//                        $key = preg_replace("/^@/","____",$key);

						$return[$key] = $val;
					}
				}
			}
		}

		if (is_array($return) === false) {

			if ($return == "false" OR $return == "true") {
				$return = ($return == "false" ? false : true);
			}

			if (preg_match("/^[0-9]+$/", $return) > 0) {
				$return = intval($return);
			}

			if (preg_match("/^[0-9]+[\.\,]+[0-9]+$/", $return) > 0) {
				$return = floatval($return);
			}

		}

		return $return;

	}

	public function getPostId() {
		return $this->data["wp:post_id"];
	}

	public function getPostType() {
		return $this->data["wp:post_type"];
	}

	public function isPage() {
		$type = $this->getPostType();

		return ($type === "page");
	}

	public function getTemplate() {
		if (isset($this->data["wp:postmeta"]["_wp_page_template"])) {
			return $this->data["wp:postmeta"]["_wp_page_template"];
		}

		return null;
	}

	public function getContent() {
		if (isset($this->data["content:encoded"])) {
			return $this->data["content:encoded"];
		}

		return "";
	}

	public function getHeader() {
		if (isset($this->data["title"])) {
			return $this->data["title"];
		}

		return "";
	}

	public function isTemplate($expected = null) {
		$template = $this->getTemplate();
		if (isset($expected) AND isset($template)) {
			return ($template === $expected);
		}

		return false;
	}

	public function isPublished() {
		if (isset($this->data["wp:status"])) {
			return ($this->data["wp:status"] == "publish");
		}

		return false;
	}

	public function getPostDate() {
		if (isset($this->data["wp:post_date"])) {
			return strtotime($this->data["wp:post_date"]);
		} 

		return "";
	}

	public function getPostLink() {
		if (isset($this->data["link"])) {
			return $this->data["link"];
		} 

		return "";
	}

	public function getPostParent() {
		if (isset($this->data["wp:post_parent"]) && ($this->data["wp:post_parent"]) != NULL) {
			return $this->data["wp:post_parent"];
		}
		return NULL;
	}

	public function getTranslations() {
		return $this->data["translations"];
	}

}
