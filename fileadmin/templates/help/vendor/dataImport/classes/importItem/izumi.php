<?php

/**
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    04.08.14
 */
class importItem_izumi extends importItem {

	public function updateReferences() {
	}

	/**
	 *
	 * @return array
	 */
	protected function transformData() {

		$return = $this->raw;
		$xml    = $this->raw;

		if (isset($xml) === false) {
			return $return;
		}

		$data = array(
			"locale" => (string)$xml->locale
		);

		// do xml magix
		$categories = $xml->help->category;
		$this->log(" " . count($categories) . ' categories found', count($categories) === 0 ? 'yellow' : 'green');
		foreach ($categories as $category) {
			$catid   = (string)$category->key;
			$content = (string)$category->content;
			$this->lognl("parsing category '" . $catid . "': ");
			$data["categories"][$catid] = array(
				"key"       => $catid,
				"paths"     => (string)$category->url,
				"title"     => (string)$category->name,
				"questions" => array(),
				"content"   => (string)$category->content,
				"display"   => (string)$category->display,
				"flagname"  => (string)$category->flagname,
				"flagvalue" => (string)$category->flagvalue,
			);

			// get the questsions
			$topics = $category->topics->topic;
			// skip categories without questions
			if (count($topics) === 0) {
				$this->log("no questions", "yellow");
				continue;
			}
			foreach ($topics as $topic) {

				$topicid = (string)$topic->url;

				$data["categories"][$catid]["questions"][$topicid] = array(
					"key"       => $topicid,
					"paths"     => (string)$topic->url,
					"title"     => (string)$topic->title,
					"display"   => (string)$topic->display,
					"flagname"  => (string)$topic->flagname,
					"flagvalue" => (string)$topic->flagvalue,
					"content"   => html_entity_decode(((string)$topic->content), ENT_XHTML)
				);
			}
			$this->log(count($data["categories"][$catid]["questions"]) . " questions", "light_green");
		}

		if (count($data) > 0) {
			$return = $data;
		}

		return $return;
	}

}