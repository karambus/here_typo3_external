<?php
include_once "DB.php";

/**
 * [short description]
 *
 * [long description]
 *
 * @category
 * @package
 *
 * @author   Tschitsch
 * @since    05.08.14 <dev@tschitsch.de>
 */
abstract class dbRow {

	/**
	 * @var DB
	 */
	protected $db;
	protected $tablename;
	protected $parents = array();
	protected $itemExists = false;

	protected $sys_language_uid = 0;
	protected $l10n_parent = 0;
	protected $l10n_diffsource = 0;
	protected $backendtitle = "";
	protected $key = "";
	protected $display = "";

	abstract function update();

	abstract function setDataFromImportItem(importItem $inst);

	public function getTablename() {
		return $this->tablename;
	}

	public function exists() {
		return $this->itemExists;
	}

	public final function __construct() {
		$this->db = DB::getInstance();
	}

	public function getUID() {
		return $this->uid;
	}

	public function getPID() {
		return $this->pid;
	}

	public function __get($prop) {
		return $this->$prop;
	}

	public function __set($prop, $value) {
		$this->$prop = $value;

		return $this;
	}

	public function __call($method, $args) {
		if (in_array(substr($method, 0, 3), array("get", "set", "has"))) {
			$prop = substr($method, 3);
			$prop = preg_replace("/([A-Z])/", " $1", $prop);
			$prop = preg_replace("/ /", "_", $prop);
			$prop = trim($prop, " _");
			$prop = strtolower($prop);

			if (substr($method, 0, 3) === "has") {
				return property_exists($this, $prop);
			}
			if (substr($method, 0, 3) === "set") {
				$this->$prop = $args[0];

				return $this;
			}
			if (substr($method, 0, 3) === "get") {
				return $this->$prop;
			}
		}
	}

	protected function setData($res) {
		foreach ($res as $key => $value) {
			$this->$key = $value;
		}

		return $this;
	}

	public function getParent($instance = null) {
		$PARENT = null;
		if (property_exists($this, 'parents') AND is_array($this->parents) AND count($this->parents) > 0) {
			foreach ($this->parents as $definition) {
				$model = $definition["model"];
				$field = $definition["field"];

				/** @var dbRow $PARENT */
				$PARENT = new $model();
				$PARENT->findByField("uid", $this->$field);

				// if we have an instance and it exists, instaleave
				if ($PARENT->exists() === true) {
					return $PARENT;
				}
			}
		}

		return $PARENT;
	}

	public final function findByField($field, $value, $casesensitive = false) {

		$q = sprintf(
			$casesensitive ? "SELECT * FROM %s WHERE LCASE(%s) = LCASE(%s)" : "SELECT * FROM %s WHERE %s = %s",
			$this->tablename,
			$field,
			$this->db->quote($value)
		);

		$stmt = $this->db->query($q);

		$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
		if (count($res) === 1) {
			$this->itemExists = true;
			$this->setData($res[0]);
		} else {
			if (count($res) > 1) {
				helpers::abort("multiple items found with: " . $q);
			}
		}

		return $this;

	}

	protected function beforeUpdateDbRowMethod(&$rowdata) {

		// set meta data
		if (isset($rowdata["tstamp"]) === false) {
			$rowdata["tstamp"] = time();
		}
		if (isset($rowdata["crdate"]) === false) {
			$rowdata["crdate"] = time();
		}

		// status data
		if (isset($rowdata["hidden"]) === false) {
			if ((int)$this->display > 1) {
				$rowdata["hidden"] = "1";
			}
		}

		// translations meta data
		if (isset($rowdata["sys_language_uid"]) === false) {
			$rowdata["sys_language_uid"] = $this->sys_language_uid;
		}
		if (isset($rowdata["l10n_parent"]) === false) {
			$rowdata["l10n_parent"] = $this->l10n_parent;
		}
		if (isset($rowdata["l10n_diffsource"]) === false) {
			$rowdata["l10n_diffsource"] = "";
		}

		// path hook
		if (isset($rowdata["paths"]) AND strlen(trim($rowdata["paths"], "'")) > 0) {

			$pathToCreate = trim($rowdata["paths"], "'");

			// skip if table is the customurls itself
			$CUSTOMURL = new dbRow_customurls();
			if ($this->tablename === $CUSTOMURL->getTablename()) {
				return true;
			}

			if (is_numeric($rowdata["paths"])) {
				$CUSTOMURL->findByField('uid', $pathToCreate);
			} else {

				// get parent item and prepend
				$PARENT       = $this->getParent();
				$tmpPathArray = array($rowdata["paths"]);
				if ($PARENT instanceof dbRow) {
					if ($PARENT->hasPaths() === true) {
						$tmpPathArray[] = $PARENT->getPathString();
					}
				}
				$tmpPathArray = array_reverse($tmpPathArray);
				$pathToCreate = trim(join("/", $tmpPathArray), "/");
				$CUSTOMURL->findByField('paths', $pathToCreate);
			}

			if ($CUSTOMURL->exists() === false) {
				//                helpers::outnl("creating path: " . $pathToCreate . "");
				$CUSTOMURL->setPaths($pathToCreate);
				// add pid same as parent
				$CUSTOMURL->setPid($rowdata["pid"]);
				$CUSTOMURL->update();
				//                helpers::out(" > created " . $CUSTOMURL->getUID() . "", "green");
			}

			$rowdata["paths"] = $CUSTOMURL->getUID();
		}
	}


	protected function updateDbRow(array $rowdata) {

		$this->beforeUpdateDbRowMethod($rowdata);

		$fields = array();
		$values = array();

		foreach ($rowdata as $key => $value) {
			$value        = $this->db->quote($value);
			$fields[]     = $key;
			$values[$key] = $value;
			$set[]        = $key . " = " . $value;
		}

		if ($this->itemExists === true) {
			$this->beforeDbUpdateQuery();

			// perform update
			$query = sprintf(
				"UPDATE %s SET %s WHERE uid = %s;",
				$this->tablename,
				join(", ", $set),
				$this->uid
			);

			$this->db->query($query);
			$this->afterDbUpdateQuery();

		} else {
			// perform insert
			$this->beforeDbInsertQuery();

			$query = sprintf(
				"INSERT INTO %s (`%s`) VALUES (%s);",
				$this->tablename,
				join("`,`", $fields),
				join(",", $values)
			);

			$this->db->query($query);
			$this->uid        = $this->db->lastInsertId();
			$this->itemExists = true;

			$this->afterDbInsertQuery();
		}
	}

	public function getPathString() {

		$pathsid = $this->getPaths();
		if (is_numeric($pathsid) === true) {
			$item = new dbRow_customurls();
			$item->findByField('uid', $pathsid);
			if ($item->exists() === true) {
				return $item->getPaths();
			}
		} else {
			return $pathsid;
		}
	}

	public function setLanguageUID($uid) {
		$this->sys_language_uid = $uid;

		return $this;
	}

	public function getLanguageUID() {
		return $this->sys_language_uid;
	}

	public function setL10nParent($pUid) {
		$this->l10n_parent = $pUid;

		return $this;
	}

	public function getL10nParent() {
		return $this->l10n_parent;
	}

	public function setKey($key) {
		$this->key = $key;

		return $this;
	}

	public function getKey() {
		return $this->key;
	}
}
