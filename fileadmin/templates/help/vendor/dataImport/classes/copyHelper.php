<?php

/**
 * Created by PhpStorm.
 * User: meekluc
 * Date: 18.08.14
 * Time: 18:30
 */
class copyHelper {

	static $abbr
		= array(
			'wp8'          => 'Windows Phone 8',
			'w81'          => 'Windows Phone 8.1',
			'wp7'          => 'Windows Phone 7',
			'mh5'          => 'MH5',
			'nokiax'       => 'Nokia X',
			'NokiaX'       => 'Nokia X',
			'suite'        => 'Android',
			'HEREapp'      => 'Android',
			'HEREapp.maps' => 'HERE',
			'web'          => 'Web',
			'aol'          => 'Nokia X',
			'andr'         => 'HERE',
			'citylens'     => 'HERE Lens',
			'drive'        => 'HERE Drive+',
			'maps'         => 'HERE Maps',
			'transit'      => 'HERE Transit',
			'transport'    => 'HERE Transport',
			'legacy'       => 'Web'
		);

	public static function resolveAbbr($abbr) {
		if (isset(self::$abbr[$abbr]) === true) {
			return self::$abbr[$abbr];
		}

		return $abbr;
	}

}
