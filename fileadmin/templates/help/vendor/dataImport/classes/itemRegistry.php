<?php

/**
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    04.08.14
 */
class itemRegistry {

	protected static $items;

	public static function set($id, $data, $override = false) {
		if (isset(self::$items[$id]) === true AND $override !== true) {
			throw new Exception("item exists");
		}
		self::$items[$id] = $data;
	}

	public static function get($id) {
		if (isset(self::$items[$id]) === false OR is_null(self::$items[$id]) === true) {
			return false;
		}

		return self::$items[$id];
	}

}
