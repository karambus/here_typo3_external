<?php

/**
 *
 * @category
 * @package
 *
 * @author   Tschitsch <dev@tschitsch.de>
 * @since    05.08.14
 */
class DB extends PDO {

	static $enableLog = false;
	static $writeToDbLog = false;
	static $logFile;

	public static $instance = null;

	public static function getInstance() {
		if (is_null(self::$instance) === true) {

			if (self::$writeToDbLog === true) {
				@unlink('dblog.log');
				self::$logFile = fopen('dblog.log', 'w+');
			}

			if (!defined("DB_DSN")) {
				helpers::abort("\nconst DB_DSN needs to be defined");
			}

			$username = defined("DB_USERNAME") ? DB_USERNAME : null;
			$password = defined("DB_PASSWORD") ? DB_PASSWORD : null;

			$options = array();
			if (defined("DB_INIT_COMMAND")) {
				$options[PDO::MYSQL_ATTR_INIT_COMMAND] = DB_INIT_COMMAND;
			}

			self::$instance = new DB(DB_DSN, $username, $password, $options);
		}

		return self::$instance;
	}


	public function query($sqlQuery) {

		if (self::$writeToDbLog === true) {
			fwrite(self::$logFile, trim($sqlQuery, ";") . ";\n");
		}
		if (self::$enableLog === true) {
			helpers::outnl("executing statement: ", "yellow");
			helpers::outnl(trim($sqlQuery, "\n\r"), 'lime');
		}

		$stmt = parent::query($sqlQuery);

		$ErrorInfo = $this->errorInfo();

		$ErrorCode = $ErrorInfo[0];
		$ErrorMsg  = $ErrorInfo[2];
		if ((int)$ErrorCode > 0) {
			helpers::outnl("Database Error: " . $ErrorCode . ": " . $ErrorMsg, "red");
			helpers::outnl("Failed Query: " . $sqlQuery, "red");
			helpers::abort("database queries not completed", 2);
		}

		return $stmt;
	}

	public function lastInsertId($seqname = null) {
		return parent::lastInsertId();
	}

}
