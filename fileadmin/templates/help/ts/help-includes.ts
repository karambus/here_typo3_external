#
# Setup
#
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/lib_metaNav.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/lib_breadCrumb.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/lib_footerNav.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/lib_searchBox.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/page.ts">

#
# Extensions
#
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/ext_indexedsearch.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/ext_powermail.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/help/ts/ext_herehelp.ts">