#
# Render the breadcrumb menu
#
lib.breadCrumb = HMENU
lib.breadCrumb {
	special = rootline
	special.range = 1|4
	entryLevel = 0
	1 = TMENU
	1 {
		noBlur = 1
		wrap =  <ul id="breadcrumbs">|</ul>

		NO {
			stdWrap.htmlSpecialChars = 1
			wrapItemAndSub.insertData = 1
			wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li>|</li>
			ATagTitle.field = title
		}

		CUR = 1
		CUR {
			stdWrap.htmlSpecialChars = 1
			wrapItemAndSub.insertData = 1
			wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li>|</li>
			doNotLinkIt = 1
		}

	}
}