TCEFORM.tt_content.section_frame {
     removeItems = 1,5,6,10,11,12,20,21
}

TCEFORM.tt_content.section_frame {
     addItems.100 = Searchresult
     addItems.105 = Content
     addItems.110 = Contact form
}

TCEFORM {
   tx_powermail_domain_model_forms {

      css {
         removeItems = layout1, layout2, layout3
         addItems {
	         left50percent = left floating 50% width item
	         right50percent = right floating 50% width item
	         block100percent = full width item
         }
      }
   }

   tx_powermail_domain_model_pages < .tx_powermail_domain_model_forms
   tx_powermail_domain_model_fields < .tx_powermail_domain_model_forms
}