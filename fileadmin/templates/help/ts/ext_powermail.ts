plugin.tx_powermail {
	view {
		templateRootPath = fileadmin/templates/help/html/powermail/Templates/
		partialRootPath = fileadmin/templates/help/html/powermail/Partials/
		plugin.tx_powermail.view.layoutRootPath = fileadmin/templates/help/html/powermail/Layouts/
	}
}