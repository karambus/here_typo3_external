#
# Indexed Search Konfiguration
#
plugin.tx_indexedsearch {
   templateFile = fileadmin/templates/help/html/indexedsearch.html
   _DEFAULT_PI_VARS{
      # standardm�ssig mit 'Wortteil' suchen statt mit ganzem Wort
      type = 1
      # Wieviele Suchergebnisse werden angegeben?
      results = 10
   }
   # standardm�ssige CSS-Styles l�schen
   #_CSS_DEFAULT_STYLE >

   # Weiss noch nicht wozu das hier ist:
   # forwardSearchWordsInResultLink = 1

   search {
      # Liste der Root-Seiten, in deren Struktur gesucht werden soll
      rootPidList = 1
      # Anzahl der auszugebenden treffer diese Einstellung funktioniert nicht mehr!!!!!
      # Siehe _DEFAULT_PI_VARS.results = 5
      # page_links = 5
   }

   # Anzeige regeln, ein- (1) bzw. ausgeblendet (0)
   show {
      # Advanced Search-Link ausschalten
      advancedSearchLink = 0
      # Suchregeln
      rules = 0
      # Ergebnisnummer anzeigen
      resultNumber = 1
      # Erstellungsinformationen des Hashes
      #       parsetimes = 1
      # Zweite ebene im Bereichs-dropdown anzeigen
      L2sections = 0
      # Erste ebene im Bereichs-dropdown anzeigen
      L1sections = 0
      # Alle "nicht im men�" oder "im men� verstecken"
      #(aber nicht "versteckte" seiten) mit anzeigen in section?
      #       LxALLtypes=0
      # leeren des Suchfeldes nach suche
      #       clearSearchBox = 0
      # Aktuelles suchwort zu den bisherigen suchw�rtern hinzuf�gen
      #       clearSearchBox.enableSubSearchCheckBox=0
   }

   # die Auswahlfelder f�r die Suchparameter werden ein- (0) bzw. ausgeblendet (1)
   blind {
      # Suchtyp (Ganzes, Wort, Wortteil, ..)
      type = 1
      # default option (Und, Oder)
      defOp = 1
      # Bereich(e) der website
      sections = 0
      # Suche in Medientypen
      media = 1
      # Sortierung
      order = 1
      # Ansicht (Sektionshierachie / Liste)
      group = 1
      # Sprachwahlbox
      lang = 1
      # Auswahl Sortierung
      desc = 1
      # Ergebnisse (Anzahl der Treffer pro Seite)
      results = 1
      # Ansicht: Erweiterte Vorschau
      extResume = 1
   }
   sectionlinks_stdWrap >
   sectionlinks_stdWrap{
      * |*| * |*| *
   }
}

#
# Suche f�r die Sprachen einschr�nken
#
plugin.tx_indexedsearch._DEFAULT_PI_VARS.lang = 0

[globalVar = GP:L = 1]
plugin.tx_indexedsearch._DEFAULT_PI_VARS.lang = 1
[global]