#
# build the footernav
#
lib.footerNav = COA
lib.footerNav {

    10 = HMENU
    10 {

        special = directory
        special.value = 10
        1 = TMENU
        1 {
            expAll = 1
            noBlur = 1
            wrap = <ul>|</ul>

            NO {
                stdWrap.htmlSpecialChars = 1
                wrapItemAndSub.insertData = 1
                wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li>|</li>
                ATagTitle.field = title
            }
        }
    }
}