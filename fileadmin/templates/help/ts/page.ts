#
# Content frames
#
tt_content.stdWrap.innerWrap.cObject = CASE
tt_content.stdWrap.innerWrap.cObject {
	key.field = section_frame
	100 = TEXT
	100.value = <div class="container"><section id="content" class="search">|</section></div>

	105 = TEXT
	105.value = <section id="content" class="index">|</section>

	110 = TEXT
	110.value = <div class="container"><section id="content" class="contact">|</section></div>
}

#
# Renders the PageTitle
#
temp.page_title = COA
temp.page_title {
	10 = TEXT
	10.data = page : title
}

#
# PAGE object
#
page >
page = PAGE
page {
	typeNum = 0

	meta {
		description = TEXT
		description.data = field : description
		keywords = TEXT
		keywords.data = field : keywords
		robots = index,follow
        X-UA-Compatible = IE=edge
	}

    headerData {
   		1 < temp.page_title
   		1.wrap = <title>|</title>


	    10 = TEXT
	    10.value = <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   	}

	10 = FLUIDTEMPLATE
	10 {
		file = fileadmin/templates/help/html/layouts/default.html
		partialRootPath = fileadmin/templates/help/html/partials/
		layoutRootPath = fileadmin/templates/help/html/layouts/
		variables {
            content < styles.content.get
            metaNav < lib.metaNav
			breadCrumb < lib.breadCrumb
            footerNav < lib.footerNav
			searchBox < lib.searchbox
		}
	}

    includeCSS {
	    file1 = fileadmin/templates/help/css/app.css
    }

    includeJSFooter {
	    file1 = fileadmin/templates/help/js/core.js
	    file2 = fileadmin/templates/help/js/app.js
    }
}

#
# Change bodyTag on contact form page
#
[globalVar=TSFE:id=22]
page.bodyTag = <body id="view-contact">
[global]

#
# Configuration for automatic language detection
#
plugin.tx_rlmplanguagedetection_pi1 >

#plugin.tx_rlmplanguagedetection_pi1 {
#    defaultLang = en
#    useOneTreeMethod = 1
#	debug = 1
#}
#page.2000 =< plugin.tx_rlmplanguagedetection_pi1
#plugin.tx_rlmplanguagedetection_pi1.dontBreakIfLanguageIsAlreadySelected = 1