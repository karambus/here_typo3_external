# --- SEARCHBOX OBJECT ----------------------------------
lib.searchbox = COA_INT
lib.searchbox {
  stdWrap.prefixComment = 2 | lib.searchbox
  10 = TEXT
  10.typolink.parameter = {$plugin.tx_indexedsearch.searchUID}
  10.typolink.returnLast = url
  10.wrap = <form action="|?tx_herehelp_help[controller]=Search&tx_herehelp_help[action]=search" method="post" id="indexedsearch"><label for="search-query"></label>
  20 = COA
  20 {
    wrap = |
    10 = TEXT
    10.data = GP : tx_herehelp_help |sword
    10.htmlSpecialChars = 1
	10.wrap = <input name="tx_herehelp_help[sword]" value="|" type="search" id="search-query" autocomplete="off" placeholder="Search for help" />
    20 = COA
    20 {
	  wrap = |
      10 = TEXT
      10.value = <input name="tx_herehelp_help[submit_button]" value="Search" type="hidden" />
    }
  }
  30 = COA
  30 {
    wrap = <tr>|</tr>
    10 = TEXT
    10.value = Advanced search �
    10.typolink.parameter = {$plugin.tx_indexedsearch.searchUID}
    10.typolink.additionalParams = &tx_indexedsearch[ext]=1
    10.wrap = <td align="right" colspan="2">|</td>
    if.isTrue = {$plugin.tx_indexedsearch.showAdvanced}
  }
  wrap = | </form>
}