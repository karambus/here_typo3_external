[globalString = IENV:HTTP_HOST = meehere.bald-live.de]
plugin.tx_herehelp.settings.baseUrlForRedirect = meehere.bald-live.de/help
[global]

[globalString = IENV:HTTP_HOST = help.here.dev]
plugin.tx_herehelp.settings.baseUrlForRedirect = help.here.dev/help
[global]

[globalString = IENV:HTTP_HOST = herelocal.dev]
plugin.tx_herehelp.settings.baseUrlForRedirect = herelocal.dev/help
[global]

[globalString = IENV:HTTP_HOST = typo3.here.dev]
plugin.tx_herehelp.settings.baseUrlForRedirect = typo3.here.dev/help
[global]