<?php
include_once(PATH_site.'typo3/sysext/cms/tslib/class.tslib_content.php');
class user_jsonTools {
    function pageToJson ($content = '' , $conf = array()) 
	{
        global $TSFE;
        //remove some unnecessary HTML elements:
		$element0 = "class=\"csc-default\">";
		$element1 = "<h1 class=\"csc-firstHeader\">";
		$element2 = "</div>";
		$pos = strpos($content, $element0);
		$content = substr($content, $pos + strlen($element0));
		$pos = strpos($content, $element1);
		// remove header if available
		if ($pos != false) {
			$content = substr($content, $pos);
			$pos = strpos($content, $element2);
			$content = substr($content, $pos + strlen($element2));
		}
		// remove final </div>
		$content = substr($content, 0, -strlen($element2));
        $cObj = t3lib_div::makeInstance('tslib_cObj');
        $arrReturn = array();
        //$arrReturn['title'] = $TSFE->page['title'];
        //$arrReturn['id'] = $TSFE->page['uid'];
        $arrReturn['html'] = $content;
		//$arrReturn['urlSegment'] = $cObj->currentPageUrl();
        return json_encode($arrReturn);
		

    }
}
?>