# include jsonProcessor 
includeLibs.jsonTools = fileadmin/templates/legal/scripts/user_jsonTools.php 

#remove anything except the content
config.disableAllHeaderCode = 1 
config.doctype = none 

# set json header
config.additionalHeaders = Content-type:application/json 

# keep typo3 from "tidying up" perfectly valid json
config.xhtml_cleaning = 0 

# json encoder 
page.stdWrap.postUserFunc = user_jsonTools->pageToJson