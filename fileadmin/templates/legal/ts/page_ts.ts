#
# Selectable FrontEnd layouts
#
TCEFORM.pages.layout {
	altLabels {
		0 = Landing page
		1 = Content page
	}
	removeItems = 2,3
}