#
# Gridelements config
#
# 1 = Legal teaserboxes
#
tt_content.gridelements_pi1.20.10.setup {
	1 < lib.gridelements.defaultGridSetup
	1 {
		columns {
			10 < .default
			10.wrap = |
		}
	}
}