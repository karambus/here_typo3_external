#
# includes the renderAction from SelectorContoller of here_legal extension
#
lib.selector = USER
lib.selector {
	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
	extensionName = HereLegal
	pluginName = Legal
	vendorName = Meelogic
	controller = Select
	action = render
	switchableControllerActions {
		Select {
			1 = render
		}
	}
	view < plugin.tx_herelegal.view
	persistence < plugin.tx_herelegal.persistence
	settings < plugin.tx_herelegal.settings
}