#
# Import splash background image from "Resources"
#
lib.splashImage = IMG_RESOURCE
lib.splashImage {
	file {
		import.data = levelmedia:-1, slide
		treatIdAsReference = 1
		import.listNum = 0
	}
}

#
# Import splash content from "abstract" field
#
lib.splashContent = TEXT
lib.splashContent.field = abstract