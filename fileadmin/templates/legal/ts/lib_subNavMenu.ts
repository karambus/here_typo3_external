#
# includes the renderAction from SelectorContoller of here_legal extension
#
lib.subNavMenu = USER
lib.subNavMenu {
	userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
	extensionName = HereLegal
	pluginName = Legal
	vendorName = Meelogic
	controller = SubMenu
	action = render
	switchableControllerActions {
		SubMenu {
			1 = render
		}
	}
	view < plugin.tx_herelegal.view
	persistence < plugin.tx_herelegal.persistence
	settings < plugin.tx_herelegal.settings
}