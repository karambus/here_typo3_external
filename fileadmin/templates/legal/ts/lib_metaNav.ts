
#
# build the metanav
#
lib.metaNav = COA
lib.metaNav {

    10 = HMENU
    10 {

        special = directory
        special.value = {$metaNavId}
        1 = TMENU
        1 {
            expAll = 1
            noBlur = 1
            wrap = <div id="menu"><ul role="menu">|</ul></div>

            NO {
                stdWrap.htmlSpecialChars = 1
                wrapItemAndSub.insertData = 1
                wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li>|</li>
                    ATagTitle.field = title
            }

            ACT = 1
            ACT {
                stdWrap.htmlSpecialChars = 1
                wrapItemAndSub.insertData = 1
                wrapItemAndSub = <li>|</li> |*| <li>|</li> |*| <li>|</li>
                    ATagParams = class="active"
            }

        }
    }
}

