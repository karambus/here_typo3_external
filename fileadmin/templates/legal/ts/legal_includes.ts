#
# Setup
#

<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/lib_metaNav.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/lib_footerNav.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/lib_subNavMenu.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/lib_selector.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/lib_splash.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/page.ts">

#
# Extensions
#
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/ext_gridelements.ts">
<INCLUDE_TYPOSCRIPT:source="file:fileadmin/templates/legal/ts/ext_herelegal.ts">

#
# JSON
#
#<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/legal/scripts/jsonProcessor.inc">
#<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/legal/scripts/user_jsonTools.php">