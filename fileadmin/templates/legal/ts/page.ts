#
# Renders the PageTitle
#
temp.page_title = COA
temp.page_title {
	10 = TEXT
	10.data = page : title
}

#
# PAGE object
#
page >
page = PAGE

#
# JSON
#

[globalString = GP:out=json] 
	<INCLUDE_TYPOSCRIPT: source="FILE: fileadmin/templates/legal/scripts/jsonProcessor.inc"> 
[global]

page {
	typeNum = 0

	meta {
		description = TEXT
		description.data = field : description
		keywords = TEXT
		keywords.data = field : keywords
		robots = index,follow
	}

    headerData {
   		1 < temp.page_title
   		1.wrap = <title>|</title>


	    10 = TEXT
	    10.value = <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

   	}

	10 = FLUIDTEMPLATE
	10 {
		file = fileadmin/templates/legal/html/layouts/default_index.html
		partialRootPath = fileadmin/templates/legal/html/partials/
		layoutRootPath = fileadmin/templates/legal/html/layouts/
		variables {
            content < styles.content.get
			contentLeft < styles.content.getLeft
            metaNav < lib.metaNav
            footerNav < lib.footerNav
            subNav < lib.subNavMenu
            selector < lib.selector
			splashImage < lib.splashImage
			splashContent < lib.splashContent
		}
	}

    includeCSS {
	    file1 = fileadmin/templates/legal/css/app.css
    }

    includeJSFooter {
	    file1 = fileadmin/templates/legal/js/core.js
	    file2 = fileadmin/templates/legal/js/app.js
    }
}

#
# Assign templates based on BackEnd selection
#
[globalVar = TSFE:page|layout = 1]
	page.10.file = fileadmin/templates/legal/html/layouts/default_detail.html
[global]
[globalString = GP:out=json]
	page.10.file = fileadmin/templates/legal/html/layouts/default_detail_json.html
[global]
