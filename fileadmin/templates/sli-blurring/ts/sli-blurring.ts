#
# PAGE object
#
page = PAGE
page {
	typeNum = 0

	meta {
		description = TEXT
		description.data = field : description
		keywords = TEXT
		keywords.data = field : keywords
		robots = index,follow
        viewport = width=device-width, initial-scale=1, maximum-scale=1
	}

	headerData {
		1 < temp.page_title
		1.wrap = <title>|</title>
	}

	10 = FLUIDTEMPLATE
	10 {
		file = fileadmin/templates/sli-blurring/html/index.html
		partialRootPath = fileadmin/templates/sli-blurring/html/partials/
		layoutRootPath = fileadmin/templates/sli-blurring/html/layouts/
		variables {
            content < styles.content.get
		}
	}

    includeCSS {
        file1 = fileadmin/templates/bootstrap/css/bootstrap.min.css
        file2 =
    }

    includeJSFooter {
        file1 = fileadmin/templates/bootstrap/js/jquery.min.js
    	file2 = fileadmin/templates/bootstrap/js/bootstrap.min.js
    }

}