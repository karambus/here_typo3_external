#
# Split headline and content into two different markers
#
temp.pageHeadline = CONTENT
temp.pageHeadline {
    table = tt_content
    select{
        pidInList = this
        where = colPos = 0
		languageField = sys_language_uid
    }
    renderObj = TEXT
    renderObj.field = header
}

temp.bodytext = CONTENT
temp.bodytext {
    table = tt_content
    select {
        pidInList = this
        where = colPos=0
		languageField = sys_language_uid
    }
    renderObj = COA
	renderObj {
		10 = TEXT
		10.field = bodytext
		10.wrap = |
		10.parseFunc < tt_content.text.20.parseFunc
	}
}

#
# We don't need the spam protection for this landingpage
#
config.spamProtectEmailAddresses = 0

#
# PAGE object
#
page = PAGE
page {
	typeNum = 0

	meta {
		description = TEXT
		description.data = field : description
		keywords = TEXT
		keywords.data = field : keywords
		robots = index,follow
        viewport = width=device-width, initial-scale=1, maximum-scale=1
	}

	headerData {
		1 < temp.page_title
		1.wrap = <title>|</title>
	}

	10 = FLUIDTEMPLATE
	10 {
		file = fileadmin/templates/help-landingpage/html/index.html
		partialRootPath = fileadmin/templates/help-landingpage/html/partials/
		layoutRootPath = fileadmin/templates/help-landingpage/html/layouts/
		variables {
            headline < temp.pageHeadline
            bodytext < temp.bodytext
		}
	}

    includeCSS {
        file1 = fileadmin/templates/help-landingpage/css/styles.css
        file2 =
    }

    includeJSFooter {
        file1 =
    	file2 =
    }

    bodyTag = <body>

}

#
# Configuration for automatic language detection
#
plugin.tx_rlmplanguagedetection_pi1 {
    defaultLang = en
    useOneTreeMethod = 1
}
page.1000 =< plugin.tx_rlmplanguagedetection_pi1
plugin.tx_rlmplanguagedetection_pi1.dontBreakIfLanguageIsAlreadySelected = 1