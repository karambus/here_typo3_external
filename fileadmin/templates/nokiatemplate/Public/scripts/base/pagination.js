/**
 * @description Find Anchors in the pagination and scroll to their references. Disabel the standard event for anchors "
 *
 * @author sebastian.grosch@razorfish.de
 * @version 1.0.0
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 *
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true,  -$:true */
/* global core:true */


window.namespace("here.pagination");

here.pagination = (function(){		
	
	_addEvents = function () {		
		jQuery('.fx--pagination a[href*=#]').bind("click", _paginationScroll);
	},
	
	_paginationScroll = function (event) {		
		jQuery('.fx--pagination ul li a').removeClass('selected');
		jQuery(this).addClass('selected');
		event.preventDefault();
		
		// Save Target URL in variable
			var href = $(this).attr("href");
			var ziel = $(this).attr("href").replace('#','');			
			var zielelm = jQuery('#content--main').find('a[name="'+ziel+'"]');
			console.log(zielelm.offset().top);
			
			if(href == '#anchor_1'){	
				//Scroll to first target element
				$('html,body').animate({
					scrollTop: 0
				// Animation duration and fallback function set the default event to true and set hash to the url
				}, 1000 ); //function (){location.hash = ziel;});
			}
			else{
				//Scroll to target element
				$('html,body').animate({
				scrollTop: jQuery(zielelm).offset().top
				// Animation duration and fallback function set the default event to true and set hash to the url
				}, 1000 ); //function (){location.hash = ziel;});				
			}			
							
	},

	/**
	 * Constructor
	 * @return  {Void}
	 * @private
	 */
	_initialize = function () {
		_addEvents();
	};

	/**
	 * Public API
	 */
	return {		

		/**
		 * Constructor
		 * @return {Void}
		 */
		initialize:_initialize		
	};

})();

// initialize
jQuery(document).on("ready",function() {

	"use strict";
	here.pagination.initialize();
});