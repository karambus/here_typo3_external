/**
 * @module here.controller.skrollr
 * @description handles the skrollr animation library.
 * We're not using it on mobile or smaller screens.
 * There's the necessity to set different values depending on the screen width / content height.
 *
 * @author martin.krause@razorfish.de
 * @contributors martin.krause@razorfish.de
 * @version 1.0.0
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true, -$:true */
/* global here:true, skrollr: true */

/**
* @ignore
*/
window.namespace("here.controller.skrollr");


/**
 * @ignore
 */
here.controller.skrollr = (function (){

	"use strict";

	// private vars
	var
		_aChannels = ["/assets/ready"],
		_isMobileOs = false,
		_skrollr = null,
		_iAmountScroll = null,
		_iMediaQuery = null,
		// _iWidthViewport,
		_$elScrollContent,
		_$elFrameContent,

	/**
	 * add "class" specific events
	 * @private
	 */
	_addEvents = function () {
		// if possible use the event delegation pattern
		// jQuery("body").on("click", ".js--TEMPLATE-module", {action:"EVENT"}, here.controller.skrollr.onEvent);
		jQuery(window).on("resizeThrottled", {action:"resize"}, here.controller.skrollr.onEvent);
	},

	/**
	 * Subscribe to a channel
	 * @param   {String} sChannel_ Channel
	 * @private
	 */
	_subscribe = function (sChannel_) {
		jQuery.channel("subscribe", sChannel_, here.controller.skrollr.onChannel);
	},

	/**
	 * Publish to channel(s)
	 * @param   {String} sChannel_ channel
	 * @private
	 */
	_publish = function (sChannel_, oData_) {
		jQuery.channel("publish", sChannel_, oData_);
	},

	/**
	 * channel handler
	 * @param  {Object} sChannel_	normalized channel on which the message was published
	 * @param  {Object} oData_		additional message data
	 * @private
	 */
	_handler = function (sChannel_, oData_) {

		var _iMediaQueryNow;

		switch (sChannel_) {

			case "/CHANNEL":
				_publish("/FOO/BAR", {});
			break;

			case "resize":

				_iMediaQueryNow = _getCurrentMediaQuery();
				// check if there's a need for skrollr: wider than 768px and not on a mobile os
				if( _iMediaQueryNow > 1 && !_isMobileOs ) {

						jQuery("html")
							.removeClass("has-no-skrollr")
							.addClass("has-skrollr");

					// did the media query change and is there already a skrollr instance?
					if(_iMediaQuery !== _iMediaQueryNow  && _skrollr) {

						// in this case: remove the skrollr
						_skrollr.destroy();
						_skrollr = null;

					}

					// we don't have a skrollr instance but we need one
					if(!_skrollr) {
						_initializeSkrollr();
					}

				}
				// no need for a skrollr instance
				else {

					jQuery("html")
						.removeClass("has-skrollr")
						.addClass("has-no-skrollr");

					if(_skrollr) {
						_skrollr.destroy();
						_skrollr = null;
					}
				}

				// save media query
				_iMediaQuery = _iMediaQueryNow;

			break;

		}

	},

	/**
	 * Calculates which media querry is currently used
	 * @return {Numer} a Number indicating which media query is in use
	 */
	_getCurrentMediaQuery = function () {

		var _iWidthViewport = _getScreenSize();

		// phone / phablet
		if(_iWidthViewport <= 768) {
			return 1;
		}

		// tablet - desktop
		if(_iWidthViewport <= 1024) {
			return 2;
		}

		// wide desktop
		return 3;

	},

	/**
	 * Calculates the amount of pixel we have to scroll to see the whole scrolling content
	 * @return {Void}
	 */
	_getScrollAmount = function () {
		// _iAmountScroll = _$elScrollContent.height() - _$elFrameContent.height() + parseInt(_$elFrameContent.css("marginTop")) + parseInt(_$elFrameContent.css("marginBottom"));
		_iAmountScroll = _$elScrollContent.outerHeight() - _$elFrameContent.height();
	},


	/**
	 * Cache elements to minimize DOM-Operations
	 * @return {Void}
	 */
	_cacheElements = function () {
		_$elScrollContent = jQuery("#content--scroll");
		_$elFrameContent = jQuery("#content--main");

	},

	/**
	 * Create the skrollr instance and set all necessary values
	 * @return {Void}
	 */
	_initializeSkrollr = function () {

		if(_skrollr) { return; }

		// calculate scroll anount and set the skrollr attribute on the content element
		_getScrollAmount();
		console.log(_iAmountScroll );
		_$elScrollContent.attr("data-end","margin-top:-"+ ( _iAmountScroll   ) +"px");

		_skrollr = skrollr.init({
			easing: "sqrt",
			forceHeight: true
		});
	},

	/**
	 * Check it we're using a mobile os
	 * @return {Bool} true in case of mobile os
	 */
	_testMobileOs = function () {
		return /(android|iemobile|ip(hone|od|ad))/i.test(navigator.userAgent);
	},

	/**
	 * Check it the screensize is bigger than the "fluid" breakpoint
	 * @return {Bool} true in case of  desktop version
	 */
	_getScreenSize = function () {
		return jQuery(window).width();
 	},

	/**
	 * Setup function
	 * @private
	 */
	_setup = function () {
		_cacheElements();
		_isMobileOs = _testMobileOs();
		_iMediaQuery = _getCurrentMediaQuery();
		_handler("resize");
	},


	/**
	 * Constructor
	 * @private
	 */
	_initialize = function () {
		var _i
		;

		for (_i = 0; _i < _aChannels[_i]; _i++ ) {
			_subscribe(_aChannels[_i]);
		}

		_addEvents();
		_setup();
		_publish("/controller/skrollr/ready");
	};

	/**
	 * Public API
	 */
	return {

		/**
		 * jQuery.Channel handler
		 * @param  {Object} oData_ additional message data
		 */
		onChannel: function (oData_) {
			_handler(oData_.originalChannel, oData_);
		},

		/**
		 * DOM event handler
		 * @param  {Event} event_ jQuery-DOM-event
		 * @param  {Object} oData_ additional message data
		 * @return {Bool}	false
		 */
		onEvent: function (event_, oData_) {
			_handler(event_.data.action, event_);
			return false;
		},


		/**
		 * Constructor
		 * @return {Void}
		 */
		initialize:_initialize
	};

})();

/**
 * @ignore
 */
jQuery(document).ready(function () {
	"use strict";
	here.controller.skrollr.initialize();
});
