/**
 * @module *
 * @description The main applicaiton file
 * It *does not contain any methods*, just three startup helpers:
 * - an anonymous function which takes a bunch of promises an starts the application as soon as all are resolved
 * - a document.ready handler
 * - a window.load hanlder
 *
 * @author martin.krause@razorfish.de
 * @version 1.5
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 *
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true,  -$:true */
/* global here:true */

/**
* @ignore
*/
window.namespace("here");

/**
 * Execute on parsing the file
 */
( function () {

	"use strict";

	var
		// setup the channels necessary for starting the app
		_aChannels = [
			"/asses/ready",
			"/controller/skrollr/ready"
		],
		_aPromises = [],
		_i = 0
	;

	// register a promises to each condition
	for (; _i < _aChannels.length; _i++) {
		_aPromises.push(new jQuery.Deferred());
		jQuery.channel("subscribe", _aChannels[_i], _aPromises[_i].resolve);
	}

	jQuery
		.when.apply(jQuery, _aPromises)
		.done(function () {
			// start up the application
			jQuery.channel("publish", "/application/start", {});
		});


})();


here.isMobile = (function(ua_) {
	"use strict";
	 return /(android|iemobile|ip(hone|od|ad))/i.test(ua_);
})(navigator.userAgent );




/**
 * Execute as soon as the dom is ready
 */
jQuery(document).ready(function () {
	"use strict";
});

/**
 * Execute as soon as every asses is loaded
 */
jQuery(window).on("load", function () {
	"use strict";
});
