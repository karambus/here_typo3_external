/**
 * @description Set elements like the footer by resize the site "
 *
 * @author sebastian.grosch@razorfish.de
 * @version 1.0.0
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 *
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true,  -$:true */
/* global core:true */


window.namespace("here.footer.responsive");

here.footer.responsive = (function(){
	
	// private vars
	var
		_iFooterWidth = jQuery('#footer--main').width()+ 40, 
		_iWinwidth = '',
		_iContentMainWidth = '',
		_iSpace	= '',
	
	_addEvents = function () {		
		jQuery(window).on("resizeThrottled", _setfooter);
	},
	
	_setfooter = function () {
		_iWinwidt = window.innerWidth;
		_iContentMainWidth = jQuery('#content--main').width() + 80;		
		_iSpace	= _iWinwidt - _iContentMainWidth;
		console.log(_iSpace + _iFooterWidth);
		
		if(_iSpace < _iFooterWidth){
			jQuery('#footer--main').css({
				'left'	: _iContentMainWidth - _iFooterWidth,
				'width'	: '100%'
			});
			
			jQuery('#content--main').css({
				'margin-bottom'	: 70
			});		
						
		}
		else{
			jQuery('#footer--main').css({
				'right'	: 40,				
				'left'	: 'auto',
				'width'	: 'auto'				
			});				
				
			jQuery('#content--main').css('margin-bottom', 40);
		}
	},


	/**
	 * Constructor
	 * @return  {Void}
	 * @private
	 */
	_initialize = function () {	
		console.log('init');	
		_addEvents();
		_setfooter();

	};

	/**
	 * Public API
	 */
	return {		

		/**
		 * Constructor
		 * @return {Void}
		 */
		initialize:_initialize		
	};

})();

// initialize
jQuery(document).on("ready",function() {

	"use strict";
	here.footer.responsive.initialize();
});