/**
 * @module window.namespace
 * @description Creates additional namespaces.
 *
 * @author martin.krause@razorfish.de
 * @contributors martin.krause@razorfish.de
 * @version 1.0.1
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 */
/* jshint browser:true, jquery:true, strict: true */

/*
 * Creates additional namespaces.
 *
 * @example
 * // create typeof ( window.FOO.BAR.BAZ ) === "object"
 * window.namespace("FOO.BAR.BAZ")
 *
 * @param {String} sNamespace_ Namespace to create
 * @return {Object}
 */
window.namespace = function (sNamespace_) {

	"use strict";

	var
		_aSub = sNamespace_.split("."),
		_i,
		_oBase = null,
		_sType = typeof window[_aSub[0]]
	;

	// top-level namespace is already defined but not an object
	if (_sType !== "undefined" && _sType !== "object" ) {
		throw new TypeError([_aSub[0], " already defined as ", _sType].join(""));
	}

	// force first item to be an object
	if (_sType === "undefined" ) {
		window[_aSub[0]] = {};
	}

	// set basic namespace to the first item of the array
	_oBase = window[_aSub[0]] ;

	// create!
	for (_i = 1; _i < _aSub.length; _i++) {
		// don't have current obj
		if (typeof(_oBase[_aSub[_i]]) === "undefined") {
			// add obj
			_oBase[_aSub[_i]] = {};
		}
		_oBase = _oBase[_aSub[_i]];
	}
	// return reference
	return _oBase;

};
