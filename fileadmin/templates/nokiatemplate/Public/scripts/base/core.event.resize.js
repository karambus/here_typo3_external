/**
 * @description Throttles the resize event to be send every 50ms als "resizeThrottled"
 *
 * @author martin.krause@razorfish.de
 * @version 1.5
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 *
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true,  -$:true */
/* global core:true */

/**
 * @ignore
 */
window.namespace("core.event.resize");

/**
 * @ignore
 */
core.event.resize = (function(){

	"use strict";

	// private vars
	var
		_iThrottle = 50,
		_bLockHandler = false,
		_isIOS = false,


	/**
	 * Actual scroll handler.
	 * @return {Void}
	 */
	_doResize = function () {
		// trigger the new event
		jQuery(window).trigger("resizeThrottled");
		// and remove scroll lock
		_bLockHandler = false;
	},


	/**
	 * resize handler proxy
	 * @param  {Event} event_ jQuery-event
	 * @return {Void}
	 */
	_onResize = function (event_) {
		// check if there's currently a running handler, ignore on iOS (resize triggered after scrolling is complete)
		if (_bLockHandler === false && _isIOS === false ) {
			_bLockHandler = true;
			// call handler
			window.setTimeout(core.event.resize.doResize, _iThrottle);		
		}
	},


	/**
	 * Constructor
	 * @return  {Void}
	 * @private
	 */
	_initialize = function () {
		var
			_isIOS = false // add check if necessary
		;

		jQuery(window).on("resize", core.event.resize.onEventresize);

	};

	/**
	 * Public API
	 */
	return {

		/**
		 * Resize-event handler: sets the lock and the timeout
		 * @return {Void}
		 */
		onEventresize:_onResize,

		/**
		 * Removes the scroll lock and triggers the new event
		 * @return {Void}
		 */
		doResize:_doResize,

		/**
		 * Constructor
		 * @return {Void}
		 */
		initialize:_initialize
	};

})();

// initialize
jQuery(document).on("ready",function() {

	"use strict";

	core.event.resize.initialize();
});








