/**
 * @module window.core
 * @description Provides core functionalities and some library-wide helpers
 *
 * @author martin.krause@razorfish.de
 * @version 1.5
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 *
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true,  -$:true */
/* global core:true */

/**
* @ignore
*/
window.namespace("core");

/**
 * Clears timer (interval & timeout)
 * @param {Object} timer_ Interval & timeout
 * @example myTimer = audi.rs.clearTimer(myTimer)
 * @return {Null}
 */
core.clearTimer = function (timer_) {

	"use strict";

	if (!timer_) {
		return null;
	}
	// clear timer
	window.clearTimeout(timer_);
	window.clearInterval(timer_);
	return null;
};


/**
* @ignore
*/
jQuery(document).data("UNIQUE_sBase", "core" );
jQuery(document).data("UNIQUE_iCounter", new Date().getTime() );

/**
 * Returns a unique ID.
 * @return {String}
 */
core.getUniqueId = function () {

	"use strict";

	// return unique id: UNIQUE_sBaseUNIQUE_iCounter
	jQuery(document).data("UNIQUE_iCounter", jQuery(document).data("UNIQUE_iCounter") + 1) ;
	return [jQuery(document).data("UNIQUE_sBase"), jQuery(document).data("UNIQUE_iCounter")].join("_");
};


/**
 * Returns a random number between iMin_ and iMax_.
 * @param {Number} iMin_ lowest possible value
 * @param {Number} iMax_ highest possible vlaue
 * @see http://aktuell.de.selfhtml.org/artikel/javascript/zufallszahlen/
 * @return {Number}
 */
core.getRandomNumber = function (iMin_, iMax_) {

	"use strict";

	return Math.floor(Math.random() * (iMax_ - iMin_ + 1)) + iMin_;
};


/**
* initialize everything
* @ignore
*/
jQuery(document).ready(function () {

	"use strict";

	jQuery("html").removeClass("no-js");
});

jQuery(document).ready(function () {


	// $('body').height(
	// 	$('body').height() + jQuery('#content--scroll').height() - jQuery('#content--main').height()
	// )

});

