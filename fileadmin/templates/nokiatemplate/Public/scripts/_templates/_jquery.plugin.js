/**
 *
 * @description A simple template for jquery plugins
 *
 * @see :
 * http://www.learningjquery.com/2007/10/a-plugin-development-pattern
 * http://learn.jquery.com/plugins/basic-plugin-creation/
 * http://learn.jquery.com/plugins/advanced-plugin-concepts/
 *
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true, -$:true */

/**
 * @ignore
 */
(function ($) {

	"use strict";

	/**
	 * @method
	 * @api
	 * @param  {Object} oOpts_ Plugin options
	 * @return {[type]}
	 */
	$.fn.PLUGIN = function (oOpts_) {

		_private(this);

		// merge default options and supplied options
		var _oOpts = $.extend({}, $.fn.PLUGIN.options, oOpts_);
		// iterate and reformat each matched element
		return this.each(function () {
			var _$element = jQuery(this);
			$.fn.PLUGIN.public();
		});
	};

	/**
	* @private
	*/

	/**
	 * [_private description]
	 * @private
	 * @return {[type]}
	 */
	function _private () {
		window.console.log("private this", this);
	}

	/**
	 * [public description]
	 * @api
	 * @return {[type]}
	 */
	$.fn.PLUGIN.public = function () {
		window.console.log("public this", this);
	};

	// plugin default options
	$.fn.PLUGIN.options = {
		foreground: "red",
		background: "yellow"
	};

// set scope
})(jQuery);
