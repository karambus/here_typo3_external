/**
 * @module TEMPLATE.module
 * @description TEMPLATE description
 *
 * @author martin.krause@razorfish.de
 * @contributors martin.krause@razorfish.de
 * @version 1.0.0
 * @copyright Razorfish GmbH
 *
 * Use scriptDoc comments - ever method requires a useful description
 * Visit http://docs.aptana.com/docs/index.php/ScriptDoc_comprehensive_tag_reference
 * for a help on the scriptDoc syntax
 */
/* jshint browser:true, jquery:true, strict: true */
/* global jQuery:true, -$:true */
/* global TEMPLATE:true */

/**
* @ignore
*/
window.namespace("TEMPLATE.module");


/**
 * @ignore
 */
TEMPLATE.module = (function (){

	"use strict";

	// private vars
	var
		_aChannels = ["/CHANNEL"],

	/**
	 * add "class" specific events
	 * @private
	 */
	_addEvents = function () {
		// if possible use the event delegation pattern
		jQuery("body").on("click", ".js--TEMPLATE-module", {action:"EVENT"}, TEMPLATE.module.onEvent);
	},

	/**
	 * Subscribe to a channel
	 * @param   {String} sChannel_ Channel
	 * @private
	 */
	_subscribe = function (sChannel_) {
		jQuery.channel("subscribe", sChannel_, TEMPLATE.module.onChannel);
	},

	/**
	 * Publish to channel(s)
	 * @param   {String} sChannel_ channel
	 * @private
	 */
	_publish = function (sChannel_, oData_) {
		jQuery.channel("publish", sChannel_, oData_);
	},

	/**
	 * channel handler
	 * @param  {Object} sChannel_	normalized channel on which the message was published
	 * @param  {Object} oData_		additional message data
	 * @private
	 */
	_handler = function (sChannel_, oData_) {
		// console.log("sChannel_", sChannel_, oData_);
		switch (sChannel_) {

			case "/CHANNEL":
				_publish("/FOO/BAR", {});
			break;

			case "EVENT":
			break;

		}


	},


	/**
	 * Setup function
	 * @private
	 */
	_setup = function () {

	},


	/**
	 * Constructor
	 * @private
	 */
	_initialize = function () {
		var _i
		;

		for (_i = 0; _i < _aChannels[_i]; _i++ ) {
			_subscribe(_aChannels[_i]);
		}

		_addEvents();

	};

	/**
	 * Public API
	 */
	return {

		/**
		 * jQuery.Channel handler
		 * @param  {Object} oData_ additional message data
		 */
		onChannel: function (oData_) {
			_handler(oData_.originalChannel, oData_);
		},

		/**
		 * DOM event handler
		 * @param  {Event} event_ jQuery-DOM-event
		 * @param  {Object} oData_ additional message data
		 * @return {Bool}	false
		 */
		onEvent: function (event_, oData_) {
			_handler(event_.data.action, event_);
			return false;
		},

		/**
		 * Returns application status object
		 * @api
		 * @return {Object} status object
		 */
		/** @dev-*/
		getStatus: function () {
			return ;
		},
		/**-@dev*/

		/**
		 * Constructor
		 * @return {Void}
		 */
		initialize:_initialize
	};

})();

/**
 * @ignore
 */
jQuery(document).ready(function () {
	"use strict";
	TEMPLATE.module.initialize();
});
