// START (./grunt/hook-templates/pre-commit.js)
var exec = require("child_process").exec;

exec("git rev-parse --abbrev-ref=strict HEAD",function (err, stdout, stderr) {

	// skip hook on feature branches
	if(stdout.indexOf("feature/") !== -1) {
		console.log("on branch '"+stdout+"': skipping pre-commit-hook");
		process.exit(0);
	}

	// execute grunt tasks
	exec("grunt {{task}}", {
			// cwd: "{{expectedWorkingDir}}"
		}, function (err, stdout, stderr) {

		var exitCode = 0;

		console.log(stdout);

		if (err) {
			console.log(stderr);
			exitCode = -1;
		}

		process.exit(exitCode);
	});

});
// END (./grunt/hook-templates/pre-commit.js)
