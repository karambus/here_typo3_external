/**
 * Razorfish Gruntfile: configuration / tasks for jshint.
 *
 * @description		Validate files with JSHint.
 * @see				https://github.com/gruntjs/grunt-contrib-jshint
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			jshint: {
				project: {
					src: "<%= pkg.files.js.project.src %>"
				},
				vendor: {
					src: "<%= pkg.files.js.vendor.src %>"
				},
				unified: {
					src: "<%= pkg.files.js.unified.dest %>"
				}
			}
		}
	);
};
