/**
 * Razorfish Gruntfile: configuration / tasks for githooks.
 *
 * The hooks will be initialized via the setup-task.
 *
 * Make sure you upade the hooks!
 *
 * @description		A Grunt plugin to help bind Grunt tasks to Git hooks
 * @see				https://github.com/wecodemore/grunt-githooks
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	grunt.config.merge(
		{
			githooks: {
				all: {
					options: {
						template: "./grunt/templates/pre-commit.js"
					},
					"pre-commit":  "pre-commit",
					"post-checkout": "post-checkout"
				}
			}
		}
	);

	/**
	 * Pre commit hook
	 * - lints the project css
	 * - lints the project js
	 * - runs all tests
	 */
	grunt.registerTask("pre-commit", "pre-commit hook: validates js & css files, runs all tests", function () {
		grunt.fail.fatal("./grunt/tasks/githooks.js: SETUP NECESSARY - DEFAULT BEHAVIOUR: FAIL");
		// grunt.task.run("validate");
	});

	/**
	 * post checkout hook
	 * - bower update
	 */
	grunt.registerTask("post-checkout", "post-checkout hook: run bower update", function () {
		grunt.task.run("bower-update");
		grunt.task.run("create-docs:js");
	});


};
