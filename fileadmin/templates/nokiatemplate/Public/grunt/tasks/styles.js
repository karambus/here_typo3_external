/**
 * Razorfish Gruntfile: configuration / tasks for all style files.
 *
 * - lint the project files
 * - running all tests
 * - concat and minifing the vendor and  project-files into one unified file
 * - creates the loader
 *
 * optional: "validate" to run only the valdiation part
 *
 * @description		General file task for handling the scrips.
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	/**
	 * Handles the css files:
	 * - lint the project files (validate checks modified files only!)
	 * - running all tests (css only)
	 * - concat and minifing the vendor and  project-files into one unified file
	 * - creates the loader
	 * @example {String}	optional: "validate" to run only the valdiation part
	 */
	grunt.registerTask("css", "process css files", function () {

		var _aArgs = this.args
		;

		// grunt css:validate
		if (_aArgs[0] === "validate") {
			grunt.task.run("newer:csslint:project");
			grunt.task.run("tests:css");
		}

		if (_aArgs.length === 0) {
			grunt.task.run("csslint:project");
			grunt.task.run("tests:css");
			grunt.task.run("cssmin:unified");
			grunt.task.run("create-loader:css");
		}
	});

};
