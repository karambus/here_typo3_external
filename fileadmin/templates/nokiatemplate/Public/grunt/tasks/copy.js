/**
 * Razorfish Gruntfile: configuration / tasks for contrib-copy.
 *
 * @description		Copy files and folders.
 * @see				https://github.com/gruntjs/grunt-contrib-copy
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			copy: {
				// copy all files to the release directory
				release: {
					expand: true,
					// cwd: "./"+"<%= pkg.release.directory %>",
					src: ["**/*/build/*", "*.html", "assets/**", "version.json", "!bower*/**/*/build/*", "!node*/**/*/build/*", "!.*"],
					dest: "<%= pkg.release.directory %>"
				},
				// replace the import/loader files with the unified / minfied files
				loaderJs: {
					src: "<%= pkg.release.directory %>" + "<%= pkg.files.js.unified.dest %>",
					dest: "<%= pkg.release.directory %>" + "<%= pkg.files.js.import %>"
				},
				loaderCss: {
					src: "<%= pkg.release.directory %>" + "<%= pkg.files.css.unified.dest %>",
					dest: "<%= pkg.release.directory %>" + "<%= pkg.files.css.import %>"
				}

			}
		}
	);
};
