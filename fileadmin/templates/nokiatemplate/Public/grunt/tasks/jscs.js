/**
 * Razorfish Gruntfile: configuration / tasks for the jscs task.
 *
 * @description		Task for checking JavaScript Code Style with jscs.
 * @see				https://github.com/jscs-dev/grunt-jscs
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
/*jshint camelcase: false */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			jscs: {
				src: ["./grunt/tasks/*.js", "<%= pkg.files.js.project.src %>"],
				options: {
					config: "./.jscsrc"
				}
			}
		}
	);
};
