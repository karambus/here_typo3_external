/**
 * Razorfish Gruntfile: configuration / tasks for handling bower
 *
 * bower-install - installs all bower dependencies
 * bower-update - update all bower dependencies
 *
 * @description		Create css and javascript loader.
 * @see				https://github.com/rgrove/lazyload
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	/**
	 * bower-install
	 * install all bower dependencies
	 */
	grunt.registerTask("bower-install", "Install bower dependencies", function () {
		var _bower = require("bower"),
			_done = this.async()
		;

		_bower
			.commands
			.install([], { /*save: true*/ }, { /*"directory" : "vendorXXX"*/ })
			.on("log", function (result) {
				grunt.log.writeln("bower " + result.id + " " + result.message);
			})
			.on("error", function () {
				_done(false);
			})
			.on("end", function () {
				_done();
			});
	});

	/**
	 * bower-update
	 * update all bower dependencies
	 */
	grunt.registerTask("bower-update", "Update bower dependencies", function () {
		var _bower = require("bower"),
			_done = this.async()
		;

		_bower
			.commands
			.update([], { /*save: true*/ }, { /*"directory" : "vendorXXX"*/ })
			.on("log", function (result) {
				grunt.log.writeln("bower " + result.id + " " + result.message);
			})
			.on("error", function () {
				_done(false);
			})
			.on("end", function () {
				_done();
				grunt.task.run("notify:bower_update");
			});

	});

};
