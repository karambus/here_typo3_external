/**
 * Razorfish Gruntfile: configuration / tasks for csslint
 *
 * @description		Lint CSS files.
 * @see				https://github.com/gruntjs/grunt-contrib-csslint
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			csslint: {
				options: {
					csslintrc: "./.csslintrc"
				},
				project: {
					src: "<%= pkg.files.css.project.src %>"
				},
				vendor: {
					src: "<%= pkg.files.css.vendor.src %>"
				},
				unified: {
					src: "<%= pkg.files.css.unified.src %>"
				}
			}
		}
	);
};
