/**
 * Razorfish Gruntfile: configuration / tasks for the watch task
 *
 *
 * @description		Run tasks whenever watched files change.
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			watch: {

				scripts: {
					files: "<%= pkg.files.js.project.src %>",
					tasks: ["js:validate"],
					options: {
						spawn: false
					}
				},

				styles: {
					files: "<%= pkg.files.css.project.src %>",
					tasks: ["css:validate"],
					options: {
						spawn: false
					}
				},
				myth: {
					files: "./styles/**/*.myth.css",
					tasks: ["myth:convert"],
					options: {
						spawn: false
					}
				},
				packagejson: {
					files: "./package.json",
					tasks: ["create-loader"],
					options: {
						spawn: false
					}
				}

			}
		}
	);
};
