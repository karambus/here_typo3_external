/**
 * Razorfish Gruntfile: configuration / tasks for creating javascript docs.
 *
 * @description		Creates the docs based on JSDoc-tags in your files
 * @see				https://github.com/darsain/grunt-bumpup
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			 docco: {
				src: "<%= pkg.files.js.project.src %>",
				options: {
					output:"./docs/scripts/docco/"
				}
			},

			doxx: {
				all: {
					src: "./scripts/",
					target: "./docs/scripts/doxx",
					options: {
						// Task-specific options go here.
						readme: "./docs/styleguide.md",
						ignore: "build,_templates",
						template: "./grunt/templates/doxx.jade",
						skipSingleStar: true,
						raw: true
					}
				}
			}
		}
	);

	grunt.registerTask("create-docs:scripts", [ "clean:scriptDocs","doxx", "docco" ]);
};
