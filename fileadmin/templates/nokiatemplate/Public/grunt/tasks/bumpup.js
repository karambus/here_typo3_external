/**
 * Razorfish Gruntfile: configuration / tasks for bumpup.
 *
 * @description		Updates the version, date, and other properties in your JSON files.
 * @see				https://github.com/darsain/grunt-bumpup
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			bumpup: {
				options: {
					normalize: true,
					updateProps: {
						pkg: "package.json"
					}
				},
				files: ["package.json", "version.json"]
			}
		}
	);
};
