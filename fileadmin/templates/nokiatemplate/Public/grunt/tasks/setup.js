/**
 * Razorfish Gruntfile: configuration / tasks for creating a working project after cloning.
 *
 * validate - validates the current version
 * build - create a build by running the js / css tasks. No copying etc
 * release - creates a complete release
 *
 * @description		General file task for creating a working project after cloning.
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {


	/**
	 * Set up all project dependencies and build all necessary files once
	 */
	grunt.registerTask("setup", "Set up all project dependencies and build all necessary files once", function () {
		grunt.task.run("githooks");
		grunt.task.run("bower-install");
		grunt.task.run("create-loader");
		grunt.task.run("build");

		// grunt.task.run("watch");

	});
};
