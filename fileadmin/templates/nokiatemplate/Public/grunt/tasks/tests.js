/**
 * Razorfish Gruntfile: configuration / tasks for automated testing.
 *
 * tests - Execute all tests
 * tests-smoke - Execute smoketests
 * tests-acceptance - Execute acceptancetests
 * tests-specs - Execute specs using mocha & chai
 *
 * @description		Run automated test
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	/**
	 * Execute all tests
	 */
	grunt.registerTask("tests", "Execute all automated tests", function () {

		var _aArgs = this.args
		;

		// grunt.log.fail("./grunt/tasks/tests.js: SETUP NECESSARY - DEFAULT BEHAVIOUR: FAIL");

		if (_aArgs[0] === "css") {
			grunt.task.run("tests-specs");
		}

		if (_aArgs[0] === "js") {
			grunt.task.run("tests-specs");
		}
		// grunt.task.run("tests-smoke");
		// grunt.task.run("tests-acceptance");
		// grunt.task.run("tests-specs");
	});

	/**
	 * Execute smoketests
	 */
	grunt.registerTask("tests-smoke", "Execute smoketests", function () {

	});

	/**
	 * Execute acceptancetests
	 */
	grunt.registerTask("tests-acceptance", "Execute acceptancetests", function () {

	});

	/**
	 * Execute acceptancetests
	 */
	grunt.registerTask("tests-specs", "Execute specs using mocha & chai", function () {
		grunt.task.run("mocha");
	});

};
