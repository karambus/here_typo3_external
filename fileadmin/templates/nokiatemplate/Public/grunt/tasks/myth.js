/**
 * Razorfish Gruntfile: configuration / tasks for myth.io
 *
 * We'll parse all *.myth.css files and write them to *.css.
 *
 * Please checkthe documentation and set the browser list for
 * autoprefixer according to your requirements:
 * @see  https://github.com/postcss/autoprefixer#browsers
 *
 * @description		Postprocessor that polyfills CSS
 * @see				https://www.npmjs.org/package/grunt-myth
 * @see				http://www.myth.io/
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge({

		myth: {
			options: {
				sourcemap: false,
				compress: false,
				// default autoprefixer settings
				// browsers: ["> 1%", "last 2 versions", "Firefox ESR", "Opera 12.1"],
				browsers: ["Explorer 11", "last 2 Chrome versions", "last 2 Firefox versions", "last 2 Safari versions", "iOS 6.1.3", "iOS 7.1.1", "iOS 8.1", "Android 4.4", "Android 5.0", "ExplorerMobile >= 10"],
				features: {
					import: true,
					variables: true,
					customMedia: true,
					hexAlpha: true,
					color: true,
					calc: true,
					fontVariant: true,
					rebeccapurple: true,
					prefixes: true
				}
			},
			convert: {
				files: [
					{
						expand: true,
						cwd: './styles/',
						src: ['./**/*.myth'],
						dest: './styles/',
						ext: '.css',
						extDot: 'first'
					}
				]
			}
		}

	});
};
