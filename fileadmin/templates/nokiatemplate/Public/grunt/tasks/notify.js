/**
 * Razorfish Gruntfile: configuration / tasks for the notify plugin.
 *
 * @description   Automatic Notifications when Grunt tasks fail.
 * @see       https://github.com/dylang/grunt-notify
 *
 * @author      Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version     1.0.0
 *
 */
// jshint ignore:start
 module.exports = function (grunt) {
	grunt.config.merge(
		{
			notify: {

// jscs:disable
				task_name: {
// jscs:enable

					options: {
						// Task-specific options go here.
					}
				},

// jscs:disable
				bower_update: {
// jscs:enable

					options: {
						message: "Bower packages updated" // required
					}
				},
				setup: {
					options: {
						message: "Setup Complete. Your're ready to go." // required
					}
				}
			}
		}
	);
};
// jshint ignore:end
