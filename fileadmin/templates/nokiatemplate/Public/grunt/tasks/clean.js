/**
 * Razorfish Gruntfile: configuration / tasks for contrib-clean.
 *
 * @description		Clear files and folders.
 * @see				https://github.com/gruntjs/grunt-contrib-clean
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			clean: {
				// delete all files created during a previous run
				releasePre: {
					src: [
						"<%= pkg.release.directory %>",
						"<%= pkg.files.js.unified.dest %>",
						"<%= pkg.files.css.unified.dest %>"
					]
				},
				// delete all unnecesary / temporary files for this release
				releasePost: {
					src: [
						"<%= pkg.release.directory %>" + "<%= pkg.files.css.unified.dest %>",
						"<%= pkg.release.directory %>" + "<%= pkg.files.js.unified.dest %>",
						"<%= pkg.files.css.unified.dest %>",
						"<%= pkg.files.js.unified.dest %>"
					]
				},
				// delete all script docs
				scriptDocs: {
					src: [
						"./docs/scripts/"
					]
				}
			}
		}
	);
};
