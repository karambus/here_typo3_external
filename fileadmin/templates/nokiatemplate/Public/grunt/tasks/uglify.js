/**
 * Razorfish Gruntfile: configuration / tasks for contrib-uglify.
 *
 * @description		Minify files with UglifyJS.
 * @see				https://github.com/gruntjs/grunt-contrib-uglify
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
/*jshint camelcase: false */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			uglify: {
				options: {
					// jscs:disable
					compress: {
						sequences     : true,  // join consecutive statemets with the “comma operator”
						properties    : true,  // optimize property access: a["foo"] → a.foo
						dead_code     : true,  // discard unreachable code
						drop_debugger : true,  // discard “debugger” statements
						unsafe        : false, // some unsafe optimizations (see below)
						conditionals  : true,  // optimize if-s and conditional expressions
						comparisons   : true,  // optimize comparisons
						evaluate      : true,  // evaluate constant expressions
						booleans      : true,  // optimize boolean expressions
						loops         : true,  // optimize loops
						unused        : true,  // drop unused variables/functions
						hoist_funs    : true,  // hoist function declarations
						hoist_vars    : false, // hoist variable declarations
						if_return     : true,  // optimize if-s followed by return/continue
						join_vars     : true,  // join var declarations
						cascade       : true,  // try to cascade `right` into `left` in sequences
						side_effects  : true,  // drop side-effect-free statements
						warnings      : true,  // warn about potentially dangerous optimizations/code
						global_defs   : {
							DEBUG: false
						}     // global definitions
					},
					mangle  : true,
					beautify: false,
					sourceMap: false,
					sourceMapIncludeSources: false,
					preserveComments: "some",
					drop_debugger: true,
					drop_console: true,
					unused: true,
					report: "gzip",
					banner: "<%= override.banner %>" + "/**! vendor: <%= pkg.files.js.vendor.src %> ; project: <%= pkg.files.js.project.src %> */" + grunt.util.linefeed
					// jscs:enable
				},

				unified: {
					src: ["<%= pkg.files.js.vendor.src %>", "<%= pkg.files.js.project.src %>"],
					dest: "<%= pkg.files.js.unified.dest %>"
				}

			}
		}
	);
};
