/**
 * Razorfish Gruntfile: configuration / tasks for cssmin
 *
 * @description		Compress CSS files.
 * @see				https://github.com/gruntjs/grunt-contrib-cssmin
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			cssmin: {
				options: {
					banner: "<%= override.banner %>" + "/**! vendor: <%= pkg.files.css.vendor.src %> ; project: <%= pkg.files.css.project.src %> */" + grunt.util.linefeed
				},
				unified: {
					src: ["<%= pkg.files.css.vendor.src %>", "<%= pkg.files.css.project.src %>"],
					dest: "<%= pkg.files.css.unified.dest %>"
				}
			}
		}
	);
};
