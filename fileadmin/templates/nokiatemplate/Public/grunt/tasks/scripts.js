/**
 * Razorfish Gruntfile: configuration / tasks for all script files.
 *
 * - lint the project files
 * - running all tests
 * - concat and minifing the vendor and  project-files into one unified file
 * - creates the loader
 *
 * optional: "validate" to run only the valdiation part
 *
 * @description		General file task for handling the scrips.
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	/**
	 * Handles the javascript files:
	 * - lint the project files (validate checks modified files only!)
	 * - running all tests
	 * - concat and minifing the vendor and  project-files into one unified file
	 * - creates the loader
	 * @example {String}	string optional: "validate" to run only the valdiation part
	 */
	grunt.registerTask("js", "process js files", function () {

		var _aArgs = this.args
		;

		// grunt js:project or grunt js
		if (_aArgs[0] === "validate") {
			grunt.task.run("newer:jshint:project");
			grunt.task.run("tests:scripts");
		}

		if (_aArgs.length === 0) {
			grunt.task.run("jshint:project");
			grunt.task.run("tests");
			grunt.task.run("create-docs:scripts");
			grunt.task.run("uglify:unified");
			grunt.task.run("create-loader:js");
		}

	});
};
