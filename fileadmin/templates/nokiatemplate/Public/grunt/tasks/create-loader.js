/**
 * Razorfish Gruntfile: configuration / tasks for creating css and javascript loader.
 *
 * We"re using two files: build/styles.css and build/scripts.js (defined as ".import" in the package.json)
 * - the css file uses @import to load the original css files
 * - the js file uses LazyLoad.js to load the original js files (the loader template is defined as ".loader" in the package.json)
 *
 * During the build / release process both files will be replaced with their minified counterparts (see: copy and clean tasks).
 *
 * create-loader - css and js
 * create-loader:css - css only
 * create-loader:js - js only
 *
 * @description		Create css and javascript loader.
 * @see				https://github.com/rgrove/lazyload
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	grunt.registerTask("create-loader", "create css and javascript loader", function () {

		var
			_aArgs = this.args,
			_sFileNameImport,
			_aFiles = [],
			_contentLoader,
			_contentOutput
			;

		// create javascript file loader from package.json
		if (_aArgs[0] === "js" || _aArgs.length === 0) {

			_sFileNameImport = grunt.config.get("pkg").files.js.import;

			// cleanup
			if(grunt.file.exists(_sFileNameImport)) {
				grunt.file.delete(_sFileNameImport);
			}
			_contentLoader = grunt.file.read(grunt.config.get("pkg").files.js.loader);

			// add the vendor files
			grunt.config.get("pkg").files.js.vendor.src.forEach(function (sFile_) {
				_aFiles.push("'" + sFile_ + "?v=" + grunt.config.get("override").timestamp + "'");
			});

			// add the project files
			grunt.config.get("pkg").files.js.project.src.forEach(function (sFile_) {
				_aFiles.push("'" + sFile_ + "?v=" + grunt.config.get("override").timestamp + "'");
			});

			// create window.scripts array
			_contentOutput = "window.scripts = [" + grunt.util.linefeed + "" + _aFiles.join("," + grunt.util.linefeed) + "" + grunt.util.linefeed + "];";
			_contentOutput = _contentOutput + grunt.util.linefeed + _contentLoader;

			// write file
			grunt.file.write( _sFileNameImport , _contentOutput );
		}


		// create css file loader from package.json
		if (_aArgs[0] === "css" || _aArgs.length === 0) {

			_contentOutput = grunt.config.get("override").banner + grunt.util.linefeed;

			_sFileNameImport = grunt.config.get("pkg").files.css.import;

			// cleanup
			if(grunt.file.exists(_sFileNameImport)) {
				grunt.file.delete(_sFileNameImport);
			}

			// add the vendor files
			grunt.config.get("pkg").files.css.vendor.src.forEach(function (sFile_) {
				_contentOutput += "@import url('../../" + sFile_ + "?v=" + grunt.config.get("override").timestamp + "');" + grunt.util.linefeed;
			});

			// add the project files
			grunt.config.get("pkg").files.css.project.src.forEach(function (sFile_) {
				_contentOutput += "@import url('../../" + sFile_ + "?v=" + grunt.config.get("override").timestamp + "');" + grunt.util.linefeed;
			});

			// write file
			grunt.file.write( _sFileNameImport , _contentOutput );
		}

	});

};
