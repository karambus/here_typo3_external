/**
 * Razorfish Gruntfile: configuration / tasks for creating a release and related utility functions.
 *
 * validate - validates the current version
 * build - create a build by running the js / css tasks. No copying etc
 * release - creates a complete release
 *
 * @description		General file task for creating a release and related utility functions.
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {

	/**
	 * File validation
	 * - lints the project css
	 * - lints the project js
	 * - runs all tests
	 */
	grunt.registerTask("validate", "Validates the current version", function () {
		grunt.task.run("css:validate");
		grunt.task.run("js:validate");
		grunt.task.run("tests");
	});


	/**
	 * Creates a build by running the js / css tasks. No copying etc
	 */
	grunt.registerTask("build", "Create a build by running the js / css tasks. No copying etc", function () {
		grunt.task.run("css");
		grunt.task.run("js");
	});


	/**
	 * Creates a complete release - an alias task for release with buildmeta suffix support
	 * @param {String} sType_
	 * @param {String} sBuild_
	 * @example
	 * grunt release            // Default patch release
	 * grunt release:major      // Major release
	 * grunt release:minor      // Minor release
	 * grunt release:minor:1458 // Minor release with buildtype suffix
	 * grunt release:build:1459 // Only build suffix will be modified
	 *
	 */
	grunt.registerTask("release", "Creates a complete release", function (sType_, sBuild_) {

		var bumpParts = ["bumpup"];

		if (sType_) { bumpParts.push(sType_); }
		if (sBuild_) { bumpParts.push(sBuild_); }

		grunt.task.run(bumpParts.join(":"));

		grunt.task.run("clean:releasePre");
		grunt.task.run("build");
		grunt.task.run("copy:release");
		grunt.task.run("copy:loaderCss");
		grunt.task.run("copy:loaderJs");
		grunt.task.run("clean:releasePost");
	});
};
