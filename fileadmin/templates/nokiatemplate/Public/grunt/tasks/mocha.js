/**
 * Razorfish Gruntfile: configuration / tasks for the  mocha tasks
 *
 *
 * @description		Grunt task for running mocha specs in a headless browser (PhantomJS)
 * @see				https://github.com/kmiyashiro/grunt-mocha
 *
 * @author			Martin Krause <martin.krause@razorfish.de>
 * @contributors
 *
 * @version			1.0.0
 *
 */
module.exports = function (grunt) {
	grunt.config.merge(
		{
			mocha: {
				all: {
					src: ["tests/specs/**/*.html"],
					options: {
						timeout: 10000,
						run: true,
						logErrors: true,
						log: true
					}
				}
			}
		}
	);
};
