/**
 * Default Razorfish Gruntfile.
 *
 * Please adjust, especially the copy, clean and test sections.
 *
 * Please use
 *
 *	- phantomCSS for CSS regression testing (https://github.com/chrisgladd/grunt-phantomcss)
 * and
 *	- unCSS for removing unused CSS from your projects (https://github.com/addyosmani/grunt-uncss)
 *
 * if possible.
 *
 * Think about using imageoptim or something similar for reducing the filesizes and spritesmith
 * for automatically generating css-sprites.
 *
 * Consider using
 *
 *  - grunt-concurrent: Run grunt tasks concurrently
 *  https://github.com/sindresorhus/grunt-concurrent
 *
 *
 * If you have any questions please contact martin.krause@razorfish.de
 *
 * @author Martin Krause <martin.krause@razorfish.de>
 * @version  1.0.0
 */
module.exports = function(grunt) {

	// basic configuration, each tasks extends this by itself
	grunt.initConfig({

		override : {
			timestamp : grunt.template.today("yyyymmddhhMMss"),
			now : grunt.template.today("yyyy-mm-dd|HH:MM:ss"),
			banner: "/*! <%= pkg.name %>, v<%= pkg.version %>, <%= override.now %> - <%= pkg.copyright %> */"+grunt.util.linefeed
		},

		pkg: grunt.file.readJSON("package.json")

	});


	// load all grunt tasks from the package.json
	require("load-grunt-tasks")(grunt);

	// load all custom tasks
	grunt.loadTasks("./grunt/tasks");

	// measures the time each task takes
	require("time-grunt")(grunt);

	// set default task
	grunt.registerTask("default", ["build"]);
};