
lib.field_submenu = HMENU
lib.field_submenu {
	entryLevel = 1
	special = directory
	special.value = 3
	1 = TMENU
	1 {
		wrap = <ul class="nav navbar-nav">|</ul>
		noBlur = 1
		NO = 1
		NO {
			doNotLinkIt = 0
			wrapItemAndSub = <li>|</li>
			stdWrap = htmlSpecialChars = 1
			ATagTitle.field = title
		}
		ACT <.NO
		ACT {
			wrapItemsAndSub = <li class="active">|</li>
		}

	} 
}

