


lib.field_sitemenu = HMENU
lib.field_sitemenu {


  entryLevel = 1
  special = directory
  special.value = 2

  1 = TMENU
  1 {
    wrap = <ul>|</ul>
    noBlur = 1
    NO = 1
    NO {

      doNotLinkIt = 1
      stdWrap = htmlSpecialChars = 1
      ATagTitle.field = title
      wrapItemAndSub = <li><a href="anchor_|"></a></li>

      stdWrap >
      stdWrap {
        cObject = TEXT
        cObject {
          field = uid
         
        }
      }
     
    }
    ACT <.NO
    ACT {
      wrapItemsAndSub = <li class="selected">|</li>
    }

  } 
}

