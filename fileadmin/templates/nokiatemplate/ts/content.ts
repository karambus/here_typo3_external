

lib.sectionContent = HMENU
lib.sectionContent {
  entryLevel = 1
  1 = TMENU
  1 {
    NO = 1
    NO {
      doNotLinkIt = 1
      stdWrap >
      stdWrap {
        cObject = COA
        cObject {

          10 < temp.titleSectionId
          20 = CONTENT
          20 {
            table = tt_content
            select {
              pidInList.field = uid
            }
           
            renderObj < tt_content
          }
        }
      }
    }
  }
}
