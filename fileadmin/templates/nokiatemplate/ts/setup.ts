page = PAGE

page {

	config {
    	metaCharset = utf-8
    	additioinalHeaders = Content-Type:text/html;charset=utf-8
  	}

	includeCSS.styleCss = fileadmin/templates/nokiatemplate/Public/styles/build/styles.css
  	includeJS.scripts = fileadmin/templates/nokiatemplate/Public/scripts/build/scripts.js

 	10 = FLUIDTEMPLATE
	10{
		file = fileadmin/templates/nokiatemplate/index.html
		layoutRootPath = fileadmin/templates/nokiatemplate/layouts/
		partialRootPath = fileadmin/templates/nokiatemplate/partials/
		variables {
			content < styles.content.get
		}
	}
}