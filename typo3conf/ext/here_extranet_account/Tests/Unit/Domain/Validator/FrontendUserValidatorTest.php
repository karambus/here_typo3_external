<?php
namespace Here\HereExtranetAccount\Domain\Validator;

use Here\HereExtranetAccount\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Tests\Unit\Validation\Validator\AbstractValidatorTestcase;

/**
 * Test the Domain Validator for FrontendUsers
 * 
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   02.05.14 14:46
 */
class FrontendUserValidatorTest extends AbstractValidatorTestcase {

    protected $validatorClassName = 'Here\\HereExtranetAccount\\Domain\\Validator\\FrontendUserValidator';

    /**
     * @test
     */
    public function acceptOnlyObject() {
        $testObject = array();

        $validateResults = $this->validator->validate($testObject);
        $this->assertTrue($validateResults->hasErrors());

        /* @var $errorArray \TYPO3\CMS\Extbase\Validation\Error[] */
        $errorArray = $validateResults->getErrors();
        $this->assertCount(1, $errorArray);
        $this->assertEquals(1399034711, $errorArray[0]->getCode());
    }

    /**
     * @test
     */
    public function providedPasswordsAreNotEqual() {
        $user = new FrontendUser();
        $user->setPassword('1234');
        $user->setPasswordRepeat('12345');

        $validationResult = $this->validator->validate($user);

        $this->assertTrue($validationResult->hasErrors());
    }

    /**
     * @test
     */
    public function providedPasswordsAreEqual()
    {
        $user = new FrontendUser();
        $user->setPassword('12345');
        $user->setPasswordRepeat('12345');

        $validationResult = $this->validator->validate($user);
        $this->assertFalse($validationResult->hasErrors());
    }
} 