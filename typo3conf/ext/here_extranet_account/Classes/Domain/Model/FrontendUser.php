<?php
/**
 * Override the FE-User for custom validation
 *
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   30.04.14 17:56
 */
namespace Here\HereExtranetAccount\Domain\Model;

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Saltedpasswords\Salt\SaltFactory;
use TYPO3\CMS\Saltedpasswords\Utility\SaltedPasswordsUtility;

class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser {

    /**
     * The username (basically the e-mail address)
     *
     * @validate EmailAddress
     * @var string
     */
    protected $username;

    /**
     * Placeholder for Repeating password
     *
     * @var string
     */
    protected $passwordRepeat;

    /**
     * Birthday of the user
     *
     * @var \DateTime
     */
    protected $birthday;

    /**
     * Set a new Password
     * @param string $passwordRepeat
     */
    public function setPasswordRepeat($passwordRepeat)
    {
        if (ExtensionManagementUtility::isLoaded('saltedpasswords')) {
            if (SaltedPasswordsUtility::isUsageEnabled('FE')) {
                $objSalt = SaltFactory::getSaltingInstance(NULL);
                if (is_object($objSalt)) {
                    $passwordRepeat = $objSalt->getHashedPassword($passwordRepeat);
                }
            }
        }

        $this->passwordRepeat = $passwordRepeat;
    }

    /**
     * Set a new password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        if (ExtensionManagementUtility::isLoaded('saltedpasswords')) {
            if (SaltedPasswordsUtility::isUsageEnabled('FE')) {
                $objSalt = SaltFactory::getSaltingInstance(NULL);
                if (is_object($objSalt)) {
                    $password = $objSalt->getHashedPassword($password);
                }
            }
        }

        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPasswordRepeat()
    {
        return $this->passwordRepeat;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }



}
