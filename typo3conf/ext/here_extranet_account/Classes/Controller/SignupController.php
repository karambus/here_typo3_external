<?php
namespace Here\HereExtranetAccount\Controller;

use Here\HereExtranetAccount\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Controller for the Signup-Process
 * 
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   30.04.14 17:31
 */
class SignupController extends ActionController {

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $userRepository;

    /**
     * Mask for creating a new account on HERE.com
     *
     * @param \Here\HereAccount\Domain\Model\FrontendUser $user
     * @return void
     */
    public function indexAction(FrontendUser $user = NULL)
    {
        $days   = range(1, 31);
        $months = range(1,12);
        $years  = range(1930, date('Y'));

        if(!$user)
            $user = new FrontendUser();

        $this->view->assignMultiple(array(
            'user'   => $user,
            'days'   => array_combine($days, $days),
            'months' => array_combine($months, $months),
            'years'  => array_combine($years, $years)
        ));
    }

    /**
     * Save the FrontendUser in Database
     * @param \Here\HereAccount\Domain\Model\FrontendUser $user
     */
    public function createAction(FrontendUser $user)
    {
        $this->flashMessageContainer->add('User successfully created', 'OK');

        $this->userRepository->add( $user );
        $this->view->assign('user', $user);
    }

    protected function getErrorFlashMessage() {
        return 'There was an error in signin up';
    }
} 