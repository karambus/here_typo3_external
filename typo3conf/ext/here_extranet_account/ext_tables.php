<?php
/**
 * Description
 * 
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   30.04.14 17:39
 */

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin('Here.' . $_EXTKEY, 'Signup', 'HERE Account Signup');