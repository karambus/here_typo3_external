<?php
/**
 * Description
 * 
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   30.04.14 17:45
 */
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

ExtensionUtility::configurePlugin(
    'Here.' . $_EXTKEY,
    'Signup',
    array('Signup' => 'index,create'),
    array('Signup' => 'index,create'),
    ExtensionUtility::PLUGIN_TYPE_PLUGIN
);
