<?php
namespace Here\HereExtranetTemplates\Controller;

use FluidTYPO3\Fluidpages\Controller\AbstractPageController;

/**
 * An own PageController for Demo Purposes
 * 
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   25.04.14 11:08
 */
class PageController extends AbstractPageController {

    /**
     * Action for the Template "Default.html"
     * @return void
     */
    public function defaultAction()
    {
        $this->view->assign('custom_variable', 'foo!');
    }

} 