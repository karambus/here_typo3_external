<?php
namespace Here\HereProvider;

use TYPO3\CMS\Extbase\Tests\Unit\BaseTestCase;

/**
 * This is a simple and yet empty TestCase
 * 
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   28.04.14 17:36
 */
class EmptyTest extends BaseTestCase {

    /**
     * @test
     */
    public function isOneEqualOne()
    {
        $this->assertEquals('1', 1);
    }
} 