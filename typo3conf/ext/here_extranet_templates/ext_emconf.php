<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "here_provider".
 *
 * Auto generated 24-04-2014 14:58
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Provider extension HERE Extranet',
    'description' => 'Provider extension for HERE Extranet',
    'category' => 'misc',
    'shy' => 0,
    'version' => '0.0.1',
    'dependencies' => 'cms,extbase,fluid,flux,fluidcontent',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'experimental',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearcacheonload' => 1,
    'lockType' => '',
    'author' => 'Michael Feinbier',
    'author_email' => 'michael.feinbier@interone.de',
    'author_company' => '',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => array(
        'depends' => array(
            'typo3' => '4.5-6.1.99',
            'cms' => '',
            'extbase' => '',
            'fluid' => '',
            'flux' => '',
            'fluidpages' => '',
            'fluidcontent' => '',
            'vhs' => '',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    '_md5_values_when_last_written' => 'a:0:{}',
    'suggests' => array(),
);
