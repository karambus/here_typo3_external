<?php
use FluidTYPO3\Flux\Core;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Provider extension for HERE Extranet');
Core::registerProviderExtensionKey('Here.HereExtranetTemplates', 'Page');
Core::registerProviderExtensionKey('Here.HereExtranetTemplates', 'Content');
