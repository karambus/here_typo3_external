<?php
/**
 * ext_localconf.php for here provider extension
 *
 * @author Michael Feinbier <michael.feinbier@interone.de>
 * @date   24.04.14 15:43
 */
use FluidTYPO3\Flux\Core;

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

Core::registerProviderExtensionKey('Here.HereExtranetTemplates', 'Page');
Core::registerProviderExtensionKey('Here.HereExtranetTemplates', 'Content');