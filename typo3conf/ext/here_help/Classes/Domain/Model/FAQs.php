<?php
namespace Meelogic\HereHelp\Domain\Model;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FAQs extends AbstractHelpModel {

	/**
	 * headline
	 *
	 * @var \string
	 */
	protected $headline;

	/**
	 * title
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * transitdb
	 *
	 * @var boolean
	 */
	protected $transitdb = false;

	/**
	 * questions
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\Questions>
	 */
	protected $questions;

	/**
	 * __construct
	 *
	 * @return FAQs
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->questions = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the headline
	 *
	 * @return \string $headline
	 */
	public function getHeadline() {
		return $this->headline;
	}

	/**
	 * Sets the headline
	 *
	 * @param \string $headline
	 *
	 * @return void
	 */
	public function setHeadline($headline) {
		$this->headline = $headline;
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 *
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the transitdb
	 *
	 * @return boolean $transitdb
	 */
	public function getTransitdb() {
		return $this->transitdb;
	}

	/**
	 * Sets the transitdb
	 *
	 * @param boolean $transitdb
	 *
	 * @return void
	 */
	public function setTransitdb($transitdb) {
		$this->transitdb = $transitdb;
	}

	/**
	 * Returns the boolean state of transitdb
	 *
	 * @return boolean
	 */
	public function isTransitdb() {
		return $this->getTransitdb();
	}

	/**
	 * Adds a Questions
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\Questions $question
	 *
	 * @return void
	 */
	public function addQuestion(\Meelogic\HereHelp\Domain\Model\Questions $question) {
		$this->questions->attach($question);
	}

	/**
	 * Removes a Questions
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\Questions $questionToRemove The Questions to be removed
	 *
	 * @return void
	 */
	public function removeQuestion(\Meelogic\HereHelp\Domain\Model\Questions $questionToRemove) {
		$this->questions->detach($questionToRemove);
	}

	/**
	 * Returns the questions
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\Questions> $questions
	 */
	public function getQuestions() {
		return $this->questions;
	}

	/**
	 * Sets the questions
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\Questions> $questions
	 *
	 * @return void
	 */
	public function setQuestions(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $questions) {
		$this->questions = $questions;
	}

}

?>