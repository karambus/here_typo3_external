<?php
namespace Meelogic\HereHelp\Domain\Model;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Subsections extends AbstractHelpModel {

	/**
	 * sections
	 *
	 * @var \int
	 */
	protected $sections;

	/**
	 * title
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * icon
	 *
	 * @var \string
	 */
	protected $icon;

	/**
	 * apps
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\Apps>
	 */
	protected $apps;

	/**
	 * faqs
	 *
	 * @lazy
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\FAQs>
	 */
	protected $faqs;

	/**
	 * paths
	 *
	 * @var \string
	 */
	protected $paths;

	/**
	 * __construct
	 *
	 * @return Subsections
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->apps = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();

		$this->faqs = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the section id
	 *
	 * @return int
	 */
	public function getSections() {
		return $this->sections;
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 *
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the section uid
	 *
	 * @return int
	 */
	public function getSectionsId() {
		return $this->sections;
	}

	/**
	 * Returns the icon
	 *
	 * @return \string $icon
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Sets the icon
	 *
	 * @param \string $icon
	 *
	 * @return void
	 */
	public function setIcon($icon) {
		$this->icon = $icon;
	}

	/**
	 * Adds a Apps
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\Apps $app
	 *
	 * @return void
	 */
	public function addApp(\Meelogic\HereHelp\Domain\Model\Apps $app) {
		$this->apps->attach($app);
	}

	/**
	 * Removes a Apps
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\Apps $appToRemove The Apps to be removed
	 *
	 * @return void
	 */
	public function removeApp(\Meelogic\HereHelp\Domain\Model\Apps $appToRemove) {
		$this->apps->detach($appToRemove);
	}

	/**
	 * Returns the apps
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\Apps> $apps
	 */
	public function getApps() {
		return $this->apps;
	}

	/**
	 * Sets the apps
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\Apps> $apps
	 *
	 * @return void
	 */
	public function setApps(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $apps) {
		$this->apps = $apps;
	}

	/**
	 * Adds a FAQs
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\FAQs $faq
	 *
	 * @return void
	 */
	public function addFaq(\Meelogic\HereHelp\Domain\Model\FAQs $faq) {
		$this->faqs->attach($faq);
	}

	/**
	 * Removes a FAQs
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\FAQs $faqToRemove The FAQs to be removed
	 *
	 * @return void
	 */
	public function removeFaq(\Meelogic\HereHelp\Domain\Model\FAQs $faqToRemove) {
		$this->faqs->detach($faqToRemove);
	}

	/**
	 * Returns the faqs
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\FAQs> $faqs
	 */
	public function getFaqs() {
		return $this->faqs;
	}

	/**
	 * Sets the faqs
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\FAQs> $faqs
	 *
	 * @return void
	 */
	public function setFaqs(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $faqs) {
		$this->faqs = $faqs;
	}

	/**
	 * @param \string $paths
	 */
	public function setPaths($paths) {
		$this->paths = $paths;
	}

	/**
	 * @return \string
	 */
	public function getPaths() {
		return $this->paths;
	}

}

?>