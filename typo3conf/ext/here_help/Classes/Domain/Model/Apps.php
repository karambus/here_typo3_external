<?php
namespace Meelogic\HereHelp\Domain\Model;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class Apps extends AbstractHelpModel {

	/**
	 * title
	 *
	 * @var \string
	 */
	protected $title;

	/**
	 * icon
	 *
	 * @var \string
	 */
	protected $icon;

	/**
	 * subsections
	 *
	 * @var \string
	 */
	protected $subsections;

	/**
	 * paths
	 *
	 * @var \integer
	 */
	protected $paths;

	/**
	 * faqs
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\FAQs>
	 */
	protected $faqs;

	/**
	 * __construct
	 *
	 * @return Apps
	 */
	public function __construct() {
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties.
	 *
	 * @return void
	 */
	protected function initStorageObjects() {
		/**
		 * Do not modify this method!
		 * It will be rewritten on each save in the extension builder
		 * You may modify the constructor of this class instead
		 */
		$this->faqs = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Returns the title
	 *
	 * @return \string $title
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Sets the title
	 *
	 * @param \string $title
	 *
	 * @return void
	 */
	public function setTitle($title) {
		$this->title = $title;
	}

	/**
	 * Returns the icon
	 *
	 * @return \string $icon
	 */
	public function getIcon() {
		return $this->icon;
	}

	/**
	 * Sets the icon
	 *
	 * @param \string $icon
	 *
	 * @return void
	 */
	public function setIcon($icon) {
		$this->icon = $icon;
	}

	/**
	 * Returns the subsections
	 *
	 * @return string
	 */
	public function getSubsections() {
		return $this->subsections;
	}

	/**
	 * Sets the subsections
	 *
	 * @param $subsections
	 *
	 * @return void
	 */
	public function setSubsections($subsections) {
		$this->subsections = $subsections;
	}

	/**
	 * Returns the paths
	 *
	 * @return int
	 */
	public function getPaths() {
		return $this->paths;
	}

	/**
	 * Sets the paths
	 *
	 * @param $paths
	 * @return void
	 */
	public function setPaths($paths) {
		$this->paths = $paths;
	}

	/**
	 * Adds a FAQs
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\FAQs $faq
	 *
	 * @return void
	 */
	public function addFaq(\Meelogic\HereHelp\Domain\Model\FAQs $faq) {
		$this->faqs->attach($faq);
	}

	/**
	 * Removes a FAQs
	 *
	 * @param \Meelogic\HereHelp\Domain\Model\FAQs $faqToRemove The FAQs to be removed
	 *
	 * @return void
	 */
	public function removeFaq(\Meelogic\HereHelp\Domain\Model\FAQs $faqToRemove) {
		$this->faqs->detach($faqToRemove);
	}

	/**
	 * Returns the faqs
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\FAQs> $faqs
	 */
	public function getFaqs() {
		return $this->faqs;
	}

	/**
	 * Sets the faqs
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Meelogic\HereHelp\Domain\Model\FAQs> $faqs
	 *
	 * @return void
	 */
	public function setFaqs(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $faqs) {
		$this->faqs = $faqs;
	}

}

?>