<?php
namespace Meelogic\HereHelp\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SearchRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Find by searchword
	 *
	 * @param $searchword
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findBySearchword($searchword) {

		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('
			SELECT q.uid as questionuid,q.question,q.faqs as questionfaq,
			          a.uid as answeruid,a.answer as answer,
			          f.uid as faquid,f.headline as faqheadline,f.apps as faqapps, f.paths as faqpath, f.sections as faqsec, f.l10n_parent as faqparent,
			          c.paths as path

			          FROM tx_herehelp_domain_model_questions AS q
			          LEFT JOIN tx_herehelp_domain_model_answers AS a ON (a.uid=q.uid)
			          LEFT JOIN tx_herehelp_domain_model_faqs AS f ON (f.uid=q.faqs)
			          LEFT JOIN tx_herehelp_domain_model_customurls AS c ON (c.uid=f.paths)

			          WHERE (q.question LIKE "%' . $searchword . '%" OR a.answer LIKE "%' . $searchword . '%")
			          AND f.sys_language_uid=' . $GLOBALS['TSFE']->sys_language_uid . ' AND a.sys_language_uid=' . $GLOBALS['TSFE']->sys_language_uid
						. ' AND q.sys_language_uid=' . $GLOBALS['TSFE']->sys_language_uid . ' AND q.deleted=0 AND q.hidden=0 AND f.deleted=0 AND f.hidden=0');

		return $query->execute();
	}

}

?>