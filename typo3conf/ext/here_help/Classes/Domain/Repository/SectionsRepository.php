<?php
namespace Meelogic\HereHelp\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SectionsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Find by UID
	 *
	 * @param int $uid
	 *
	 * @return array|object|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByUidAndLanguage($uid, $languageUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectSysLanguage(false);
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setSysLanguageUid($languageUid);
		$query->getQuerySettings()->setReturnRawQueryResult(false);
		$result = $query->matching(
				$query->logicalAnd(
						$query->equals('uid', $uid),
						$query->equals('sys_language_uid', $languageUid)
				)
		)->execute();

		return $result;
	}

	/**
	 * Find sections by language
	 *
	 * @param $languageUid
	 * @param $parentUid
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByLanguage($languageUid, $parentUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectSysLanguage(false);
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setSysLanguageUid($languageUid);
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$result = $query->matching(
				$query->logicalAnd(
						$query->equals('sys_language_uid', $languageUid),
						$query->equals('l10n_parent', $parentUid)
				)
		)->execute();

		return $result;
	}

}

?>