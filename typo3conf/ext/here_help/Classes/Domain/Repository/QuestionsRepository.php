<?php
namespace Meelogic\HereHelp\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class QuestionsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {


	/**
	 * Fetch FAQs by faqID
	 *
	 * @param $faqsuid
	 * @param $languageUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByFaqs($faqsuid, $languageUid = 0) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->getQuerySettings()->setSysLanguageUid($languageUid);
		if($languageUid > 0) {
			$query->statement($q='
				SELECT q2.uid, q.faqs, q.faqs as faqs_uid, a.answer as answer, q2.question as question, q.paths as paths
				FROM
					tx_herehelp_domain_model_questions as q
				LEFT JOIN tx_herehelp_domain_model_questions as q2 ON (q.uid = q2.l10n_parent AND q2.sys_language_uid = '.$languageUid.')
				LEFT JOIN tx_herehelp_domain_model_answers as a ON (q2.answers = a.uid)
				WHERE
					q.faqs=' . $faqsuid .' AND q.deleted=0 AND q.hidden=0 AND q2.deleted=0 AND q2.hidden=0 ORDER BY q.sorting ASC');
		} else {
			$query->statement($q='
				SELECT q.*,a.*, a.answer as answers
				FROM
					tx_herehelp_domain_model_questions as q
				LEFT JOIN tx_herehelp_domain_model_answers as a ON (q.answers = a.uid)
				WHERE
					q.faqs=' . $faqsuid .' AND q.deleted=0 AND q.hidden=0 ORDER BY q.sorting ASC');
		}

		$res = $query->execute();

		return $res;
	}
}