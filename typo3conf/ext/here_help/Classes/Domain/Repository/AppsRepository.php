<?php
namespace Meelogic\HereHelp\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AppsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Find by UID
	 *
	 * @param int $uid
	 * @return array|object|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByUid($uid) {
		$query = $this->createQuery();
		$query->statement('SELECT * FROM tx_herehelp_domain_model_apps WHERE uid=' . $uid . ' AND hidden=0 AND deleted=0 ORDER BY sorting ASC');

		return $query->execute();
	}

	/**
	 * Find By Paths
	 *
	 * @param $pathUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByPaths($pathUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(false);
		$query->statement(
			'SELECT * FROM tx_herehelp_domain_model_apps WHERE paths=' . $pathUid . ' AND hidden=0 AND deleted=0 ORDER BY sorting ASC'
		);

		return $query->execute();
	}

	/**
	 * Find Siblins of given App
	 *
	 * @param integer $appId
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findAppSiblings($appId) {
		$query = $this->createQuery();
		$query->getQuerySettings()
			->setReturnRawQueryResult(true);
		$query->statement(
			'
						SELECT a.uid as app_uid, aa.* FROM
							tx_herehelp_domain_model_apps as a
						LEFT JOIN
							tx_herehelp_domain_model_subsections as s ON (a.subsections = s.uid)
						LEFT JOIN
							tx_herehelp_domain_model_apps as aa ON (aa.subsections = s.uid)
						WHERE
							a.uid=' . $appId . ' AND
                a.deleted=0 AND
                a.hidden=0 AND
                aa.deleted=0 AND
                aa.hidden=0 AND
                s.deleted=0 AND
                s.hidden=0 ORDER BY sorting ASC'
		);

		return $query->execute();
	}

	/**
	 * Find Apps by Subsections
	 *
	 * @param $sid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findBySubsections($sid) {
		$constraints = array();
		$query       = $this->createQuery();

		$constraints[] = $query->like('subsections', $sid);

		if (!empty($constraints)) {
			$query->matching($query->logicalAnd($constraints));
		}

		return $query->execute();
	}

}

?>