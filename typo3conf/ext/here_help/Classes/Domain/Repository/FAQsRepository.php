<?php
namespace Meelogic\HereHelp\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class FAQsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * questionsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\QuestionsRepository
	 * @inject
	 */
	protected $questionsRepository = null;

	/**
	 * Fetch FAQs by subsections ID in Apps
	 *
	 * @param $app
	 * @param $languageUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByAppSubsection($app, $languageUid = 0) {
		$query = $this->createQuery();
		$query->getQuerySettings()
			->setReturnRawQueryResult(true);
		$query->statement('SELECT * FROM tx_herehelp_domain_model_apps WHERE subsections=' . $app);
		$sub = $query->execute();

		$res = array();
		for ($x = 0; $x < count($sub); $x++) {
			$query = $this->createQuery();
			$query->getQuerySettings()->setReturnRawQueryResult(true);
			$query->statement(
				'SELECT f.*, f.uid as faqs_uid, q.uid, q.question, a.uid, a.answer, a.image FROM tx_herehelp_domain_model_faqs as f LEFT JOIN tx_herehelp_domain_model_questions as q ON (q.faqs = f.uid) LEFT JOIN tx_herehelp_domain_model_answers as a ON (a.uid = q.answers) WHERE f.apps='
				. $sub[$x]['uid'] . ' AND f.deleted=0 AND f.hidden=0 AND q.hidden=0 AND q.deleted=0 AND f.sys_language_uid = ' . $languageUid . ' ORDER BY sorting ASC');

			$ft = $query->execute();

			// in case we have an empty result we assume that we need fallback to default language
			if (empty($ft)) {
				$query->statement(
					'SELECT f.*, f.uid as faqs_uid, q.uid, q.question, a.uid, a.answer, a.image FROM tx_herehelp_domain_model_faqs as f LEFT JOIN tx_herehelp_domain_model_questions as q ON (q.faqs = f.uid) LEFT JOIN tx_herehelp_domain_model_answers as a ON (a.uid = q.answers) WHERE f.apps='
					. $sub[$x]['uid'] . ' AND f.deleted=0 AND f.hidden=0 AND q.hidden=0 AND q.deleted=0 AND f.sys_language_uid =0 ORDER BY sorting ASC');

				$ft = $query->execute();
			}

			array_push($res, $ft);
		}

		return $res;
	}

	/**
	 * Fetch FAQs by App UID
	 *
	 * @param $uid
	 * @param $languageUid
	 *
	 * @return array
	 */
	public function findByApp($uid, $languageUid = 0) {

		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->getQuerySettings()->setSysLanguageUid($languageUid);

		$query->statement('SELECT * FROM tx_herehelp_domain_model_faqs WHERE apps=' . $uid . ' AND deleted=0 AND hidden=0 AND sys_language_uid = ' . $languageUid . ' ORDER BY sorting ASC');
		$res = $query->execute();

		// in case we have an empty result we assume that we need fallback to default language
		if (empty($res)) {
			$languageUid    = 0;
			$query->statement('SELECT * FROM tx_herehelp_domain_model_faqs WHERE apps=' . $uid . ' AND deleted=0 AND hidden=0 AND sys_language_uid = ' . $languageUid . ' ORDER BY sorting ASC');
			$res = $query->execute();
		}

		// test first item fo have the questions
		if(count($res) > 0) {
			if(is_numeric($res[0]["questions"]) AND $res[0]["questions"] > 0) {
				foreach($res as $k => $resdata) {
					$res[$k]["questions"] = $this->questionsRepository->findByFaqs($resdata["uid"], $languageUid);
				}
			}
		}

		return $res;
	}

	/**
	 * Find by UID
	 *
	 * @param int $uid
	 *
	 * @return array|object|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByUid($uid) {
		$contraints = array();
		$query      = $this->createQuery();

		$contraints[] = $query->like('uid', $uid);

		if (!empty($contraints)) {
			$query->matching(($query->logicalAnd($contraints)));
		}

		return $query->execute();
	}

	/**
	 * Find FAQs for a section only
	 *
	 * @param $sid
	 * @param $languageUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findBySection($sid, $languageUid = 0) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement(
			$q =
				'SELECT f.*, f.uid as faqs_uid, q.uid, q.question, a.uid, a.answer, a.image FROM tx_herehelp_domain_model_faqs as f LEFT JOIN tx_herehelp_domain_model_questions as q ON (q.faqs = f.uid) LEFT JOIN tx_herehelp_domain_model_answers as a ON (a.uid = q.answers) WHERE f.sections='
				. $sid . ' AND f.deleted=0 AND f.hidden=0 AND q.deleted=0 AND q.hidden=0 AND f.sys_language_uid = ' . $languageUid . ' ORDER BY sorting ASC');
		$res = $query->execute();

		// in case we have an empty result we assume that we need fallback to default language
		if (empty($res)) {
			$query->statement(
						$q =
							'SELECT f.*, f.uid as faqs_uid, q.uid, q.question, a.uid, a.answer, a.image FROM tx_herehelp_domain_model_faqs as f LEFT JOIN tx_herehelp_domain_model_questions as q ON (q.faqs = f.uid) LEFT JOIN tx_herehelp_domain_model_answers as a ON (a.uid = q.answers) WHERE f.sections='
							. $sid . ' AND f.deleted=0 AND f.hidden=0 AND f.sys_language_uid=0 AND q.deleted=0 AND q.hidden=0 ORDER BY sorting ASC');
			$res = $query->execute();
		}

		return $res;
	}

	/**
	 * Find FAQs for a subsection only
	 *
	 * @param $sid
	 * @param $languageUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findBySubsection($sid, $languageUid = 0) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->getQuerySettings()->setSysLanguageUid($languageUid);
		$query->statement(
			$q = 'SELECT * FROM tx_herehelp_domain_model_faqs WHERE subsections=' . $sid . ' AND deleted=0 AND hidden=0 AND sys_language_uid = ' . $languageUid . ' ORDER BY sorting ASC');

		$res = $query->execute();

		// in case we have an empty result we assume that we need fallback to default language
		if (empty($res)) {
			$query->statement($q = 'SELECT * FROM tx_herehelp_domain_model_faqs WHERE subsections=' . $sid . ' AND deleted=0 AND hidden=0 AND sys_language_uid =0 ORDER BY sorting ASC');
			$res = $query->execute();
			$languageUid = 0;
		}

		// test first item fo have the questions
		if(count($res) > 0) {
			if(is_numeric($res[0]["questions"]) AND $res[0]["questions"] > 0) {
				foreach($res as $k => $resdata) {
					$res[$k]["questions"] = $this->questionsRepository->findByFaqs($resdata["uid"],$languageUid);
				}
			}
		}

		return $res;
	}
}