<?php
namespace Meelogic\HereHelp\Domain\Repository;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class TransitRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

	/**
	 * @param $isoLanguage
	 * @return string
	 */
	function mapLanguage( $isoLanguage ) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement(
			sprintf(
				"SELECT DISTINCT isoLanguage
				 FROM tx_herehelp_transit_cities
				 WHERE isoLanguage='%s'
				 "
				,$isoLanguage
			)
		);
		$count = count($query->execute());
		return $count > 0 ? $isoLanguage : 'en';
	}

	/**
	 * Returns list of covered regions in given language.
	 *
	 * @param $isoLanguage
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	function getTransitRegions( $isoLanguage ) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement(
			sprintf(
				"SELECT DISTINCT isoRegion,regionName
				 FROM tx_herehelp_transit_cities
				 WHERE isoLanguage='%s'
				 ORDER BY regionName ASC
				 "
				,$isoLanguage
			)
		);

		$res = $query->execute();

		// in case we have an empty result we assume that we need fallback to default language
		if (empty($res)) {
			$query->statement(
				sprintf(
					"SELECT DISTINCT isoRegion,regionName
					 FROM tx_herehelp_transit_cities
					 WHERE isoLanguage='en'
					 ORDER BY regionName ASC
					 "
					,$isoLanguage
				)
			);

			$res = $query->execute();
		}

		return $res;

	}

	/**
	 * Returns list of covered countries for given region in given language.
	 *
	 * @param $isoLanguage
	 * @param $isoRegion
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	function getTransitCountries( $isoLanguage, $isoRegion ) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement(
			sprintf(
				"SELECT DISTINCT isoCountry,countryName
				 FROM tx_herehelp_transit_cities
				 WHERE isoLanguage='%s' AND isoRegion='%s'
				 ORDER BY countryName ASC
				 "
				,$isoLanguage
				,$isoRegion
			)
		);

		$res = $query->execute();

		// in case we have an empty result we assume that we need fallback to default language
		if (empty($res)) {
			$query->statement(
				sprintf(
						"SELECT DISTINCT isoCountry,countryName
						 FROM tx_herehelp_transit_cities
						 WHERE isoLanguage='en' AND isoRegion='EU'
						 ORDER BY countryName ASC
						 "
						,$isoLanguage
						,$isoRegion
				)
			);

			$res = $query->execute();
		}

		return $res;

	}

	/**
	 * Returns list of covered cities with quality for given country in given language.
	 *
	 * @param $isoLanguage
	 * @param $isoCountry
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	function getTransitCities( $isoLanguage, $isoCountry ) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement(
			sprintf(
				"SELECT DISTINCT id,cityName,quality
				 FROM tx_herehelp_transit_cities
				 WHERE isoLanguage='%s' AND isoCountry='%s'
				 ORDER BY cityName ASC
				 "
				,$isoLanguage
				,$isoCountry
			)
		);

		$res = $query->execute();

		// in case we have an empty result we assume that we need fallback to default language
		if (empty($res)) {
			$query->statement(
				sprintf(
						"SELECT DISTINCT id,cityName,quality
						 FROM tx_herehelp_transit_cities
						 WHERE isoLanguage='en' AND isoCountry='%s'
						 ORDER BY cityName ASC
						 "
						,$isoCountry
				)
			);

			$res = $query->execute();
		}

		return $res;

	}
}

?>