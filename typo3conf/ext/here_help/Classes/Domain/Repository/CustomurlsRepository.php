<?php
namespace Meelogic\HereHelp\Domain\Repository;

    /***************************************************************
     *  Copyright notice
     *
     *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
     *
     *  All rights reserved
     *
     *  This script is part of the TYPO3 project. The TYPO3 project is
     *  free software; you can redistribute it and/or modify
     *  it under the terms of the GNU General Public License as published by
     *  the Free Software Foundation; either version 3 of the License, or
     *  (at your option) any later version.
     *
     *  The GNU General Public License can be found at
     *  http://www.gnu.org/copyleft/gpl.html.
     *
     *  This script is distributed in the hope that it will be useful,
     *  but WITHOUT ANY WARRANTY; without even the implied warranty of
     *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     *  GNU General Public License for more details.
     *
     *  This copyright notice MUST APPEAR in all copies of the script!
     ***************************************************************/

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class CustomurlsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Find by Paths
     *
     * @param $path
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByPaths($path)
    {

        $constraints = array();
        $query = $this->createQuery();

        $constraints[] = $query->like('paths', $path);

        if (!empty($constraints)) {
            $query->matching($query->logicalAnd($constraints));
        }

        return $query->execute();
    }

    /**
     * Find by App
     *
     * @param $app
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByApps($app)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setReturnRawQueryResult(FALSE);
        $query->statement('SELECT uid FROM tx_herehelp_domain_model_customurls WHERE paths="' . $app . '" AND hidden=0 AND deleted=0');
        return $query->execute();
    }

    /**
     * Find by App
     *
     * @param $path
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByPath($path)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setReturnRawQueryResult(false);
        $query->statement($q = 'SELECT uid FROM tx_herehelp_domain_model_customurls WHERE paths = \'' . $path . '\' AND hidden=0 AND deleted=0');
        return $query->execute();
    }


}

?>