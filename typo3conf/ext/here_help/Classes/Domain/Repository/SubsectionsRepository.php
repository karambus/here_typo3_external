<?php
namespace Meelogic\HereHelp\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Utility\DebugUtility;

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SubsectionsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Find by UID
	 *
	 * @param int $uid
	 *
	 * @return array|object|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByUid($uid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(false);
		$query->statement('SELECT * FROM tx_herehelp_domain_model_subsections WHERE uid=' . $uid . ' AND hidden=0 AND deleted=0 ORDER BY sorting ASC');

		return $query->execute();
	}

	/**
	 * Find by AppId
	 *
	 * @param int $appId
	 *
	 * @return array|object|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByAppID($appId) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('SELECT subsections FROM tx_herehelp_domain_model_apps WHERE uid=' . $appId . ' AND deleted=0 AND hidden=0 ORDER BY sorting ASC');

		$res = $query->execute();
		if (is_array($res) AND count($res) === 1 AND isset($res[0]["subsections"])) {
			$inst = $this->findByUid($res[0]["subsections"]);
			if (count($inst) === 1) {
				return $inst[0];
			}

			return $inst;
		}

		return null;
	}

	/**
	 * Find Subsections based on Paths UID
	 *
	 * @param $pathUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByPaths($pathUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(false);
		$query->statement('SELECT * FROM  tx_herehelp_domain_model_subsections WHERE paths=' . $pathUid . ' AND hidden=0 AND deleted=0 ORDER BY sorting ASC');

		return $query->execute();
	}

	/**
	 * Find Subsections based on Apps UID
	 *
	 * @param $pathUid
	 *
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByApps($pathUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(false);
		$query->statement('SELECT uid FROM tx_herehelp_domain_model_subsections WHERE apps=' . $pathUid	. ' AND hidden=0 AND deleted=0 ORDER BY sorting ASC');

		return $query->execute();
	}

	/**
	 * Find localized subsection
	 *
	 * @param $languageUid
	 * @param $parentUid
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function findByLanguage($languageUid, $parentUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setRespectSysLanguage(false);
		$query->getQuerySettings()->setRespectStoragePage(false);
		$query->getQuerySettings()->setSysLanguageUid($languageUid);
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$result = $query->matching(
				$query->logicalAnd(
						$query->equals('sys_language_uid', $languageUid),
						$query->equals('l10n_parent', $parentUid)
				)
		)->execute();

		return $result;

	}

}

?>