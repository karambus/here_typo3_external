<?php
namespace Meelogic\HereHelp\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use Meelogic\HereHelp\Domain\Model\AbstractHelpModel;
use Meelogic\HereHelp\Domain\Model\Sections;

/**
 *
 *
 * @package here_help
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SectionsController extends AbstractController {

	/**
	 * sectionsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\SectionsRepository
	 * @inject
	 */
	protected $sectionsRepository = null;

	/**
	 * subsectionsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\SubsectionsRepository
	 * @inject
	 */
	protected $subsectionsRepository = null;

	/**
	 * appsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\AppsRepository
	 * @inject
	 */
	protected $appsRepository = null;

	/**
	 *
	 * faqsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\FAQsRepository
	 * @inject
	 */
	protected $faqsRepository = null;

	/**
	 * customurlsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\CustomurlsRepository
	 * @inject
	 */
	protected $customurlsRepository = null;

	/**
	 * questionsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\QuestionsRepository
	 * @inject
	 */
	protected $questionsRepository = null;

	/**
	 * action list
	 *
	 * @return void
	 */
	public function listAction() {
		$sections = $this->sectionsRepository->findAll();

		$this->view->assignMultiple(
			array(
				'sections'       => $sections,
                'hasSubsections' => $this->_hasSubsections($sections),
                'settings'       => $this->settings,
				'activeSection'  => 1
			)
		);

		$this->view->assign('sections', $sections);
	}

	/**
	 * action show
	 *
	 * @return void
	 */
	public function showAction() {

		// if no url is set we assume we're on the landingpage
		if (($this->settings['path'] === '') || (!isset($this->settings['path']))) {

			// try to get cached content.
			// if there is no content in cache, just return it
			// Otherwise generate the content, save in cache and then return it
			$cacheIdentifier = $this->calculateCacheIdentifier(array(
				$GLOBALS['TSFE']->sys_language_uid,
			));

			$cache = $GLOBALS['typo3CacheManager']->getCache('herehelp_cache');

			if (($content = $cache->get($cacheIdentifier)) === false) {
				// no content in cache, generate
				$content = $this->getListActionContent();

				// save content in cache
				$cache->set($cacheIdentifier, $content, array(
					'listAction_herehelp'
				));
			}

			return $content;

		} else {
			$sections   = null;
			$subsection = null;
			$faqs       = null;
			$app        = null;
			$appPaths   = $this->customurlsRepository->findAll();

			$app        = $this->appsRepository->findByPaths($this->settings['path'])->getFirst();
			$subsection = $this->subsectionsRepository->findByPaths($this->settings['path'])->getFirst();

			if (isset($app)) {
				$activeApp = $app->getUid();
				$section = $this->sectionsRepository->findByUid($app->getSubsections());
				if (isset($section)) {
					$activeSec = $section->getUID();
				}
			}
			if (isset($subsection)) {
				$activeApp = null;
				$activeSub = $subsection->getUID();
				$activeSec = $subsection->getSections();

				/** Get localized data in respect of Claudia's handling using "Localize all records" */
				if ($this->getCurrentLanguageUID() > 0) {
					$activeSection      = $this->sectionsRepository->findByLanguage($this->getCurrentLanguageUID(), $activeSec);
					if (!empty($activeSection)) {
						$activeSec = $activeSection[0]['_LOCALIZED_UID'];
					}
					$activeSubsection   = $this->subsectionsRepository->findByLanguage($this->getCurrentLanguageUID(), $activeSub);
					if (!empty($activeSubsection)) {
						$activeSub = $activeSubsection[0]['_LOCALIZED_UID'];
					}
				}
			}

			// section given
			if ($activeSec) {
				// also subsection given
				if ($activeSub) {
					$sections       = $this->sectionsRepository->findByUidAndLanguage($activeSec, $this->getCurrentLanguageUID());
					$faqs           = $this->faqsRepository->findBySubsection($activeSub, $this->getCurrentLanguageUID());
					$checkForApps   = $this->appsRepository->findBySubsections($activeSub);

					if ($checkForApps->count() > 0) {
						$sections = $checkForApps;
						$activeApp = null;
					}

				} else {
					// subsection not given, app given
					if ($activeApp) {
						$sections   = $this->appsRepository->findAppSiblings($activeApp);
						$faqs       = $this->faqsRepository->findByApp($activeApp, $this->getCurrentLanguageUID());
						$app        = $this->appsRepository->findByUid($activeApp);
						$subsection = $this->subsectionsRepository->findByAppID($activeApp);
						// only section given
					} else {
						$sections = $this->sectionsRepository->findAll();
						$faqs = $this->faqsRepository->findBySection($activeSec, $this->getCurrentLanguageUID());
					}
				}
				// section not given


			} else {
				// subsection given
				if ($activeSub) {
					// also given
					if ($activeApp) {
						$sections   = $this->appsRepository->findBySubsections($activeApp);
						$faqs       = $this->faqsRepository->findByAppSubsection($activeApp, $this->getCurrentLanguageUID());
						// only subsection given
					} else {
						$sections   = $this->sectionsRepository->findAll();
						$apps       = $this->appsRepository->findBySubsections($activeSub);
						if (count($apps) > 0) {
							$sections = $apps;
						}
						$faqs = $this->faqsRepository->findBySubsection($activeSub, $this->getCurrentLanguageUID());
					}
					// no section and subsection given
				} else {
					// only app given
					if ($activeApp) {
						$faqs       = $this->faqsRepository->findByApp($activeApp, $this->getCurrentLanguageUID());
						$sections   = $this->appsRepository->findAppSiblings($activeApp);
						// nothing given
					} else {
						$sections   = $this->sectionsRepository->findAll();
						$faqs       = $this->faqsRepository->findBySection($activeSec, $this->getCurrentLanguageUID());
					}
				}
			}

			// make sure that view gets an array so icon is rendered correctly
			if ($sections instanceof \Meelogic\HereHelp\Domain\Model\Sections) {
				$sections = array($sections);
			}

			$this->view->assignMultiple(
					array(
							'sections'          => $sections,
							'subsection'        => $subsection,
							'hasSubsections'    => $this->_hasSubsections($sections),
							'faqs'              => $faqs,
							'app'               => $app,
							'activeSubsection'  => $activeSub,
							'activeSection'     => $activeSec,
							'activeApp'         => $activeApp,
							'appPaths'          => $appPaths,
							'headline'          => $this->settings['headline'],
							'uri'               => $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI]
					)
			);
		}
	}

	/**
	 * Check is section has subsections
	 *
	 * @param $sections
	 * @return bool
	 */
	private function _hasSubsections($sections) {
        foreach ($sections as $section) {
            if ($section instanceof Sections && $section->getSubsections()->count() > 0) {
                return true;
            }
        }
        return false;
    }

	/**
	 * Calculates the cache identifier
	 *
	 * @param \array $arr
	 * @return \string
	 */
	public static function calculateCacheIdentifier($arr) {
		return sha1(json_encode($arr));
	}

	/**
	 * Method to get content for the listAction, used by internal caching
	 *
	 * @return string
	 */
	public function getListActionContent() {
		$sections = $this->sectionsRepository->findAll();

		$this->view->assignMultiple(
			array(
				'sections'       => $sections,
                'hasSubsections' => $this->_hasSubsections($sections),
                'settings'       => $this->settings,
				'activeSection'  => 1
			)
		);

		return $this->view->render();
	}

}

?>