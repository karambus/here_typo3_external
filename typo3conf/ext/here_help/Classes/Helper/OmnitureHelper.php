<?php
namespace Meelogic\HereHelp\Helper;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Christoph Kluge <christoph.kluge@meelogic.com>, Meelogic AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Mvc\Request;

class OmnitureHelper {

	/**
	 * Add omniture code to header
	 *
	 * @param Request $request
	 * @return void
	 */
	public function addOmniture(Request $request) {

		$objectManager        = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'TYPO3\CMS\Extbase\Object\ObjectManager'
		);
		$configurationManager = $objectManager->get(
			'TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface'
		);
		$settings             = $configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
			'tx_herehelp'
		);

		$OmnitureUrl       = $settings['plugin.']['tx_herehelp.']['settings.']['omniture.']['scripturl'];
		$OmnitureNamespace = $settings['plugin.']['tx_herehelp.']['settings.']['omniture.']['namespace'];

		// FIXME fallback:default values
		if(empty($OmnitureUrl)) {
			$OmnitureUrl = "//tags.tiqcdn.com/utag/here/heresupport/dev/utag.js";
		};
		if(empty($OmnitureNamespace)) {
			$OmnitureNamespace = "supporthere";
		};

		$requestUri = $request->getRequestUri();
		$parseUrlResults = parse_url($requestUri);
		$requestpath = $parseUrlResults["path"];

		$GLOBALS['TSFE']->additionalHeaderData['tx_herehelp'] = $this->getOmnitureScriptCode(
			$this->getOmnitureRoute($OmnitureNamespace . ":" . $requestpath),
			$OmnitureUrl,
			$this->getCurrentLanguageCode(true),
			$this->getCurrentCountryCode(true)
		);

	}

	/**
	 * get the omniture tracking script snippet
	 *
	 * @param string $route     pagename (should be a valid route)
	 * @param string $scripturl environment string (either dev or prod)
	 * @param string $language  2 characters long language code
	 * @param string $country   2 characters long country code
	 *
	 * @fixme script include is currently commented out
	 *
	 * @return string
	 */
	protected function getOmnitureScriptCode($route, $scripturl, $language = "en", $country = "ww") {

		return sprintf(
			'
			<script type="text/javascript">
				var nkT = {
					pName : "%s",
					sCountry : "%s",
					sLang : "%s"
				}
			</script>
			<script src="%s"></script>
			',
			$route,
			$language,
			$country,
			$scripturl
		);
	}

	/**
	 * get a valid omniture route from a given string string
	 *
	 * @param string $fromString what string should be transformed into a valid route
	 *
	 * @return string
	 */
	protected function getOmnitureRoute($fromString) {
		$fromString = iconv('UTF-8', 'ASCII//TRANSLIT', $fromString);
		$fromString = trim($fromString, "/"); // remove trailing slashes
		$fromString = strtolower($fromString); // lowercase this
		$fromString = str_replace("/", ":", $fromString); // change slashes dot doublecolon
		$fromString = preg_replace("/[^a-z0-9:]+/", "", $fromString); // remove unwanted characters
		$fromString = preg_replace("/::+/", ":", $fromString); // remove double doublecolon

		return $fromString;
	}

	/**
	 * get the current used language code
	 *
	 * @return string|null
	 */
	protected function getCurrentLanguageCode() {
		return strtolower($GLOBALS['TSFE']->lang);
	}

	/**
	 * get the current used country code
	 *
	 * @return string|null
	 */
	protected function getCurrentCountryCode() {
		return strtoupper($GLOBALS['TSFE']->lang);
	}
}