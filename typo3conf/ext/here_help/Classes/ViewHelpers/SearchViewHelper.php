<?php

namespace Meelogic\HereHelp\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Class SearchViewHelper
 *
 * @package Meelogic\HereHelp\ViewHelpers
 */

class SearchViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Render the search result link
	 *
	 * @param $array $array
	 * @return void
	 */
	public function render($array) {

		$postArray = \t3lib_div::_GP("tx_herehelp_help");

		// get current pageUrl and request URI
		$pageURL = $_SERVER['SERVER_PORT'] != '80' ? $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] : $_SERVER['SERVER_NAME'];
		$uri = explode('/', $_SERVER['REQUEST_URI']);

		// lang param or not?
		if ($uri[1] === 'help') {
			$currentLanguage = '/help/';
		} else {
			if ($uri[1] === 'search') {
				$currentLanguage = '/';
			} else {
				if ($uri[2] === 'search') {
					$currentLanguage = '/' . $uri[1] . '/';
				} else {
					$currentLanguage = '/' . $uri[1]. '/help/';
				}
			}
		}

		// generate hashtag link
		$hash = '#q' . $array['questionuid'];

		// highlight the searchword in results
		$question = $this->regexp_replace_exclude_urls($postArray['sword'], $array['question']);
		$answer = $this->regexp_replace_exclude_urls($postArray['sword'], $array['answer']);

		// render the result
		$link = '//' . $pageURL . $currentLanguage . $array['path'] . $hash;
		$out = '<div class="title"><a href="' . $link . '">' . $question . '</a></div><p>' . $answer . '</p>';

		return $out;

	}

    public function regexp_replace_exclude_urls($key, $sourceString) {
       // URLs:
       $regex = "/<a[^>]*>/";
       $texts = preg_split($regex, $sourceString);
       preg_match_all($regex, $sourceString, $uris);
       $result = "";
       $index = 0;

		foreach ($texts as $k => $text) {
			$result = $result . str_ireplace($key, '<strong class="tx-indexedsearch-redMarkup">' . $key . '</strong>', $text);
			if (!empty($uris)) {
				if (sizeof($uris[0]) > $index) {
					$result = $result . $uris[0][$index];
				}
			}
			$index = $index + 1;
		} 
		return $result;
	}
}