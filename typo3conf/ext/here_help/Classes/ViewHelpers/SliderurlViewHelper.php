<?php

namespace Meelogic\HereHelp\ViewHelpers;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Class SliderUrlViewHelper
 *
 * @package Meelogic\HereHelp\ViewHelpers
 */

class SliderurlViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager = '';

	/**
	 * customurlsRepository
	 *
	 * @var \Meelogic\HereHelp\Domain\Repository\CustomurlsRepository
	 * @inject
	 */
	protected $customurlsRepository = null;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'TYPO3\CMS\Extbase\Object\ObjectManager'
		);
	}

	/**
	 * @param $array $array
	 *
	 * @return void
	 */
	public function render($array) {

		// get the base Url based on TypoScript settings
		$configurationManager = $this->objectManager->get(
			'TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface'
		);
		$settings             = $configurationManager->getConfiguration(
			\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
			'tx_herehelp'
		);
		$baseUrl              = $settings['plugin.']['tx_herehelp.']['settings.']['baseUrlForRedirect'];
		$imagesPath           = $settings['plugin.']['tx_herehelp.']['settings.']['imagesPath'];

		$urlParts = explode(',', $array);

		$pathUid          = $urlParts[0];
		$appUid           = $urlParts[1];
		$secUid           = $urlParts[2];
		$image            = $urlParts[3];
		$altTitle         = $urlParts[4];
		$isSubsectionLink = (boolean)$urlParts[5];

		// get path
		$path = $this->customurlsRepository->findByUid($pathUid);

		// build link
		$langUrl = explode('/', $baseUrl);

		if ($GLOBALS['TSFE']->sys_language_uid > 0) {
			$linkUrl = $langUrl[0] . '/' . $GLOBALS['TSFE']->lang;

			// use context after domain only if it exists
			if (!empty($langUrl[1])) {
				$linkUrl .= '/' . $langUrl[1];
			}
		} else {
			// remove trailing slash from base URL if exists
			if (substr($baseUrl, -1, 1) == '/') {
				$linkUrl = substr($baseUrl, 0, -1);
			} else {
				$linkUrl = $baseUrl;
			}

			// link fix for development
			if (strpos($baseUrl, 'herelocal.dev') !== false || strpos($baseUrl, 'bald-live.de') !== false) {
				$linkUrl = $baseUrl;
			}
		}

		$urlInclPath = '//' . $linkUrl . '/' . $path->getPaths();
		$link = '<a href="' . $urlInclPath . '">';

		if (isset($image)) {
			$link .= '<img src="' . $imagesPath . $image . '" alt="' . $altTitle . '" title="' . $altTitle . '" />';
		}

		$link .= $altTitle . '</a>';

		return $link;
	}
}
