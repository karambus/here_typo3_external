<?php

namespace Meelogic\HereHelp\ViewHelpers;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Class LangmenuViewHelper
 *
 * @package Meelogic\HereHelp\ViewHelpers
 */

class LangmenuViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * Render the language menu based on available languages within TYPO3
	 *
	 * @return void
	 */
	public function render() {

		// get current pageUrl and request URI
		$pageURL = $_SERVER['SERVER_PORT'] != '80' ? $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] : $_SERVER['SERVER_NAME'];
		$requestURI = $_SERVER['REQUEST_URI'];
		$exlReq = explode('/', $requestURI);

		// if we're already on a language the request uri will contain the current lang uid!
		if ($GLOBALS['TSFE']->sys_language_uid > 0) {
			$requestURI = substr($requestURI, 3);
		}

		$sysLanguages = $this->getSysLanguages();

		// render the language menu
		$out = '<div id="language-switcher">
				<a href="#">' . strtoupper($GLOBALS['TSFE']->lang) . '</a>
				<ul role="languages">
				';

		foreach ($sysLanguages as $lang) {

			if ($lang['uid'] === 0) {
				$link = '//' . $pageURL . $requestURI;
			} else {
				$link = '//' . $pageURL . '/' . strtolower($lang['ISOcode']) . $requestURI;
			}

			$out .= '<li><a href="' . $link . '">
							<span class="code">' . $lang['ISOcode'] . '</span>
							<span class="name">' . $lang['title'] . '</span>
						</a>
					</li>';
			}

		$out .= '</ul></div>';

		return $out;

	}

	/**
	 * Returns an array of sys_language records containing the ISO code as the key and the record's uid as the value
	 *
	 * @return	array	sys_language records: ISO code => uid of sys_language record
	 */
	private function getSysLanguages() {

		$availableLanguages = array();

		$sys_languages = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'sys_language', 'faqs=1', '', 'flag ASC');
		foreach ($sys_languages as $row) {
			$languageIconTitles[$row['uid']] = $row;
			if ($row['static_lang_isocode'] && \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('static_info_tables')) {
				$staticLangRow = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('static_languages', $row['static_lang_isocode'], 'lg_iso_2');
				if ($staticLangRow['lg_iso_2']) {
					$languageIconTitles[$row['uid']]['ISOcode'] = $staticLangRow['lg_iso_2'];
				}
			}
			if (strlen($row['flag'])) {
				$languageIconTitles[$row['uid']]['flagIcon'] = \TYPO3\CMS\Backend\Utility\IconUtility::mapRecordTypeToSpriteIconName('sys_language', $row);
			}
		}

		// add default language to array
		$languageIconTitles[] = array(
			'uid' => 0,
			'title' => 'English (UK)',
			'flag' => 'en',
			'static_lang_isocode' => '30',
			'ISOcode' => 'EN'
		);

		// sort array based on 'ISOcode'
		$key = 'ISOcode';
		uasort($languageIconTitles,
		       function($a,$b) use ($key){
		          return strcmp($a[$key],$b[$key]);
		       }
		);

		return $languageIconTitles;
	}



}