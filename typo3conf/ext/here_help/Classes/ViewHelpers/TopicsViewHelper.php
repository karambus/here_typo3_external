<?php

namespace Meelogic\HereHelp\ViewHelpers;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Christoph Kluge <christoph.kluge@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * ViewHelper to display the complete category/questions listing
 *
 * @author  Christoph Kluge <christoph.kluge@meelogic.com>
 * @since   2014-09-01
 *
 * @package Meelogic\HereHelp\ViewHelpers
 */
class TopicsViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper
{

    /**
     * transitRepository
     *
     * @var \Meelogic\HereHelp\Domain\Repository\TransitRepository
     * @inject
     */
    protected $transitRepository = null;

    protected $faqs = array();

    /**
     * transform the faqs into a easily handable structure
     * faqs will be available in $faqs property
     *
     * @param $faqs
     *
     * @return void
     */
    protected function prepareFaqs($faqs)
    {

        if (is_array($faqs) AND isset($faqs['uid'])) {
            $faqs = array($faqs);
        }

        foreach ($faqs as $faq) {

            if (isset($faq['faqs_uid'])) {
                $faqsuid = $faq['faqs_uid'];
            } else {
                $faqsuid = $faq['uid'];
            }

            if (isset($this->faqs[$faqsuid]) === false) {
                $this->faqs[$faqsuid] = array(
                    'uid' => $faqsuid,
                    'headline' => $faq['headline'],
                    'transitdb' => $faq['transitdb'],
                    'questions' => array()
                );
            }

            if (isset($faq['answer'])) {
                $this->faqs[$faqsuid]['questions'][] = $faq;
            } else {
                $this->faqs[$faqsuid]['questions'] = $faq['questions'];
            }
        }
    }

    /**
     * @param $array $array
     *
     * @return string
     */
    public function render($array)
    {
        $this->prepareFaqs($array);

        $faqs = array();
        foreach ($this->faqs as $faq) {

            $questions = array();
            foreach ($faq['questions'] as $question) {

                if (strlen(trim($question['answer'])) === 0 OR strlen(trim($question['question'])) === 0) continue;

                $hash = 'q' . $question['uid'];
                $html = sprintf(
                    '<li>
                        <a href="#%1$s" class="question" ref="%1$s">%2$s</a>
                        <div class="answer">
                            %3$s%4$s
                        </div>
                    </li>',
                    $hash,
                    $question['question'],
                    // TODO will this <f:image/> work properly?
                    $question['image'] ? '<f:image src="uploads/tx_herehelp/' . $question['image'] . '" alt="" />' : '',
                    $question['answer']
                );
                $questions[] = $html;
            }

            $transitdb = null;

            $hash = 'g' . $faq['uid'];
            if ($faq['transitdb'] != 0) {
                $faqs[] =
                    '<li class="root-question">
                        <a href="#' . $hash . '" ref="' . $hash . '" class="question">' . $faq['headline'] . '</a>' .
                        $this->getTransitHtml() .
                    '</li>';
            } else {
                if (count($questions) > 0) {
                    $faqs[] =
                        '<li>
                            <a href="#' . $hash . '" ref="' . $hash . '" class="question">' . $faq['headline'] . '</a>
                            <ul class="group">
                                ' . join("\n", $questions) . '
                            </ul>
                        </li>';
                }
            }
        }

        return join('', $faqs);
    }


    /**
     * geht the complete html for coverage item
     *
     * @return string
     */
    protected function getTransitHtml()
    {

        // optioncontainers
        $regionOptions = array();
        $countryOptions = array();
        $cityOptions = array();
        // selectitems html
        $regionSelectHtml = '';
        $countrySelectHtml = '';
        $citySelectHtml = '';
        // get current language from typo3 globals, yikes!
        $languageUID = $GLOBALS['TSFE']->lang;
        // shortcut the injected transitRepository,
        $repo = $this->transitRepository;

        $coverageData = array();
        $leadText = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_topicsviewhelper.copytext', 'here_help');
        $legendHeader = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_cities.header', 'here_help');
        $legendSimpleRouting = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_cities.simplerouting', 'here_help');
        $legendTimeTableRouting = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_cities.timetablerouting', 'here_help');
        $legendRealTimeRouting = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_cities.realtimerouting', 'here_help');

        // get the regions from the transit db
        $regions = $repo->getTransitRegions($languageUID);

        // iterate through all regions returned
        foreach ($regions as $region) {

            // assign regiondata to simple variables for further usage
            $regionISO = $region['isoRegion'];
            $regionName = $region['regionName'];

            // add all regions to the regionsoptions container
            $regionOptions[] = sprintf(
                '<option value="%s">%s</option>',
                $regionISO,
                $regionName
            );

            // get the countries for current region
            $countries = $repo->getTransitCountries($languageUID, $regionISO);
            // and create a region index within the countryoptions container for later usage
            $countryOptions[$regionISO] = array();

            // iterate through the countries returned
            foreach ($countries as $country) {

                // assign countrydata to simple variables for further usage
                $countryISO = $country['isoCountry'];
                $countryName = $country['countryName'];

                // add all countries to the regions countryoptions container
                $countryOptions[$regionISO][] = sprintf(
                    '<option value="%s">%s</option>',
                    $countryISO,
                    $countryName
                );

                // get all cities for the given country
                $cities = $repo->getTransitCities($languageUID, $countryISO);
                // and create city index within the cityoptions container for later usage
                $cityOptions[$countryISO] = array();
                $uniqueCities = array();

                foreach ($cities as $city) {
                    $name = $city['cityName'];
                    if (!isset($uniqueCities[$name])) {
                        $uniqueCities[$name] = array(
                            'ids'     => array(),
                            'quality' => array(),
                        );
                    }
                    $uniqueCities[$name]['ids'][] = $city['id'];
                    $quality = array_map('trim', explode(',', $city['quality']));
                    $uniqueCities[$name]['quality'] = array_unique(array_merge($uniqueCities[$name]['quality'], $quality));
                }

                // iterate through all cities retured
                foreach ($uniqueCities as $cityName => $cityData) {

                    // assign citydata to simple variables for further usage
                    $cityIds = implode(',', $cityData['ids']);
                    $cityQualities = implode(' & ', $cityData['quality']);

                    // add all cities to the countries cityoptions container
                    $cityOptions[$countryISO][] = sprintf(
                        '<option value="%s" data-quality="%s">%s</option>',
                        $cityIds,
                        $cityQualities,
                        $cityName
                    );
                }
            }
        }

        // build html: regions select field
        $regionSelectHtml = sprintf(
            '<label for="regions">%s</label><select name="region" id="regions"><option></option>%s</select>',
            \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_label.regions', 'here_help'),
            join('', $regionOptions)
        );

        // build html: all country select fields
        $countryOptions = array_merge(array('' => ''), $countryOptions);
        foreach ($countryOptions as $regionISO => $countryItems) {
            $countrySelectHtml .= sprintf(
                '<label for="countries_in_%2$s" class="hidden">%1$s</label><select name="country" id="countries_in_%2$s" class="hidden"><option></option>%3$s</select>',
                \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_label.countries', 'here_help'),
                $regionISO,
                join('', $countryItems)
            );
        }

        // build html: all citiy select fields
        $cityOptions = array_merge(array('' => ''), $cityOptions);
        foreach ($cityOptions as $countryISO => $cityItems) {
            $citySelectHtml .= sprintf(
                '<label for="cities_in_%2$s" class="hidden">%1$s</label><select name="city" id="cities_in_%2$s" class="hidden"><option></option>%3$s</select>',
                \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('tx_herehelp_transit_label.cities', 'here_help'),
                $countryISO,
                join('', $cityItems)
            );
        }

        return sprintf('
			<div class="answer" id="here_help_transitcoverage">
				<p>%s</p>
				<form>
				    <div class="row">
						<div class="col-md-4">
							%s
						</div>
						<div class="col-md-4">
							%s
						</div>
						<div class="col-md-4">
							%s
						</div>
					</div>
                    <div id="here_help_transitcoverage_result"></div>
                    <div id="here_help_transitcoverage_legend" data-legend0="%s" data-legend1="%s" data-legend2="%s" data-legend3="%s"></div>
				</form>
			</li>',
            $leadText,
            $regionSelectHtml,
            $countrySelectHtml,
            $citySelectHtml,
            $legendHeader,
            $legendSimpleRouting,
            $legendTimeTableRouting,
            $legendRealTimeRouting
        );
    }
}
