<?php
namespace Meelogic\HereHelp\Hook;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Meelogic\HereHelp\Domain\Model\Apps;
use Meelogic\HereHelp\Domain\Model\Subsections;
use Meelogic\HereHelp\Domain\Repository\SubsectionsRepository;

/**
 * Class ContentPostProc
 *
 * @package Meelogic\HereHelp\Hook
 */
class ContentPostProc {

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager = '';

	/**
	 * @var \Meelogic\HereHelp\Domain\Repository\CustomurlsRepository
	 */
	protected $customUrlsRepository = '';

	/**
	 * @var \Meelogic\HereHelp\Domain\Repository\SubsectionsRepository
	 */
	protected $subsectionsRepository = '';

	/**
	 * @var \Meelogic\HereHelp\Domain\Repository\AppsRepository
	 */
	protected $appsRepository = '';

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->objectManager         = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'TYPO3\CMS\Extbase\Object\ObjectManager'
		);
		$this->customUrlsRepository  = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'Meelogic\HereHelp\Domain\Repository\CustomurlsRepository'
		);
		$this->subsectionsRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'Meelogic\HereHelp\Domain\Repository\SubsectionsRepository'
		);
		$this->appsRepository        = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(
			'Meelogic\HereHelp\Domain\Repository\AppsRepository'
		);
	}

	/**
	 * Match the URL to redirect to the selected FAQ item
	 */
	public function urlMatching() {

		// ignore if any of this has been set
		$redirected = (\t3lib_div::_GP('sec') || \t3lib_div::_GP('sub') || \t3lib_div::_GP('sec'));
		if ($redirected === false) {

			$currentUrl = $this->currentPageURL();

			// get the base Url based on TypoScript settings
			$configurationManager = $this->objectManager->get(
				'TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface'
			);
			$settings             = $configurationManager->getConfiguration(
				\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
				'tx_herehelp'
			);

			$baseUrl = $settings['plugin.']['tx_herehelp.']['settings.']['baseUrlForRedirect'];

			$baseUrl    = trim($baseUrl, "/ ") . "/";
			$currentUrl = trim($currentUrl, "/ ") . "/";

			$MATCHSUBSECTION = null;
			$MATCHAPP        = null;

			// only redirect if the url match the here extension
			if (strpos($currentUrl, $baseUrl) === 0) {

				$tmp_baseUrl    = $baseUrl;
				$tmp_currentUrl = $currentUrl;
				if (substr($tmp_currentUrl, 0, 4) !== "http") {
					$tmp_currentUrl = "http://" . $tmp_currentUrl;
				}
				if (substr($tmp_baseUrl, 0, 4) !== "http") {
					$tmp_baseUrl = "http://" . $tmp_baseUrl;
				}

				$matchPath = str_replace($tmp_baseUrl, "", $tmp_currentUrl);
				if (strpos($matchPath, '?') !== false) {
					$matchPath = substr($matchPath, 0, strpos($matchPath, '?'));
				}

				$matchPath   = trim($matchPath, " /");
				$MATCHEDPATH = $this->customUrlsRepository->findByPath($matchPath)->getFirst();

				if (isset($MATCHEDPATH)) {
					$MATCHEDPATHID = $MATCHEDPATH->getUID();
					$redirectToUri = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
					$redirectToUri .= $this->currentPageURL(false);

					if (!$_COOKIE['tx_herehelp_ldt']) {
						setcookie('tx_herehelp_ldt', 'true');
						header('Location: ' . $redirectToUri);
						exit;
					}
				} else {
					if (!$_COOKIE['tx_herehelp_ldt']) {
						setcookie('tx_herehelp_ldt', 'true');

						$url = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
						$url .= $this->currentPageURL(false);

						if (substr($url,-1) !== '/') {
							$url = $url . '/';
						}

						header('Location: ' . $url);
						exit;
					}
				}
			}
		}

		// if manual language switcher has been used set the FrontEnd language accordingly
		if (\t3lib_div::_GP('ls')) {
			$GLOBALS['TSFE']->lang = strtolower(\t3lib_div::_GP('ls'));
			$sysLanguages = $this->getSysLanguages();
			foreach ($sysLanguages as $lang) {
				if (strtolower($lang['ISOcode']) === \t3lib_div::_GP('ls')) {
					$GLOBALS['TSFE']->sys_language_uid = $lang['uid'];
				}
			}
			setcookie('tx_herehelp_LS', true, time()+3600);
		}
	}

	/**
	 * Get current page URL
	 *
	 * @param boolean $trimLanguagePath wheather remove 'de/','fr/', etc. from path or not
	 *
	 * @return string
	 */
	private function currentPageURL($trimLanguagePath = true) {
		//$pageURL = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$pageURL = $_SERVER['SERVER_PORT'] != '80' ?
			$_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"]
			: $_SERVER['SERVER_NAME'];

		$languageUID = $this->getCurrentLanguageUID();
		$requestPath = $_SERVER['REQUEST_URI'];
		$requestPath = trim($requestPath,"/");

		$langPath = $this->getCurrentLanguagePath();
		$currentLanguage = null;

		if(!(substr($requestPath,0,3) === $langPath . "/")) {
			$availableLanguages = $this->getSysLanguages();
			$acceptedLanguages  = $this->getAcceptedLanguages();
			$matched = false;
			for($j = 0; $j < count($acceptedLanguages); $j++) {
				if($matched === true) continue;
				$currentLanguage = array_values($acceptedLanguages);
				$currentLanguage = explode(',',$currentLanguage[$j]);
				$currentLanguage = $currentLanguage[$j];

				foreach ($availableLanguages as $avLang) {
					if($matched === true) continue;
					if (strtolower($avLang['ISOcode']) === $currentLanguage) {
						// we got a match!
						$requestPath = $currentLanguage . '/' . $requestPath;
						$matched = true;
					}
				}
			}
		}

		if(true === $trimLanguagePath) {
			$langPath = $this->getCurrentLanguagePath();

			if(isset($currentLanguage)) {
				$langPath = $currentLanguage;
			}

			// only remove if the beginning of the request path matches the current url
			if(substr($requestPath,0,3) === $langPath . "/") {
				$requestPath = "/" . trim(substr($requestPath,3),"/ ");
			}
		} else {
			$requestPath = "/" . $requestPath;
		}
		$pageURL .= $requestPath;

		return $pageURL;
	}

	/**
	 * return the current language uid
	 *
	 * @return mixed
	 */
	private function getCurrentLanguageUID() {
		return $GLOBALS["TSFE"]->sys_language_uid;
	}

	/**
	 * return the current language path
	 *
	 * @return mixed
	 */
	private function getCurrentLanguagePath() {
		return $GLOBALS["TSFE"]->lang;
	}

	/**
	 * Returns the preferred languages ("accepted languages") from the visitor's
	 * browser settings.
	 *
	 * @return	array	An array containing the accepted languages; key and value = iso code, sorted by quality
	 */
	private function getAcceptedLanguages () {
		$languagesArr = array ();
		$rawAcceptedLanguagesArr = explode (',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

		foreach($rawAcceptedLanguagesArr as $languageAndQualityStr) {
			list($languageCode, $quality) = explode(';', $languageAndQualityStr);
			$languageCode = str_replace("_","-",$languageCode);

			if(stripos($languageCode,'-') !== false) {
				list($languageCode,$countryCode) = explode("-",$languageCode);
			}
			$acceptedLanguagesArr[$languageCode] = $quality ? (float)substr ($quality,2) : (float)1;
		}

		// Now sort the accepted languages by their quality and create an array containing only the language codes in the correct order.
		if (is_array ($acceptedLanguagesArr)) {
			arsort ($acceptedLanguagesArr);
			$languageCodesArr = array_keys($acceptedLanguagesArr);
			if (is_array($languageCodesArr)) {
				foreach ($languageCodesArr as $languageCode) {
					$languagesArr[$languageCode] = $languageCode;
				}
			}
		}

		return $languagesArr;
	}

	/**
	 * Returns an array of sys_language records containing the ISO code as the key and the record's uid as the value
	 *
	 * @return	array	sys_language records: ISO code => uid of sys_language record
	 */
	private function getSysLanguages() {

		$availableLanguages = array();

		$sys_languages = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'sys_language', '');
		foreach ($sys_languages as $row) {
			$languageIconTitles[$row['uid']] = $row;
			if ($row['static_lang_isocode'] && \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('static_info_tables')) {
				$staticLangRow = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('static_languages', $row['static_lang_isocode'], 'lg_iso_2');
				if ($staticLangRow['lg_iso_2']) {
					$languageIconTitles[$row['uid']]['ISOcode'] = $staticLangRow['lg_iso_2'];
				}
			}
			if (strlen($row['flag'])) {
				$languageIconTitles[$row['uid']]['flagIcon'] = \TYPO3\CMS\Backend\Utility\IconUtility::mapRecordTypeToSpriteIconName('sys_language', $row);
			}
		}

		return $languageIconTitles;
	}

}
