<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$TCA['tx_herehelp_domain_model_faqs'] = array(
	'ctrl' => $TCA['tx_herehelp_domain_model_faqs']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, headline, title, paths, transitdb, questions',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, headline, title, transitdb, questions,--div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_herehelp_domain_model_faqs',
				'foreign_table_where' => 'AND tx_herehelp_domain_model_faqs.pid=###CURRENT_PID### AND tx_herehelp_domain_model_faqs.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'headline' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:here_help/Resources/Private/Language/locallang_db.xlf:tx_herehelp_domain_model_faqs.headline',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'title' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:here_help/Resources/Private/Language/locallang_db.xlf:tx_herehelp_domain_model_faqs.title',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'transitdb' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:here_help/Resources/Private/Language/locallang_db.xlf:tx_herehelp_domain_model_faqs.transitdb',
			'config' => array(
				'type' => 'check',
				'default' => 0
			),
		),
		'questions' => array(
			'exclude' => 0,
			'label' => 'LLL:EXT:here_help/Resources/Private/Language/locallang_db.xlf:tx_herehelp_domain_model_faqs.questions',
			'config' => array(
				'type' => 'inline',
				'foreign_table' => 'tx_herehelp_domain_model_questions',
				'foreign_field' => 'faqs',
				'maxitems'      => 9999,
				'appearance' => array(
					'collapseAll' => 1,
					'levelLinksPosition' => 'top',
					'showSynchronizationLink' => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink' => 1,
					'useSortable' => 1
				),
			),
		),
		'sections' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'subsections' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
		'apps' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),
			'paths' => array(
				'exclude' => 0,
				'label' => 'LLL:EXT:here_help/Resources/Private/Language/locallang_db.xlf:tx_herehelp_domain_model_customurls.paths',
				'config' => array(
					'type' => 'select',
					'foreign_table' => 'tx_herehelp_domain_model_customurls',
					'size' => 1,
					'minitems' => 0,
					'maxitems' => 1
				),
			),
	),
);

?>