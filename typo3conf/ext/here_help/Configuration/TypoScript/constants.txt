plugin.tx_herehelp {
	view {
		# cat=plugin.tx_herehelp/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:here_help/Resources/Private/Templates/
		# cat=plugin.tx_herehelp/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:here_help/Resources/Private/Partials/
		# cat=plugin.tx_herehelp/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:here_help/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_herehelp//a; type=string; label=Default storage PID
		storagePid = 11
	}
}