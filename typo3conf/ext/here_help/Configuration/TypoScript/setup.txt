plugin.tx_herehelp {
	view {
		templateRootPath = {$plugin.tx_herehelp.view.templateRootPath}
		partialRootPath = {$plugin.tx_herehelp.view.partialRootPath}
		layoutRootPath = {$plugin.tx_herehelp.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_herehelp.persistence.storagePid}
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
	}
	settings {
		imagesPath = uploads/tx_herehelp/
		baseUrlForRedirect = here.bald-live.de/help/
		omniture {
			scripturl = //tags.tiqcdn.com/utag/here/heresupport/dev/utag.js
			namespace = supporthere
		}
	}
	mvc.callDefaultActionIfActionCantBeResolved = 1
}

plugin.tx_herehelp._CSS_DEFAULT_STYLE (
)
