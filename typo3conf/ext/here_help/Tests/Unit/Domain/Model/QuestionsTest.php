<?php

namespace Meelogic\HereHelp\Tests;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Meelogic\HereHelp\Domain\Model\Questions.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Help & FAQ
 *
 * @author Oliver Wand <oliver.wand@meelogic.com>
 */
class QuestionsTest extends \TYPO3\CMS\Extbase\Tests\Unit\BaseTestCase {
	/**
	 * @var \Meelogic\HereHelp\Domain\Model\Questions
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new \Meelogic\HereHelp\Domain\Model\Questions();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getQuestionReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setQuestionForStringSetsQuestion() { 
		$this->fixture->setQuestion('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getQuestion()
		);
	}
	
	/**
	 * @test
	 */
	public function getAnswersReturnsInitialValueForAnswers() { }

	/**
	 * @test
	 */
	public function setAnswersForAnswersSetsAnswers() { }
	
}
?>