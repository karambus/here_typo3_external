<?php

namespace Meelogic\HereHelp\Tests;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Meelogic\HereHelp\Domain\Model\Apps.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage Help & FAQ
 *
 * @author Oliver Wand <oliver.wand@meelogic.com>
 */
class AppsTest extends \TYPO3\CMS\Extbase\Tests\Unit\BaseTestCase {
	/**
	 * @var \Meelogic\HereHelp\Domain\Model\Apps
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new \Meelogic\HereHelp\Domain\Model\Apps();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getTitleReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setTitleForStringSetsTitle() { 
		$this->fixture->setTitle('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getTitle()
		);
	}
	
	/**
	 * @test
	 */
	public function getIconReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setIconForStringSetsIcon() { 
		$this->fixture->setIcon('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getIcon()
		);
	}
	
	/**
	 * @test
	 */
	public function getFaqsReturnsInitialValueForFAQs() { 
		$newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$this->assertEquals(
			$newObjectStorage,
			$this->fixture->getFaqs()
		);
	}

	/**
	 * @test
	 */
	public function setFaqsForObjectStorageContainingFAQsSetsFaqs() { 
		$faq = new \Meelogic\HereHelp\Domain\Model\FAQs();
		$objectStorageHoldingExactlyOneFaqs = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$objectStorageHoldingExactlyOneFaqs->attach($faq);
		$this->fixture->setFaqs($objectStorageHoldingExactlyOneFaqs);

		$this->assertSame(
			$objectStorageHoldingExactlyOneFaqs,
			$this->fixture->getFaqs()
		);
	}
	
	/**
	 * @test
	 */
	public function addFaqToObjectStorageHoldingFaqs() {
		$faq = new \Meelogic\HereHelp\Domain\Model\FAQs();
		$objectStorageHoldingExactlyOneFaq = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$objectStorageHoldingExactlyOneFaq->attach($faq);
		$this->fixture->addFaq($faq);

		$this->assertEquals(
			$objectStorageHoldingExactlyOneFaq,
			$this->fixture->getFaqs()
		);
	}

	/**
	 * @test
	 */
	public function removeFaqFromObjectStorageHoldingFaqs() {
		$faq = new \Meelogic\HereHelp\Domain\Model\FAQs();
		$localObjectStorage = new \TYPO3\CMS\Extbase\Persistence\Generic\ObjectStorage();
		$localObjectStorage->attach($faq);
		$localObjectStorage->detach($faq);
		$this->fixture->addFaq($faq);
		$this->fixture->removeFaq($faq);

		$this->assertEquals(
			$localObjectStorage,
			$this->fixture->getFaqs()
		);
	}
	
}
?>