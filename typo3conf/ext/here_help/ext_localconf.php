<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Meelogic.' . $_EXTKEY,
	'Help',
	array(
		'Sections' => 'show, list',
		'Subsections' => 'list, show',
		'Apps' => 'list, show',
		'FAQs' => 'list, show',
		'Questions' => 'list, show',
		'Answers' => 'list, show',
		'Search' => 'search'
	),
	// non-cacheable actions
	array(
		'Sections' => 'show, list',
		'Subsections' => 'list, show',
		'Apps' => 'list, show',
		'FAQs' => 'list, show',
		'Questions' => 'list, show',
		'Answers' => 'list, show',
		'Search' => 'search'
	)
);

/**
 * register cache for extension
 */
if (!is_array($TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['herehelp_cache'])) {
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['herehelp_cache'] = array();
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['herehelp_cache']['frontend'] = 'TYPO3\\CMS\\Core\\Cache\\Frontend\\VariableFrontend';
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['herehelp_cache']['backend'] = 'TYPO3\\CMS\\Core\\Cache\\Backend\\Typo3DatabaseBackend';
  $TYPO3_CONF_VARS['SYS']['caching']['cacheConfigurations']['herehelp_cache']['options']['compression'] = 1;
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][] = '\\Meelogic\\HereHelp\\Hook\ContentPostProc->urlMatching';