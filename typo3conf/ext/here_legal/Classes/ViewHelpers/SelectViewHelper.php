<?php

namespace Meelogic\HereLegal\ViewHelpers;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Class SelectViewHelper
 *
 * @package Meelogic\HereLegal\ViewHelpers
 */

class SelectViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param \string $userLanguage
	 * @param \string $userCountry
	 * @param \string $uri
	 * @param \array $pageMappingData
	 * @return mixed
	 */
	public function render($userLanguage = NULL, $userCountry = NULL, $uri = NULL, $pageMappingData = NULL) {

		$out = '<select name="country" id="country" onchange="document.location.href=this.value">';

		foreach ($pageMappingData as $data) {
			$selected = '';
			if ((strtolower($data['url']) === $userCountry) && (strtolower($data['language']['lg_iso_2']) === $userLanguage)) {
				$selected = 'selected';
			}

			$out .= '<option value="' . strtolower($data['language']['lg_iso_2']) . $uri . "/" . $data['url'] . '" ' . $selected . '>' . $data['country']['name'] . ' - ' . $data['language']['title'] . '</option>';
		}

		$out .= '</select>';

		return $out;
	}
}