<?php

namespace Meelogic\HereLegal\ViewHelpers;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Class SubMenuViewHelper
 *
 * @package Meelogic\HereLegal\ViewHelpers
 */

class SubMenuViewHelper extends \TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper {

	/**
	 * @param \string $uri
	 * @param \string $topUri
	 * @param \array $pageMappingData
	 * @return mixed
	 */
	public function render($uri = NULL, $topUri = NULL, $pageMappingData = NULL) {

		$lastUri = substr($uri, strrpos($uri, "/")+1);
		$out = '<ul role="menu">';
		foreach ($pageMappingData as $data) {
			$class = '';
			$title = '';
			if ($data['url'] === $lastUri) {
				$class = ' class="active-item"';
			}

			$out .= '<li> <a title="' . $data['title'] . '" href="' . $topUri . "/" . $data['url'] . '"' . $class . '>' . $data['title'] . '</a> </li>';
		}

		$out .= '</ul>';

		return $out;
	}
}