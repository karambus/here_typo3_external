<?php
namespace Meelogic\HereLegal\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
     *  (c) 2014 Tomasz Efner <tomasz.efner@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_legal
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SubMenuRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Get defined Names and URLs for children of given page uid
	 *
	 * @param $pageUid
	 * @return array
	 */
	public function getNamesAndUrlsForPageChildren($pageUid) {
		$namesAndUrls = array();

		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('SELECT uid, tx_realurl_pathsegment, title FROM pages WHERE pid=' . $pageUid . ' AND hidden=0 AND deleted=0');

		$children = $query->execute();
		if ($children[0]['uid'] !== NULL) {
			foreach ($children as $child) {
				$nameUrl = array();
				$nameUrl['pid'] = $child['uid'];
				$nameUrl['url'] = $child['tx_realurl_pathsegment'];
				$pageOverlay = $GLOBALS['TSFE']->sys_page->getPageOverlay($nameUrl['pid'], $GLOBALS['TSFE']->sys_language_uid);
				$nameUrl['title'] = $child['title'];
				if ($pageOverlay['title'] !== NULL) {
					$nameUrl['title'] = $pageOverlay['title'];
				}
				$namesAndUrls[] = $nameUrl;
			}
		}
		return $namesAndUrls;
	}

	/**
	 * Returns an PageID of the page where content should be displayed
	 *
	 * @return	\integer uid
	 */
	public function getPageIdOfTopPage($pageid, $excludeLangingPage) {
		// go up until layout == 0 (landing page) and this will be 2 levels below
		$continue = true;
		$retVal = array();
		$retVal['id'] = $pageid;
		$retPath="";
		while ($continue) {
			$query = $this->createQuery();
			$query->getQuerySettings()->setReturnRawQueryResult(true);
			$query->statement('SELECT pid, layout, tx_realurl_pathsegment FROM pages WHERE uid=' . $pageid . ' AND hidden=0 AND deleted=0');
			$page = $query->execute();

			if ($page[0] !== NULL) {
				if ($page[0]['layout'] == "1") {
					$retVal['id'] = $pageid;
					$pageid = $page[0]['pid'];
					$retPath = $retVal['path'];
					$retVal['path'] = $page[0]['tx_realurl_pathsegment'];
				} else {
					if ($excludeLangingPage == false) {
						$retVal['path'] = $page[0]['tx_realurl_pathsegment'] . "/" . $retVal['path'];
					}
					$retVal['fullPath'] = $retVal['path'] . "/" . $retPath;
					$continue = false;
				}
			} else {
				$continue = false;
			}
		}
		return $retVal;
	}

}
?>