<?php
namespace Meelogic\HereLegal\Domain\Repository;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_legal
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SelectRepository extends \TYPO3\CMS\Extbase\Persistence\Repository {

	/**
	 * Get defined countries for given page uid
	 *
	 * @param $pageUid
	 * @return array
	 */
	public function getCountriesForPage($pageUid) {
		$countryNames = array();

		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('SELECT tx_herelegal_countries FROM pages WHERE uid=' . $pageUid . ' AND hidden=0 AND deleted=0');

		$countries = $query->execute();

		if ($countries[0]['tx_herelegal_countries'] !== NULL) {
			$countriesArray = explode(',', $countries[0]['tx_herelegal_countries']);
			$cnt = 0;
			foreach ($countriesArray as $country) {
				$query = $this->createQuery();
				$query->getQuerySettings()->setReturnRawQueryResult(true);
				$query->statement('SELECT cn_short_local,cn_iso_2 FROM static_countries WHERE uid=' . $country);
				$cName = $query->execute();
				$countryNames[$cnt]['name'] = $cName[0]['cn_short_local'];
				$countryNames[$cnt]['iso'] = $cName[0]['cn_iso_2'];
				$cnt++;
			}
		}
		return $countryNames;
	}

	/**
	 * Get available page translations for given page uid
	 *
	 * @param $pageUid
	 * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
	 */
	public function getAvailableTranslationsForPage($pageUid) {
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('SELECT sys_language_uid,
									b.title,
									c.lg_iso_2
							FROM pages_language_overlay AS a
							LEFT JOIN sys_language AS b ON (a.sys_language_uid = b.uid)
							LEFT JOIN static_languages AS c ON (b.static_lang_isocode = c.uid)
							WHERE a.pid=' . $pageUid . ' AND a.deleted=0');

		$translations = $query->execute();

		// add default language to array (if text exists)
		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('SELECT bodytext
							FROM tt_content WHERE pid=' . $pageUid . ' AND deleted=0 AND sys_language_uid=0');
		$defaultLanguage = $query->execute();
		if (($defaultLanguage !== null) AND (count($defaultLanguage) > 0) AND (strlen($defaultLanguage[0]['bodytext']) > 0)) {
			$translations[] = array(
				'sys_language_uid'  => 0,
				'title'             => 'English UK',
				'lg_iso_2'          => 'en'
			);
		};
		return $translations;
	}

		/**
	 * Get defined countries and languages for children of given page uid
	 *
	 * @param $pageUid
	 * @return array
	 */
	public function getCountriesAndLanguagesForPageChildren($pageUid) {
		$countriesAndLanguages = array();

		$query = $this->createQuery();
		$query->getQuerySettings()->setReturnRawQueryResult(true);
		$query->statement('SELECT uid, tx_realurl_pathsegment FROM pages WHERE pid=' . $pageUid . ' AND hidden=0 AND deleted=0');

		$children = $query->execute();
		if ($children[0]['uid'] !== NULL) {
			foreach ($children as $child) {
				$countries = $this->getCountriesForPage($child['uid']);
				$languages = $this->getAvailableTranslationsForPage($child['uid']);
				foreach ($countries as $country) {
				    foreach ($languages as $language) {
						$countryLanguage = array();
						$countryLanguage['pid'] = $child['uid'];
						$countryLanguage['url'] = $child['tx_realurl_pathsegment'];
						$countryLanguage['country'] = $country;
						$countryLanguage['language'] = $language;
						$countriesAndLanguages[] = $countryLanguage;
					}
				}
			}
		}
		usort($countriesAndLanguages, 'self::compare_sorter');
		return $countriesAndLanguages;
	}

	private static function compare_sorter($a, $b) {
		$srtVal = strcmp($a['country']['name'], $b['country']['name']);
		if ($srtVal == 0) {
			$srtVal = strcmp($a['language']['title'], $b['language']['title']);
		}
		return $srtVal;
	}

	/**
	 * Returns an PageID of the page where content should be displayed
	 *
	 * @return	\array page data
	 */
	public function getPageIdOfTopPage($pageid, $excludeLangingPage) {
		// go up until layout == 0 (landing page) and this will be 2 levels below
		$continue = true;
		$retVal0 = array();
		$retVal0['id'] = $pageid;
		$retVal1 = array();
		$retVal1['id'] = $pageid;
		while ($continue) {
			$query = $this->createQuery();
			$query->getQuerySettings()->setReturnRawQueryResult(true);
			$query->statement('SELECT pid, layout, tx_realurl_pathsegment FROM pages WHERE uid=' . $pageid . ' AND hidden=0 AND deleted=0');
			$page = $query->execute();

			if ($page[0] !== NULL) {
				if ($page[0]['layout'] == "1") {
					$retVal0 = $retVal1;
					$retVal1['id'] = $pageid;
					$retVal1['path'] = $page[0]['tx_realurl_pathsegment'];
					$pageid = $page[0]['pid'];
				} else {
					$retVal = "";
					if ($excludeLangingPage === "false") {
						if ($page[0]['tx_realurl_pathsegment'] !== NULL) {
							$retVal = $page[0]['tx_realurl_pathsegment'] . "/";
						}
					}
					if ($retVal1['path'] !== NULL) {
						$retVal = $retVal . $retVal1['path'] . "/";
					}
					if (strlen($retVal) > 0) {
						$retVal0['path'] = $retVal . $retVal0['path'];
					}
					$continue = false;
				}
			} else {
				$continue = false;
			}
		}
		return $retVal0;
	}

}
?>