<?php
namespace Meelogic\HereLegal\Controller;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Tomasz Efner <tomasz.efner@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_legal
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SubMenuController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * submenuRepository
	 *
	 * @var \Meelogic\HereLegal\Domain\Repository\SubMenuRepository
	 * @inject
	 */
	protected $submenuRepository;

	/**
	 * renderAction
	 *
	 * @return void
	 */
	public function renderAction() {

		$excludeLangingPage = $this->settings['baseUrlExcludeLangingPage'];
		$uri                = $this->currentPageURL(true, false);
		$topPage            = $this->submenuRepository->getPageIdOfTopPage($GLOBALS['TSFE']->id, $excludeLangingPage);
		$pageMappingData    = $this->submenuRepository->getNamesAndUrlsForPageChildren($topPage['id']);

		$this->view->assignMultiple(
			array(
				'currentUri'    => $topPage['fullPath'],
				'topUri'        => $this->getCurrentLanguagePath() . "/" . $topPage['path'],
				'pageMappingData'=> $pageMappingData
			)
		);

	}

	/**
	 * Get current page URL
	 *
	 * @param \boolean $trimLanguagePath wheather remove 'de/','fr/', etc. from path or not
	 *
	 * @return string
	 */
	private function currentPageURL($trimLanguagePath = true, $fullPath = true) {
		//$pageURL = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$pageURL = $_SERVER['SERVER_PORT'] != '80' ?
			$_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"]
			: $_SERVER['SERVER_NAME'];

		$languageUID = $this->getCurrentLanguageUID();
		$requestPath = $_SERVER['REQUEST_URI'];
		$requestPath = trim($requestPath,"/");

		$langPath = $this->getCurrentLanguagePath();
		$currentLanguage = null;

		if(!(substr($requestPath,0,3) === $langPath . "/")) {
			$availableLanguages = $this->getSysLanguages();
			$acceptedLanguages  = $this->getAcceptedLanguages();
			$matched = false;
			for($j = 0; $j < count($acceptedLanguages); $j++) {
				if($matched === true) continue;
				$currentLanguage = array_values($acceptedLanguages);
				$currentLanguage = explode(',',$currentLanguage[$j]);
				$currentLanguage = $currentLanguage[$j];

				foreach ($availableLanguages as $avLang) {
					if($matched === true) continue;
					if (strtolower($avLang['ISOcode']) === $currentLanguage) {
						// we got a match!
						$requestPath = $currentLanguage . '/' . $requestPath;
						$matched = true;
					}
				}
			}
		}

		if(true === $trimLanguagePath) {
			$langPath = $this->getCurrentLanguagePath();

			if(isset($currentLanguage)) {
				$langPath = $currentLanguage;
			}

			// only remove if the beginning of the request path matches the current url
			if(substr($requestPath,0,3) === $langPath . "/") {
				$requestPath = "/" . trim(substr($requestPath,3),"/ ");
			}
		} else {
			$requestPath = "/" . $requestPath;
		}
		$pageURL .= $requestPath;

		if ($fullPath) {
			return $pageURL;
		}
		return $requestPath;
	}

	/**
	 * return the current language uid
	 *
	 * @return mixed
	 */
	private function getCurrentLanguageUID() {
		return $GLOBALS["TSFE"]->sys_language_uid;
	}

	/**
	 * return the current language path
	 *
	 * @return mixed
	 */
	private function getCurrentLanguagePath() {
		return $GLOBALS["TSFE"]->lang;
	}

	/**
	 * Returns the preferred languages ("accepted languages") from the visitor's
	 * browser settings.
	 *
	 * @return	\array	An array containing the accepted languages; key and value = iso code, sorted by quality
	 */
	private function getAcceptedLanguages () {
		$languagesArr = array ();
		$rawAcceptedLanguagesArr = explode (',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

		foreach($rawAcceptedLanguagesArr as $languageAndQualityStr) {
			list($languageCode, $quality) = explode(';', $languageAndQualityStr);
			$languageCode = str_replace("_","-",$languageCode);

			if(stripos($languageCode,'-') !== false) {
				list($languageCode,$countryCode) = explode("-",$languageCode);
			}
			$acceptedLanguagesArr[$languageCode] = $quality ? (float)substr ($quality,2) : (float)1;
		}

		// Now sort the accepted languages by their quality and create an array containing only the language codes in the correct order.
		if (is_array ($acceptedLanguagesArr)) {
			arsort ($acceptedLanguagesArr);
			$languageCodesArr = array_keys($acceptedLanguagesArr);
			if (is_array($languageCodesArr)) {
				foreach ($languageCodesArr as $languageCode) {
					$languagesArr[$languageCode] = $languageCode;
				}
			}
		}

		return $languagesArr;
	}

	/**
	 * Returns an array of sys_language records containing the ISO code as the key and the record's uid as the value
	 *
	 * @return	\array	sys_language records: ISO code => uid of sys_language record
	 */
	private function getSysLanguages() {

		$availableLanguages = array();

		$sys_languages = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'sys_language', '');
		foreach ($sys_languages as $row) {
			$languageIconTitles[$row['uid']] = $row;
			if ($row['static_lang_isocode'] && \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('static_info_tables')) {
				$staticLangRow = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('static_languages', $row['static_lang_isocode'], 'lg_iso_2');
				if ($staticLangRow['lg_iso_2']) {
					$languageIconTitles[$row['uid']]['ISOcode'] = $staticLangRow['lg_iso_2'];
				}
			}
			if (strlen($row['flag'])) {
				$languageIconTitles[$row['uid']]['flagIcon'] = \TYPO3\CMS\Backend\Utility\IconUtility::mapRecordTypeToSpriteIconName('sys_language', $row);
			}
		}

		return $languageIconTitles;
	}
}

?>