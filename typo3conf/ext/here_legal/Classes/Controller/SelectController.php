<?php
namespace Meelogic\HereLegal\Controller;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic AG
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 *
 *
 * @package here_legal
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class SelectController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * selectRepository
	 *
	 * @var \Meelogic\HereLegal\Domain\Repository\SelectRepository
	 * @inject
	 */
	protected $selectRepository;

	/**
	 * renderAction
	 *
	 * @return void
	 */
	public function renderAction() {

		$excludeLangingPage = $this->settings['baseUrlExcludeLangingPage'];
		$uri                = $this->currentPageURL(false, false);
		$topPage            = $this->selectRepository->getPageIdOfTopPage($GLOBALS['TSFE']->id, $excludeLangingPage);
		$pageMappingData    = $this->selectRepository->getCountriesAndLanguagesForPageChildren($topPage['id']);

		$urlCountry         = $this->getCountryFromUri($uri, $topPage);
		$urlLanguage        = $this->getLanguageFromUri($uri, $topPage);

		// check if we already have country set in the url
		if ($urlCountry === null) {
			// country is not set
			$acceptedLanguages = $this->getAcceptedLanguages();
			if ($acceptedLanguages !== null) {
				$matched = false;
				$availableLanguages = $this->getSysLanguages();
				for($j = 0; $j < count($acceptedLanguages); $j++) {
					if($matched === true) continue;
					$currentLanguage = array_values($acceptedLanguages);
					$currentLanguage = $currentLanguage[$j];
					foreach ($availableLanguages as $avLang) {
						if($matched === true) continue;
						if (strtolower($avLang['ISOcode']) === $currentLanguage) {
							// we got a match!
							$urlLanguage = $currentLanguage;
							$matched = true;
						}
					}
				}
				if ($matched === false) {
					$urlLanguage = "en";
				}
			}
			$urlCtryLng = null;
			$urlCtry = null;
			$urlLng = null;
			$urlEnGB = null;
			$urlExisting = null;
			$userCountryCode    = $this->ipLookup();
			
			// check if there is a page in specified country and specified language, if yes then redirect to that page
			// if not then try to find some page in specified country and redirect to that page
			// if not possible then use english/uk
			// if not possible then any existing page
			foreach($pageMappingData as $mappingData) {
				if (strtolower($mappingData['country']['iso']) === $userCountryCode) {
					if (strtolower($mappingData['language']['lg_iso_2']) === $urlLanguage) {
						$urlCtryLng = strtolower($mappingData['language']['lg_iso_2']) . "/" . $topPage['path'] . "/" . $mappingData['url'];
					}
					$urlCtry = $topPage['path'] . "/" . $mappingData['url'];
				}
				if (strtolower($mappingData['language']['lg_iso_2']) === $urlLanguage) {
					$urlLng = strtolower($mappingData['language']['lg_iso_2']) . "/" . $topPage['path'] . "/" . $mappingData['url'];
				}
				if (strtolower($mappingData['country']['iso']) === 'gb') {
					if (strtolower($mappingData['language']['lg_iso_2']) === 'en') {
						$urlEnGB = $topPage['path'] . "/" . $mappingData['url'];
					}
				}
				if ($urlExisting === null) {
					$urlExisting = strtolower($mappingData['language']['lg_iso_2']) . "/" . $topPage['path'] . "/" . $mappingData['url'];
				}
			}
			/*
			var_dump('userCountryCode: ' . $userCountryCode);
			var_dump('urlCtryLng: ' . $urlCtryLng);
			var_dump('urlCtry: ' . $urlCtry);
			var_dump('urlLng: ' . $urlLng);
			var_dump('urlEnGB: ' . $urlEnGB);
			var_dump('urlExisting: ' . $urlExisting);
			var_dump($pageMappingData);
			*/
			if ($urlCtryLng !== null) {
				$this->doRedirect($uri['host'] . '/' . $urlCtryLng);
			} else {
				if ($urlCtry !== null) {
					$this->doRedirect($uri['host'] . '/' . $urlCtry);
				} else {
					if ($urlLng !== null) {
						$this->doRedirect($uri['host'] . '/' . $urlLng);
					} else {
						if ($urlEnGB !== null) {
							$this->doRedirect($uri['host'] . '/' . $urlEnGB);
						} else {
							$this->doRedirect($uri['host'] . '/' . $urlExisting);
						}
					}
				}
			}
		} else {
			// country is set
			// check if there is country/language combination
			//	var_dump("Country is set");
			$urlOK = false;
			$urlCtryLng = null;
			$urlEnGB = null;
			$urlExisting = null;
			if ($urlLanguage === null Or $urlLanguage ==="") {
				// default is EN
				$urlLanguage = 'en';
			}
			
			foreach($pageMappingData as $mappingData) {
				if (strtolower($mappingData['url']) === $urlCountry) {
					if (strtolower($mappingData['language']['lg_iso_2']) === $urlLanguage) {
						$urlOK = true;
					}
					$urlCtryLng = strtolower($mappingData['language']['lg_iso_2']) . "/" . $topPage['path'] . "/" . $mappingData['url'];
				}
				if (strtolower($mappingData['country']['iso']) === 'gb') {
					if (strtolower($mappingData['language']['lg_iso_2']) === 'en') {
						$urlEnGB = $topPage['path'] . "/" . $mappingData['url'];
					}
				}
				if ($urlExisting === null) {
					$urlExisting = strtolower($mappingData['language']['lg_iso_2']) . "/" . $topPage['path'] . "/" . $mappingData['url'];
				}
			}
			/*
			var_dump($urlOK);
			var_dump('urlCountry: ' . $urlCountry);
			var_dump('urlCtryLng: ' . $urlCtryLng);
			var_dump('urlEnGB: ' . $urlEnGB);
			var_dump('urlExisting: ' . $urlExisting);
			*/
			if ($urlOK) {
				$userCountryCode = $urlCountry;
			} else {
				// redirect to correct path
				if ($urlCtryLng !== null) {
					$this->doRedirect($uri['host'] . '/' . $urlCtryLng);
				} else {
					if ($urlEnGB !== null) {
						$this->doRedirect($uri['host'] . '/' . $urlEnGB);
					} else {
						$this->doRedirect($uri['host'] . '/' . $urlExisting);
					}
				}
			}
		}
		if ($topPage['path'][0] !== "/") {
			$topPage['path'] = "/" . $topPage['path'];
		}
		$this->view->assignMultiple(
			array(
				'userLanguage'  => $GLOBALS['TSFE']->lang,
				'userCountry'   => $userCountryCode,
				'currentUri'    => $topPage['path'],
				'pageMappingData'=> $pageMappingData
			)
		);
	}

	/**
	 * Perform redirect to URI
	 *
	 * @param \string $url
	 */
	private function doRedirect($urlInput) {
		$url = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$url.= $urlInput;

		if (substr($url,-1) !== '/') {
			$url = $url . '/';
		}
//		var_dump("Redirect to: " . $url);
		header('Location: ' . $url);
		exit;
		
	}

	/**
	 * Returns an country from URL
	 *
	 * Returns an country from URI, otherwise null
	 *
	 * @param \array $uri
	 * @param \array $topPage
	 */
	private function getCountryFromUri($uri, $topPage) {
		if ($uri['requestPath'] !== $topPage['path']) {
			$uriStart = strpos($uri['requestPath'], $topPage['path']);
			if ($uriStart !== false) {
				$uriPart = split("/", substr($uri['requestPath'], $uriStart + strlen($topPage['path']) + 1));
				if ($uriPart[0] === '') {
					return null;
				}
			} else {
				$uriPart = split("/", substr($uri['requestPath'], strlen($topPage['path']) + 1));
			}
			return $uriPart[0];
		}
		return null;
	}

	/**
	 * Returns an language from URL
	 *
	 * Returns an language from URI, otherwise null
	 *
	 * @param \array $uri
	 * @param \array $topPage
	 */
	private function getLanguageFromUri($uri, $topPage) {
		if ($uri['requestPath'] !== $uri['original']) {
			return substr($uri['original'], 0, strlen($uri['original']) - strlen($uri['requestPath']) - 1);
		}
		return null;
	}
	
	/**
	 * Perform the ip lookup through here's internal API
	 *
	 * Returns ISO2 country code
	 *
	 * @param \string $ip
	 * @return \string $countryCode
	 */
	private function ipLookup() {

		$url = $this->settings['ipdetection_endpoint'] . $this->getIPAddress();

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_USERPWD, $this->settings['ipdetection_username'] . ':' . $this->settings['ipdetection_password']);
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		$curl_response = curl_exec($curl);
		curl_close($curl);
		return json_decode($curl_response)->iso2CountryCode;
	}

	/**
	 * Get user's IP address
	 *
	 * @return \string $ip
	 */
	private function getIPAddress() {
		$ip = filter_input(INPUT_SERVER, 'REMOTE_ADDR');

		// in case this is called from local dev server
		if ($ip === '127.0.0.1') {
			$ip = '84.159.194.110';
		}
		if ($ip === '192.168.56.1') {
			$ip = '212.77.100.101';
		}

		return $ip;
	}

	/**
	 * Get current page URL
	 *
	 * @return string
	 */
	private function currentPageURL() {
		//$pageURL = $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://';
		$result = array();
		$pageURL = $_SERVER['SERVER_PORT'] != '80' ?
			$_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"]
			: $_SERVER['SERVER_NAME'];

		$languageUID = $this->getCurrentLanguageUID();
		$requestPath = $_SERVER['REQUEST_URI'];
		$requestPath = trim($requestPath,"/");

		$result['original'] = $requestPath;

		$langPath = $this->getCurrentLanguagePath();
		$currentLanguage = null;

		if(!(substr($requestPath,0,3) === $langPath . "/")) {
			$availableLanguages = $this->getSysLanguages();
			$acceptedLanguages  = $this->getAcceptedLanguages();
			$matched = false;
			for($j = 0; $j < count($acceptedLanguages); $j++) {
				if($matched === true) continue;
				$currentLanguage = array_values($acceptedLanguages);
				$currentLanguage = explode(',',$currentLanguage[$j]);
				$currentLanguage = $currentLanguage[$j];

				foreach ($availableLanguages as $avLang) {
					if($matched === true) continue;
					if (strtolower($avLang['ISOcode']) === $currentLanguage) {
						// we got a match!
						$requestPath = $currentLanguage . '/' . $requestPath;
						$matched = true;
					}
				}
			}
		}

		$result['host'] = $pageURL;
		$result['pathWithLang'] = $pageURL . '/' . $requestPath;
		$result['requestPathWithLang'] = $requestPath;
		
		$langPath = $this->getCurrentLanguagePath();

		if(isset($currentLanguage)) {
			$langPath = $currentLanguage;
		}

		// only remove if the beginning of the request path matches the current url
		if(substr($requestPath,0,3) === $langPath . "/") {
			$requestPath = trim(substr($requestPath,3),"/ ");
		}
		$result['path'] = $pageURL . '/' . $requestPath;
		$result['requestPath'] = $requestPath;
		return $result;
	}

	/**
	 * return the current language uid
	 *
	 * @return mixed
	 */
	private function getCurrentLanguageUID() {
		return $GLOBALS["TSFE"]->sys_language_uid;
	}

	/**
	 * return the current language path
	 *
	 * @return mixed
	 */
	private function getCurrentLanguagePath() {
		return $GLOBALS["TSFE"]->lang;
	}

	/**
	 * Returns the preferred languages ("accepted languages") from the visitor's
	 * browser settings.
	 *
	 * @return	\array	An array containing the accepted languages; key and value = iso code, sorted by quality
	 */
	private function getAcceptedLanguages () {
		$languagesArr = array ();
		$rawAcceptedLanguagesArr = explode (',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);

		foreach($rawAcceptedLanguagesArr as $languageAndQualityStr) {
			list($languageCode, $quality) = explode(';', $languageAndQualityStr);
			$languageCode = str_replace("_","-",$languageCode);

			if(stripos($languageCode,'-') !== false) {
				list($languageCode,$countryCode) = explode("-",$languageCode);
			}
			$acceptedLanguagesArr[$languageCode] = $quality ? (float)substr ($quality,2) : (float)1;
		}

		// Now sort the accepted languages by their quality and create an array containing only the language codes in the correct order.
		if (is_array ($acceptedLanguagesArr)) {
			arsort ($acceptedLanguagesArr);
			$languageCodesArr = array_keys($acceptedLanguagesArr);
			if (is_array($languageCodesArr)) {
				foreach ($languageCodesArr as $languageCode) {
					$languagesArr[$languageCode] = $languageCode;
				}
			}
		}

		return $languagesArr;
	}

	/**
	 * Returns an array of sys_language records containing the ISO code as the key and the record's uid as the value
	 *
	 * @return	\array	sys_language records: ISO code => uid of sys_language record
	 */
	private function getSysLanguages() {

		$availableLanguages = array();

		$sys_languages = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('*', 'sys_language', '');
		foreach ($sys_languages as $row) {
			$languageIconTitles[$row['uid']] = $row;
			if ($row['static_lang_isocode'] && \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('static_info_tables')) {
				$staticLangRow = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord('static_languages', $row['static_lang_isocode'], 'lg_iso_2');
				if ($staticLangRow['lg_iso_2']) {
					$languageIconTitles[$row['uid']]['ISOcode'] = $staticLangRow['lg_iso_2'];
				}
			}
			if (strlen($row['flag'])) {
				$languageIconTitles[$row['uid']]['flagIcon'] = \TYPO3\CMS\Backend\Utility\IconUtility::mapRecordTypeToSpriteIconName('sys_language', $row);
			}
		}

		return $languageIconTitles;
	}
}

?>