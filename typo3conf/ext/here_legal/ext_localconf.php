<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Meelogic.' . $_EXTKEY,
	'Legal',
	array(
		'Select' => 'render',
		'SubMenu' => 'render',
	),
	// non-cacheable actions
	array(
		'Select' => 'render',
		'SubMenu' => 'render',
	)
);

?>