<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	$_EXTKEY,
	'Legal',
	'Legal'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Legal');

$tempColumns = array(
	'tx_herelegal_countries' => array(
		'exclude' => 0,
		'label' => 'LLL:EXT:here_legal/Resources/Private/Language/locallang_db.xlf:tx_herelegal_pages_countries',
		'config' => array(
			'type' => 'select',
			'size' => '10',
			'minitems' => 0,
			'maxitems' => 999999,
			'foreign_table' => 'static_countries',
			'foreign_table_where' => ' ORDER BY cn_short_en ASC'
		)
	)
);

\t3lib_div::loadTCA('pages');
\t3lib_extMgm::addTCAcolumns('pages', $tempColumns, 1);

// add the new fields to a new tab called "Legal"
\t3lib_extMgm::addToAllTCAtypes('pages','--div--;LLL:EXT:here_legal/Resources/Private/Language/locallang_db.xlf:pages.div.herelegal,tx_herelegal_countries');

?>