plugin.tx_herelegal {
	view {
		# cat=plugin.tx_herelegal/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:here_legal/Resources/Private/Templates/
		# cat=plugin.tx_herelegal/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:here_legal/Resources/Private/Partials/
		# cat=plugin.tx_herelegal/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:here_legal/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_herelegal//a; type=string; label=Default storage PID
		storagePid =
	}
}