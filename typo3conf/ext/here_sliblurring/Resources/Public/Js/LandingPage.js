$(function() {


/*
  $(':input[placeholder]').on({
    // Hide placeholder text when search input gets focus.
    focus: function () {
      $(this).data('placeholder', $(this).attr('placeholder'));
      $(this).attr('placeholder', null);
    },
    // Show when it lose focus and has no text inside.
    blur: function () {
      $(this).attr('placeholder', $(this).data('placeholder'));
    }
  });

  var form   = $('form'),
      radios = form.find(':radio'),
      notify = $('#notifyme'),
      reset  = form.find(':reset'),
      submit = form.find(':submit'),
      hide   = function(context, hide) {
        hide = hide === undefined ? true : hide;
        context.each(function() {
          form.find('[rel="' + $(this).attr('id') + '"]').toggleClass('hidden', hide).find(':input').prop('disabled', hide);
        });
      },
      validate = function() {
        var valid = true,
            radio = radios.filter(':checked')
        ;

        // validate radio and textarea
        if (radio.length === 0) {
          valid = false;
        } else {
          var field = form.find('[rel="' + radio.attr('id') + '"] :input');
          if (field.length && !field.val().trim()) {
            valid = false;
          }
        }

        // validate email
        if (notify.is(':checked')) {
          var email = form.find('[rel="' + notify.attr('id') + '"] :input').val().trim(),
              regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          ;
          if (!regex.test(email)) {
            valid = false;
          }
        }

        submit.trigger('disable', !valid);
      }
  ;

  radios.on('change', function() {
    hide(radios);
    hide($(this), false);
  });

  notify.on('change', function() {
    hide($(this), !$(this).is(':checked'));
  });

  reset.on('click', function () {
    hide(radios);
    hide(notify);
  });

  submit.on('disable', function(e, disable) {
    disable = disable === undefined ? true : disable;
    $(this).prop('disabled', disable).toggleClass('btn-primary', !disable).toggleClass('btn-disabled', disable);
  }).trigger('disable');

  // validate
  form.find(':input:not([type=hidden]):not(:submit):not(:reset)').on('click keyup focus blur change', validate);
*/
});
