$('.submit').prop('disabled', true);
$('.submit').css({
   'background' : '#f7f7f7',
    'color'     : '#c6c6c6',
    'border'    : '1px solid #f7f7f7'
});

$('#nomatch, #viewobstructed, #poorquality').click(function() {
   $('.submit').prop('disabled', false);
   $('.submit').css({
       'background' : '#00b4e5',
       'color'     : '#ffffff',
       'border'    : '1px solid #00b4e5'
   });
});

$('input[type=checkbox]').change(function() {
    console.log('clicked');
   if ($(this).prop("checked")) {
       $('#textemail').show();
       return;
   }
   $('#textemail').hide();
});

$('.email').keyup(function() {
    val = $(this).val().trim();
    if(val.length > 0) {
        $('.submit').prop('disabled', false);
        $('.submit').css({
            'background' : '#00b4e5',
            'color'     : '#ffffff',
            'border'    : '1px solid #00b4e5'
        });
    }
});