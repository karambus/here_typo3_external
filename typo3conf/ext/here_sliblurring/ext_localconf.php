<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Meelogic.' . $_EXTKEY,
	'Sliblurring',
	array(
		'Blurring' => 'new, create, success, error',
		'ImageQuality' => 'new, create, success, error',
		'LandingPage' => 'new, create, success, error, blurring, imagequality'

	),
	// non-cacheable actions
	array(
		'Blurring' => 'new, create, success, error',
		'ImageQuality' => 'new, create, success, error',
		'LandingPage' => 'new, create, success, error, blurring, imagequality'

	)
);

?>