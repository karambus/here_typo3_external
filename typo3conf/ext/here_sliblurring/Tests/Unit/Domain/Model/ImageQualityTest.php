<?php

namespace Meelogic\HereSliblurring\Tests;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic Consulting AG
 *  			
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test case for class \Meelogic\HereSliblurring\Domain\Model\ImageQuality.
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @package TYPO3
 * @subpackage here SLI blurring reporting tool
 *
 * @author Oliver Wand <oliver.wand@meelogic.com>
 */
class ImageQualityTest extends \TYPO3\CMS\Extbase\Tests\Unit\BaseTestCase {
	/**
	 * @var \Meelogic\HereSliblurring\Domain\Model\ImageQuality
	 */
	protected $fixture;

	public function setUp() {
		$this->fixture = new \Meelogic\HereSliblurring\Domain\Model\ImageQuality();
	}

	public function tearDown() {
		unset($this->fixture);
	}

	/**
	 * @test
	 */
	public function getNomatchReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getNomatch()
		);
	}

	/**
	 * @test
	 */
	public function setNomatchForIntegerSetsNomatch() { 
		$this->fixture->setNomatch(12);

		$this->assertSame(
			12,
			$this->fixture->getNomatch()
		);
	}
	
	/**
	 * @test
	 */
	public function getObstructedviewReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getObstructedview()
		);
	}

	/**
	 * @test
	 */
	public function setObstructedviewForIntegerSetsObstructedview() { 
		$this->fixture->setObstructedview(12);

		$this->assertSame(
			12,
			$this->fixture->getObstructedview()
		);
	}
	
	/**
	 * @test
	 */
	public function getPoorqualityReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getPoorquality()
		);
	}

	/**
	 * @test
	 */
	public function setPoorqualityForIntegerSetsPoorquality() { 
		$this->fixture->setPoorquality(12);

		$this->assertSame(
			12,
			$this->fixture->getPoorquality()
		);
	}
	
	/**
	 * @test
	 */
	public function getNotifymeReturnsInitialValueForInteger() { 
		$this->assertSame(
			0,
			$this->fixture->getNotifyme()
		);
	}

	/**
	 * @test
	 */
	public function setNotifymeForIntegerSetsNotifyme() { 
		$this->fixture->setNotifyme(12);

		$this->assertSame(
			12,
			$this->fixture->getNotifyme()
		);
	}
	
	/**
	 * @test
	 */
	public function getEmailReturnsInitialValueForString() { }

	/**
	 * @test
	 */
	public function setEmailForStringSetsEmail() { 
		$this->fixture->setEmail('Conceived at T3CON10');

		$this->assertSame(
			'Conceived at T3CON10',
			$this->fixture->getEmail()
		);
	}
	
}
?>