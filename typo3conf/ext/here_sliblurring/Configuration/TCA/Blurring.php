<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$GLOBALS['TCA']['tx_heresliblurring_domain_model_blurring'] = array(
	'ctrl' => $GLOBALS['TCA']['tx_heresliblurring_domain_model_blurring']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, notifyme, email, message, whattoblur, panoramaid, latitude, longitude, heading, tilt, appId, appCode',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, notifyme, email, message, whattoblur, panoramaid, latitude, longitude, heading, tilt, appId, appCode, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => ''),
	),
	'columns' => array(

		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xlf:LGL.default_value', 0)
				),
			),
		),
		'l10n_parent' => array(
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => array(
				'type' => 'select',
				'items' => array(
					array('', 0),
				),
				'foreign_table' => 'tx_heresliblurring_domain_model_blurring',
				'foreign_table_where' => 'AND tx_heresliblurring_domain_model_blurring.pid=###CURRENT_PID### AND tx_heresliblurring_domain_model_blurring.sys_language_uid IN (-1,0)',
			),
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough',
			),
		),

		't3ver_label' => array(
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			)
		),

		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config' => array(
				'type' => 'check',
			),
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config' => array(
				'type' => 'input',
				'size' => 13,
				'max' => 20,
				'eval' => 'datetime',
				'checkbox' => 0,
				'default' => 0,
				'range' => array(
					'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
				),
			),
		),

		'notifyme' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.notifyme',
			'config' => array(
				'type' => 'check',
			)
		),
		'email' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.email',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),
		'message' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.message',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
		'whattoblur' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.whattoblur',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim,required'
			)
		),
			'panoramaid' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.panoramaid',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim'
				)
			),
			'latitude' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.latitude',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim'
				)
			),
			'longitude' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.longitude',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim'
				)
			),
			'heading' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.heading',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim'
				)
			),
			'tilt' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.tilt',
			'config' => array(
				'type' => 'text',
				'cols' => 40,
				'rows' => 15,
				'eval' => 'trim'
			)
		),
			'appId' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.appId',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim'
				)
			),
			'appCode' => array(
				'exclude' => 1,
				'label' => 'LLL:EXT:here_sliblurring/Resources/Private/Language/locallang_db.xlf:tx_heresliblurring_domain_model_blurring.appCode',
				'config' => array(
					'type' => 'text',
					'cols' => 40,
					'rows' => 15,
					'eval' => 'trim,required'
				)
			),

	),
);
