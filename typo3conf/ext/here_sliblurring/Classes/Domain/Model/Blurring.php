<?php
namespace Meelogic\HereSliblurring\Domain\Model;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic Consulting AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Blurring
 */
class Blurring extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * notifyme
	 *
	 * @var integer
	 */
	protected $notifyme = 0;

	/**
	 * email
	 *
	 * @var string
	 */
	protected $email = '';

	/**
	 * message
	 *
	 * @var string
	 */
	protected $message = '';

	/**
	 * whattoblur
	 *
	 * @var string
	 */
	protected $whattoblur = '';

	/**
	 * panoramaid
	 *
	 * @var string
	 */
	protected $panoramaid = '';

	/**
	 * latitude
	 *
	 * @var string
	 */
	protected $latitude = '';

	/**
	 * longitude
	 *
	 * @var string
	 */
	protected $longitude = '';

	/**
	 * azimuth
	 *
	 * @var string
	 */
	protected $azimuth = '';

	/**
	 * polar
	 *
	 * @var string
	 */
	protected $polar = '';

	/**
	 * appId
	 *
	 * @var string
	 */
	protected $appId;

	/**
	 * appCode
	 *
	 * @var string
	 */
	protected $appCode;

	/**
	 * Returns the notifyme
	 *
	 * @return integer $notifyme
	 */
	public function getNotifyme() {
		return $this->notifyme;
	}

	/**
	 * Sets the notifyme
	 *
	 * @param integer $notifyme
	 * @return void
	 */
	public function setNotifyme($notifyme) {
		$this->notifyme = $notifyme;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the message
	 *
	 * @return string $message
	 */
	public function getMessage() {
		return $this->message;
	}

	/**
	 * Sets the message
	 *
	 * @param string $message
	 * @return void
	 */
	public function setMessage($message) {
		$this->message = $message;
	}

	/**
	 * Returns the whattoblur
	 *
	 * @return string $whattoblur
	 */
	public function getWhattoblur() {
		return $this->whattoblur;
	}

	/**
	 * Sets the whattoblur
	 *
	 * @param string $whattoblur
	 * @return void
	 */
	public function setWhattoblur($whattoblur) {
		$this->whattoblur = $whattoblur;
	}

	/**
	 * @return string
	 */
	public function getPanoramaid() {
		return $this->panoramaid;
	}

	/**
	 * @param $panoramaid
	 * @return void
	 */
	public function setPanoramaid($panoramaid) {
		$this->panoramaid = $panoramaid;
	}

	/**
	 * @return string
	 */
	public function getLatitude() {
		return $this->latitude;
	}

	/**
	 * @param $latitude
	 * @return void
	 */
	public function setLatitude($latitude) {
		$this->latitude = $latitude;
	}

	/**
	 * @return string
	 */
	public function getLongitude() {
		return $this->longitude;
	}

	/**
	 * @param $longitude
	 * @return void
	 */
	public function setLongitude($longitude) {
		$this->longitude = $longitude;
	}

	/**
	 * @return string
	 */
	public function getAzimuth() {
		return $this->azimuth;
	}

	/**
	 * @param $azimuth
	 * @return void
	 */
	public function setAzimuth($azimuth) {
		$this->azimuth = $azimuth;
	}

	/**
	 * @return string
	 */
	public function getPolar() {
		return $this->polar;
	}

	/**
	 * @param $tilt
	 * @return void
	 */
	public function setPolar($polar) {
		$this->polar = $polar;
	}

	/**
	 * @return mixed
	 */
	public function getAppid() {
		return $this->appId;
	}

	/**
	 * @param $appId
	 * @return void
	 */
	public function setAppid($appId) {
		$this->appId = $appId;
	}

	/**
	 * @return mixed
	 */
	public function getAppcode() {
		return $this->appCode;
	}

	/**
	 * @param $appCode
	 * @return void
	 */
	public function setAppcode($appCode) {
		$this->appCode = $appCode;
	}

}