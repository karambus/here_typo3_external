<?php
namespace Meelogic\HereSliblurring\Controller;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic Consulting AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * BlurringController
 */
class LandingPageController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * initialize Action
	 *
	 * @return void
	 */
	public function initializeAction() {
		$GLOBALS['TSFE']->additionalHeaderData['tx_heresliblurring'] = '<link rel="stylesheet" type="text/css" href="'
																		. \t3lib_extMgm::siteRelPath($this->request->getControllerExtensionKey())
																		. 'Resources/Public/Css/style.css" />
																		';

		$GLOBALS['TSFE']->additionalFooterData['tx_heresliblurring'] = '<script src="'
																		. \t3lib_extMgm::siteRelPath($this->request->getControllerExtensionKey())
																		. 'Resources/Public/Js/LandingPage.js"></script>
																		';
	}

	/**
	 * action new
	 * Renders the landing page
	 *
	 * Example call from client app:
	 * https://here.com/privacy/mobile?panoramaid=17008596&latitude=37.76744635775685&longitude=-122.43501056917012&azimuth=264.47454833984375&polar=90.0&width=52.09266662597656&height=55.717994689941406&app_id=bm8G03BysQAjEHvyCgSm&app_code=CprS7w8rXTBHblaSlb8xVQ&lang=EN
	 *
	 * @return void
	 */
	public function newAction() {

		$this->view->assignMultiple(
				array(
					'blurringPid'       => $this->settings['blurringFormPid'],
					'imageQualityPid'   => $this->settings['imageQualityFormPid'],
					'panoramaid'        => \t3lib_div::_GP('panoramaid'),
					'latitude'          => \t3lib_div::_GP('latitude'),
					'longitude'         => \t3lib_div::_GP('longitude'),
					'azimuth'           => \t3lib_div::_GP('azimuth'),
					'polar'             => \t3lib_div::_GP('polar'),
					'width'             => \t3lib_div::_GP('width'),
					'height'            => \t3lib_div::_GP('height'),
					'appId'             => \t3lib_div::_GP('app_id'),
					'appCode'           => \t3lib_div::_GP('app_code')
				)
		);

	}
}