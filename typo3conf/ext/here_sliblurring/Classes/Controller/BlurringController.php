<?php
namespace Meelogic\HereSliblurring\Controller;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Oliver Wand <oliver.wand@meelogic.com>, Meelogic Consulting AG
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * BlurringController
 */
class BlurringController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * blurringRepository
	 *
	 * @var \Meelogic\HereSliblurring\Domain\Repository\BlurringRepository
	 * @inject
	 */
	protected $blurringRepository = NULL;

	/**
	 * initialize Action
	 *
	 * @return void
	 */
	public function initializeAction() {
		$GLOBALS['TSFE']->additionalHeaderData['tx_heresliblurring'] = '<link rel="stylesheet" type="text/css" href="'
																		. \t3lib_extMgm::siteRelPath($this->request->getControllerExtensionKey())
																		. 'Resources/Public/Css/style.css" />
																		';

		$GLOBALS['TSFE']->additionalFooterData['tx_heresliblurring'] = '<script src="' . \t3lib_extMgm::siteRelPath($this->request->getControllerExtensionKey())
																		. 'Resources/Public/Js/Blurring.js"></script>
																		';
	}

	/**
	 * @return void
	 */
	protected function initializeCreateAction(){
	    $propertyMappingConfiguration = $this->arguments['newBlurring']->getPropertyMappingConfiguration();
	    $propertyMappingConfiguration->allowAllProperties();
	    $propertyMappingConfiguration->setTypeConverterOption('TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter', \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, TRUE);
	}

	/**
	 * action new
	 * Render the blurring reporting form
	 *
	 * Example call from client app:
	 * https://here.com/privacy/mobile?panoramaid=17008596&latitude=37.76744635775685&longitude=-122.43501056917012&azimuth=264.47454833984375&polar=90.0&width=52.09266662597656&height=55.717994689941406&app_id=bm8G03BysQAjEHvyCgSm&app_code=CprS7w8rXTBHblaSlb8xVQ&lang=EN
	 *
	 * @param \Meelogic\HereSliblurring\Domain\Model\Blurring $newBlurring
	 * @return void
	 */
	public function newAction(\Meelogic\HereSliblurring\Domain\Model\Blurring $newBlurring = NULL) {

		$this->view->assignMultiple(
			array(
				'newBlurring'   => $newBlurring,
				'panoramaid'    => \t3lib_div::_GP('panoramaid'),
				'latitude'      => \t3lib_div::_GP('latitude'),
				'longitude'     => \t3lib_div::_GP('longitude'),
				'azimuth'       => \t3lib_div::_GP('azimuth'),
				'polar'         => \t3lib_div::_GP('polar'),
				'width'         => \t3lib_div::_GP('width'),
				'height'        => \t3lib_div::_GP('height'),
				'appId'         => \t3lib_div::_GP('app_id'),
				'appCode'       => \t3lib_div::_GP('app_code')
			)
		);
	}

	/**
	 * action create
	 * process the blurring form and persist the new object
	 *
	 * @param \Meelogic\HereSliblurring\Domain\Model\Blurring $newBlurring
	 * @return void
	 */
	public function createAction(\Meelogic\HereSliblurring\Domain\Model\Blurring $newBlurring) {

		$newBlurring->setWhattoblur($_POST['tx_heresliblurring_sliblurring']['whattoblur']);
		$newBlurring->setNotifyme($_POST['tx_heresliblurring_sliblurring']['notifyme']);
		$newBlurring->setPanoramaid($_POST['tx_heresliblurring_sliblurring']['panoramaid']);
		$newBlurring->setLatitude($_POST['tx_heresliblurring_sliblurring']['latitude']);
		$newBlurring->setLongitude($_POST['tx_heresliblurring_sliblurring']['longitude']);
		$newBlurring->setAzimuth($_POST['tx_heresliblurring_sliblurring']['azimuth']);
		$newBlurring->setPolar($_POST['tx_heresliblurring_sliblurring']['polar']);
		$newBlurring->setAppid($_POST['tx_heresliblurring_sliblurring']['appId']);
		$newBlurring->setAppcode($_POST['tx_heresliblurring_sliblurring']['appCode']);

		$this->blurringRepository->add($newBlurring);
		$persistenceManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
		$persistenceManager->persistAll();

		/** Prepare parameters */

		/*
		IMAGE_PROBLEM Values for problemModifier=IMAGE_BLURRING:
		"license plate"
		"face"
		"building"
		"other"
		IMAGE_PROBLEM Values for problemModifier=IMAGE_QUALITY:
		"incorrect location"
		"obstructed view"
		"poor quality"
		*/

		$PARAMS = array(
		   '%USER_TRACKING%'    => $_POST['tx_heresliblurring_sliblurring']['appId'] . '/' . $_POST['tx_heresliblurring_sliblurring']['appCode'],
		   '%MESSAGE%'          => $_POST['tx_heresliblurring_sliblurring']['newBlurring']['message'],
		   '%LANGUAGE%'			=> $GLOBALS['TSFE']->lang,
		   '%NOTIFY%'           => ( $_POST['tx_heresliblurring_sliblurring']['notifyme'] ) ? '1' : '0',
		   '%EMAIL%'            => ( $_POST['tx_heresliblurring_sliblurring']['notifyme'] ) ? $_POST['tx_heresliblurring_sliblurring']['newBlurring']['email'] : '',
		   '%APP_ID%'           => $_POST['tx_heresliblurring_sliblurring']['appId'],
		   '%APP_CODE%'         => $_POST['tx_heresliblurring_sliblurring']['appCode'],
		   '%LATITUDE%'         => round( $_POST['tx_heresliblurring_sliblurring']['latitude'] * 100000 ),
		   '%LONGITUDE%'        => round( $_POST['tx_heresliblurring_sliblurring']['longitude'] * 100000 ),
		   '%IMAGE_PROBLEM%'    => $_POST['tx_heresliblurring_sliblurring']['whattoblur'],
		   '%IMAGE_ID%'         => $_POST['tx_heresliblurring_sliblurring']['panoramaid'],
		   '%POLAR_ANGLE%'      => $_POST['tx_heresliblurring_sliblurring']['polar'],
		   '%AZIMUTH_ANGLE%'    => $_POST['tx_heresliblurring_sliblurring']['azimuth'],
		   '%WIDTH%'            => $_POST['tx_heresliblurring_sliblurring']['width'],
		   '%HEIGHT%'           => $_POST['tx_heresliblurring_sliblurring']['height'],
		   '%CHANGE_DATE%'      =>  date( "Y-m-d\TH:i:s.000P", time() ),
		   '%PROBLEM_MODIFIER%' => 'IMAGE_BLURRING',
		   '%ACTIVATION_KEY%'   => 'e6ltjjr57novcf5ggvafwyy762vokxe2' // const
		);


		/** Send parameters to Map Reporter */

		$TEMPLATE = '
		{
		   "MapChangeRequest":{
		      "metaData":{
		         "baselineMap":{
		            "mapName":"GDF",
		            "release":"1.5",
		            "systemName":{
		               "value":"UNKNOWN",
		               "deviceId":"3",
		               "hardwareVersion":"1",
		               "softwareVersion":"2"
		            }
		         },
		         "reporter":{
		            "name":{
		               "value":"com.here.ios",
		               "realm":"WHITELABEL"
		            },
		            "description":"%MESSAGE%",
		            "anonymousEmail":"%EMAIL%",
		            "anonymousLanguage":"%LANGUAGE%"
		         }
		      },
		      "changeCollection":[
		         {
		            "transaction":[
		               {
		                  "changeInfo":{
		                     "changeDate":"%CHANGE_DATE%",
		                     "changeReason":"HUMAN"
		                  },
		                  "changeOperation":[
		                     {
					            "userTracking":"%USER_TRACKING%",
		                        "pointFeature":{
		                           "singleAttribute":[
		                              {
		                                 "attrCode":"loc-latlong",
		                                 "value":{ "value":"%LATITUDE%;%LONGITUDE%" }
		                              },
		                              {
		                                 "attrCode":"image-id",
		                                 "value":{ "value":"%IMAGE_ID%" }
		                              },
		                              {
		                                 "attrCode":"image-problem",
		                                 "value":{ "value":"%IMAGE_PROBLEM%" }
		                              },
		                              {
		                                 "attrCode":"text-attribute",
		                                 "value":{ "value":"{ \"imageId\": %IMAGE_ID%, \"areaOnImage\": { \"centerPoint\": { \"polarAngleInDegrees\": %POLAR_ANGLE%, \"azimuthAngleInDegrees\": %AZIMUTH_ANGLE% }, \"widthInDegrees\": %WIDTH%, \"heightInDegrees\": %HEIGHT% }" }
		                              }
		                           ],
		                           "featCode":"5800"
		                        },
		                        "problemModifier":"%PROBLEM_MODIFIER%"
		                     }
		                  ]
		               }
		            ]
		         }
		      ],
		      "version":"2.0"
		   }
		}';

		$header = array(
			'Accept: application/json',
			'Content-Type: application/json',
			'activation_key: ' . $PARAMS['%ACTIVATION_KEY%'],
			'app_id: ' . $PARAMS['%APP_ID%']
		);

		$data = strtr($TEMPLATE,$PARAMS);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $this->settings['mapReporterUrl']);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_VERBOSE, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$result = curl_exec($curl);
		curl_close($curl);

		$resultDec = json_decode($result,True);
		if ( isset( $resultDec['SubmitMapReportOutput']['commonDataOutput']['statusCode'] ) && $resultDec['SubmitMapReportOutput']['commonDataOutput']['statusCode']=='00' ) {
			 $this->redirect('success');
		}else{
			 $this->redirect('error');
		}

	}

	/**
	 * success action
	 * Does basically nothing but rendering the success template
	 */
	public function successAction() {

	}

	/**
	 * error action
	 * Does basically nothing but rendering the error template
	 */
	public function errorAction() {

	}

}